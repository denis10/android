package com.example.task2_3denis;

import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

    private CustomReceiver customReceiver = new CustomReceiver();
    public static final String ACTION = "1";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    public void registerBroadcastReceiver(View view) {
        this.registerReceiver(customReceiver, new IntentFilter(
                ACTION));
        sendBroadcast(new Intent(ACTION));
        Toast.makeText(this, "Turned on",
                Toast.LENGTH_SHORT).show();
    }

    // Отменяем регистрацию
    public void unregisterBroadcastReceiver(View view) {
        this.unregisterReceiver(customReceiver);

        Toast.makeText(this, "Broadcast is turned off", Toast.LENGTH_SHORT)
                .show();
    }


}
