package com.example.task3_3denis;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Loader;
import android.os.Bundle;
import android.util.Log;


/**
 * Created by dev1 on 1/30/14.
 */
public class FactorialLoader extends AsyncTaskLoader<Integer> {
    private final String LOG_TAG = "myLogs";
    private Integer N;

    public FactorialLoader(Context context,Bundle args) {
        super(context);
        N= args.getInt("value");
    }
    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        Log.d(LOG_TAG, hashCode() + " onStartLoading");
    }
    @Override
    protected void onStopLoading() {
        super.onStopLoading();
        Log.d(LOG_TAG, hashCode() + " onStopLoading");
    }

    @Override
    public Integer loadInBackground() {
        int buf=1;
        for (int i=2;i<=N;i++){
            buf*=i;
        }
        getResultFromTask(buf);
        return buf;
    }

    void getResultFromTask(Integer result) {
        deliverResult(result);
    }
    @Override
    protected void onAbandon() {
        super.onAbandon();
        Log.d(LOG_TAG, hashCode() + " onAbandon");
    }

    @Override
    protected void onReset() {
        super.onReset();
        Log.d(LOG_TAG, hashCode() + " onReset");
    }
}
