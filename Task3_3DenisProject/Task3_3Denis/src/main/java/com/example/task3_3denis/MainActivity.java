package com.example.task3_3denis;


import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity implements LoaderManager.LoaderCallbacks<Integer> {
    static final int LOADER_ID = 1;
    private TextView v1;
    private  final String LOG_TAG = "myLogs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        v1 = (TextView) findViewById(R.id.view1);


    }

    public void onCalcButtonClick(View view){
        EditText edit1=(EditText)findViewById(R.id.edt1);
        int N=0;
        boolean correctNumber=false;
        try{
            N=Integer.parseInt(edit1.getText().toString());
            correctNumber=true;
        }catch (Exception e){
            Toast.makeText(getApplicationContext(), "Please put correct data", Toast.LENGTH_SHORT).show();
        }

        if (correctNumber){
            Bundle bndl = new Bundle();
            bndl.putInt("value",N);
            getLoaderManager().initLoader(LOADER_ID, bndl, this);
           // Toast.makeText(getApplicationContext(), "res1="+edit1.getText(), Toast.LENGTH_SHORT).show();

            //Loader<Integer> fac=getLoaderManager().getLoader(LOADER_ID);
            AsyncTaskLoader<Integer> fac;
            fac = (AsyncTaskLoader)getLoaderManager().restartLoader(LOADER_ID, bndl,this);
            //fac.forceLoad();
            fac.loadInBackground();

        }
    }
    @Override
    public AsyncTaskLoader<Integer> onCreateLoader(int i, Bundle bundle) {

        FactorialLoader fac=null;
        if (i==LOADER_ID){
            fac=new FactorialLoader(this,bundle);
        }
        return fac;
    }

    @Override
    public void onLoadFinished(Loader<Integer> stringLoader, Integer s) {
       // Toast.makeText(getApplicationContext(), "res"+s, Toast.LENGTH_SHORT).show();
        v1.setText(""+s);
    }


    @Override
    public void onLoaderReset(Loader<Integer> stringLoader) {
        Log.d(LOG_TAG, "onLoaderReset for loader " + stringLoader.hashCode());
    }
}
