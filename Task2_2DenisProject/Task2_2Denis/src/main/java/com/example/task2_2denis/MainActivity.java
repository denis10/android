package com.example.task2_2denis;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends ActionBarActivity {
final private int PICK_CONTACT_REQUEST=1;
final private int VIDEO_CODE=2;
final private int IMAGE_CODE=3;
final private int REQUEST_IMAGE_CAPTURE = 4;
//final private Uri mLocationForPhotos=Uri.parse("file///storage/ext_sd/Albums");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button b1=(Button)findViewById(R.id.button1);
        b1.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View view) {
                pickContact();
            }
        });
        Button b2=(Button)findViewById(R.id.button2);
        b2.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View view) {
                showVideo();
            }
        });
        Button b3=(Button)findViewById(R.id.button3);
        b3.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View view) {
                showImage();
            }
        });
        Button b4=(Button)findViewById(R.id.button4);
        b4.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View view) {
                capturePhoto();
            }
        });
    }

    private void pickContact() {
        // Create an intent to "pick" a contact, as defined by the content provider URI
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT_REQUEST);
    }
    private void capturePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
        }
    }
    private void showVideo(){
        Intent intent = new Intent(Intent.ACTION_PICK, null);
        intent.setType("video/*");
        startActivityForResult(intent, VIDEO_CODE);
    }
    private void showImage(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, IMAGE_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // If the request went well (OK) and the request was PICK_CONTACT_REQUEST
        if (resultCode == Activity.RESULT_OK){
            switch (requestCode){
                case PICK_CONTACT_REQUEST:
                    // Perform a query to the contact's content provider for the contact's name
                    Cursor cursor = getContentResolver().query(data.getData(),
                            new String[] {ContactsContract.Contacts.DISPLAY_NAME}, null, null, null);
                    if (cursor.moveToFirst()) { // True if the cursor is not empty
                        int columnIndex = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
                        String name = cursor.getString(columnIndex);
                        // Do something with the selected contact's name...
                        //Toast.makeText(getApplicationContext(), name, Toast.LENGTH_SHORT).show();
                        TextView text1=(TextView)findViewById(R.id.txt1);
                        text1.setText(name);
                    }
                    break;
                case REQUEST_IMAGE_CAPTURE:
                    //Toast.makeText(getApplicationContext(), (data==null)?"null":"not null", Toast.LENGTH_SHORT).show();
                    // Do other work with full size photo saved in mLocationForPhotos
                    Bundle extras = data.getExtras();
                    Bitmap imageBitmap = (Bitmap) extras.get("data");
                    ImageView im1=(ImageView)findViewById(R.id.image1);
                    im1.setImageBitmap(imageBitmap);
                    break;
                case VIDEO_CODE:
                    VideoView videoView1=(VideoView)findViewById(R.id.video1);
                    videoView1.setVideoPath(data.getData().toString());
                    videoView1.start();                    
                    break;
                case IMAGE_CODE:
                    try {
                        InputStream stream = getContentResolver().openInputStream(
                                data.getData());
                        Bitmap bitmap = BitmapFactory.decodeStream(stream);
                        stream.close();
                        ImageView imageView=(ImageView)findViewById(R.id.image1);
                        imageView.setImageBitmap(bitmap);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
