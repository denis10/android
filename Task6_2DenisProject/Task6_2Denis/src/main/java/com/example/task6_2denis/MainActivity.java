package com.example.task6_2denis;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class MainActivity extends ActionBarActivity {
    //private SQLiteDatabase db;
    private DbCommonHelper dbCommonHelper;
    private final String LOG_TAG = "myLogs";
    private Officer officer;
    private Suspect suspect;
    private District district;
    private Detention detention;
    private long idOfficer;
    private long idSuspect;
    private long idDistrict;
    private final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
    private final Lock r = rwl.readLock();
    private final Lock w = rwl.writeLock();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbCommonHelper = new DbCommonHelper(MainActivity.this);

        Button btn1=(Button)findViewById(R.id.btn1);
        btn1.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CreateDb().execute();
            }
        });

        Button btn2=(Button)findViewById(R.id.btn2);
        btn2.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DeleteOneDetention().execute();
            }
        });
        Button btn3=(Button)findViewById(R.id.btn3);
        btn3.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DeleteDb().execute();
            }
        });

        Button btn4=(Button)findViewById(R.id.btn4);
        btn4.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                new UpdateDb().execute();
            }
        });

        Button btn5=(Button)findViewById(R.id.btn5);
        btn5.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ReadDb().execute();
            }
        });
    }
    private class CreateDb extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {

            SQLiteDatabase db=null;
            w.lock();
            try{
                try {
                    db = dbCommonHelper.getWritableDatabase();
                } catch (SQLiteException ex) {
                    db = dbCommonHelper.getReadableDatabase();
                }
                officer=new Officer("Max");
                idOfficer=db.insert(DbCommonHelper.TABLE_OFFICERS, null, DbCommonHelper.officerToCv(officer));
                suspect=new Suspect("Bill");
                idSuspect=db.insert(DbCommonHelper.TABLE_SUSPECTS, null, DbCommonHelper.suspectToCv(suspect));
                district=new District("DownTown");
                idDistrict=db.insert(DbCommonHelper.TABLE_DISTRICTS, null, DbCommonHelper.districtToCv(district));
                detention=new Detention(idOfficer,idSuspect,idDistrict);
                db.insert(DbCommonHelper.TABLE_DETENTIONS, null, DbCommonHelper.detentionToCv(detention));

                officer=new Officer("Denis");
                idOfficer=db.insert(DbCommonHelper.TABLE_OFFICERS, null, DbCommonHelper.officerToCv(officer));
                suspect=new Suspect("Carl");
                idSuspect=db.insert(DbCommonHelper.TABLE_SUSPECTS, null, DbCommonHelper.suspectToCv(suspect));
                district=new District("village");
                idDistrict=db.insert(DbCommonHelper.TABLE_DISTRICTS, null, DbCommonHelper.districtToCv(district));
                detention=new Detention(idOfficer,idSuspect,idDistrict);
                db.insert(DbCommonHelper.TABLE_DETENTIONS, null, DbCommonHelper.detentionToCv(detention));
            }finally {
                if (db!=null)
                db.close();
                w.unlock();
            }

            return null;
        }
        protected void onPostExecute() {

        }
    }
    private class DeleteOneDetention extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            SQLiteDatabase db;
                try {
                    db = dbCommonHelper.getWritableDatabase();
                }
                catch (SQLiteException ex){
                    db = dbCommonHelper.getReadableDatabase();
                }
            synchronized(db){
                //db.delete(DbDetentionsHelper.TABLE_NAME, DbDetentionsHelper.PLACE + "=" + "?", new String[]{"home"});
                //db.delete(DbDetentionsHelper.TABLE_NAME, DbDetentionsHelper.OFFICER_NAME + "='" + "Max'", null);


                db.delete(DbCommonHelper.TABLE_DETENTIONS, DbCommonHelper.OFFICER_ID + "=" + 1, null);
                db.close();
            }
            return null;
        }
        protected void onPostExecute() {

        }
    }
    private class DeleteDb extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            SQLiteDatabase db;
            try {
                db = dbCommonHelper.getWritableDatabase();
            } catch (SQLiteException ex) {
                db = dbCommonHelper.getReadableDatabase();
            }
            synchronized(db){
                db.delete(DbCommonHelper.TABLE_DETENTIONS, null, null);
                db.close();
            }
            return null;
        }
        protected void onPostExecute() {

        }
    }
    private class UpdateDb extends AsyncTask<Void, Void, Void>{
        SQLiteDatabase db;

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                db = dbCommonHelper.getWritableDatabase();
            } catch (SQLiteException ex) {
                db = dbCommonHelper.getReadableDatabase();
            }
            synchronized(db){
                officer=new Officer("Fill");
                db.update(DbCommonHelper.TABLE_OFFICERS,DbCommonHelper.officerToCv(officer),"_id" + "=" + 1,null);
                db.close();
            }
            return null;
        }
        protected void onPostExecute() {

        }
    }
    private class ReadDb extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            SQLiteDatabase db;
            try {
                db = dbCommonHelper.getWritableDatabase();
            } catch (SQLiteException ex) {
                db = dbCommonHelper.getReadableDatabase();
            }
            synchronized(db){
                Cursor c = db.query(DbCommonHelper.TABLE_SUSPECTS, null, null, null, null, null, null);
                if (c.moveToFirst()) {
                    int idColIndex = c.getColumnIndex("_id");
                    int nameColIndex = c.getColumnIndex("suspect_name");
                    do {
                        Log.d(LOG_TAG,
                                "ID = " + c.getInt(idColIndex) +
                                        ", name = " + c.getString(nameColIndex));
                    } while (c.moveToNext());
                } else
                    Log.d(LOG_TAG, "0 rows");
                c.close();
                db.close();
            }
            return null;
        }
        protected void onPostExecute() {

        }
    }
}
