package com.example.task5_2denis;

/**
 * Created by dev1 on 2/3/14.
 */
public class Item {
    private  String label;
    public Item(String label){
        this.label=label;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
