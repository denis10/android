package com.example.task5_2denis;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by dev1 on 2/5/14.
 */
public class MyNewSpinnerAdapter extends ArrayAdapter<Item> {
    private final Context context;
    private final ArrayList<Item> itemsArrayList;
    private Spinner spinner;
    private View spinnerView;
    private TextView labelView;
    private RadioButton radioButton;
    private MyInterface myInterface;


    public MyNewSpinnerAdapter(Context context, ArrayList<Item> itemsArrayList,MyInterface myInterface) {
        super(context, R.layout.spinner_row, itemsArrayList);

        this.context = context;
        this.itemsArrayList = itemsArrayList;
        this.myInterface=myInterface;
    }

    @Override
    public Item getItem(int position) {

        return itemsArrayList.get(position);
    }

    @Override
    public int getCount() {
        return itemsArrayList.size();
    }
    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
    public View getCustomView(int position, View convertView,
                              ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView= inflater.inflate(R.layout.spinner_row, parent, false);
        labelView=(TextView) rowView.findViewById(R.id.spinner_textView);
       /* if (itemsArrayList.get(position).get("type") != null) {
            viewHolder.spType.setSelection(Integer.parseInt(results.get(position).get("type")));
        }*/

            labelView.setText(itemsArrayList.get(position).getLabel());

        spinnerView= inflater.inflate(R.layout.spinner, parent, false);
        spinner = (Spinner) spinnerView.findViewById(R.id.planets_spinner);

        radioButton    =(RadioButton)rowView.findViewById(R.id.spinner_radio_button);
        final int posRadio=position;
        radioButton.setOnClickListener(new View.OnClickListener(){

            public void onClick(View v){
                final RadioButton mBtnRadio = (RadioButton) v;
                // select only one radio button at any given time
                if (radioButton != null) {
                    radioButton.setChecked(false);
                }
                mBtnRadio.setChecked(true);
                radioButton = mBtnRadio;
                Toast.makeText(getContext(), "CLICKED radio button:" + itemsArrayList.get(posRadio).getLabel(), Toast.LENGTH_SHORT).show();
            }
        });

        final int pos=position;
        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TextView output = (TextView) spinnerView.findViewById(R.id.output);
                //output.setText(labelView.getText());

                Toast.makeText(getContext(), "CLICKED:" + itemsArrayList.get(pos).getLabel(), Toast.LENGTH_SHORT).show();
                //labelView.setText(itemsArrayList.get(pos).getLabel());
                myInterface.onClick(pos);
            }
        });
        /*spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {
                // Get selected row data to show on screen
                TextView output = (TextView) spinnerView.findViewById(R.id.output);
                output.setText(labelView.getText());
                Toast.makeText(getContext(), "CLICKED:" + itemsArrayList.get(position).getLabel(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });*/
        /*final int pos=position;
        spinnerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "CLICKED:" + itemsArrayList.get(pos).getLabel(), Toast.LENGTH_SHORT).show();
            }
        });*/

        return rowView;
    }

}

