package com.example.task5_2denis;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class SecondActivity extends ActionBarActivity  {
    private LinearLayout lin;
    private LayoutInflater inflater;
    private ListView listView;
    private List<String> groupList;
    private List<String> childList;
    private Map<String, List<String>> laptopCollection;
    private ExpandableListView expListView;
    private GridView gridView;
    private Spinner spinner;
    private TextView output;
   // private MyInterface myInterface;

    private class MyController implements  MyInterface{

        @Override
        public void onClick(int pos) {
            spinner.setSelection(pos);

        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        final int res=getIntent().getExtras().getInt("value");
        lin=(LinearLayout)findViewById(R.id.lin_second);

        inflater =(LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (res){
            case 1:
                Toast.makeText(getApplicationContext(), "ListView", Toast.LENGTH_SHORT).show();
                inflater.inflate(R.layout.list_view_layout,lin,true);
                listView=(ListView)findViewById(R.id.planets_list_view);
                MyListViewAdapter adapter = new MyListViewAdapter(this,generateData());
                listView.setAdapter(adapter);
                break;
            case 2:
                Toast.makeText(getApplicationContext(), "GridView", Toast.LENGTH_SHORT).show();
                inflater.inflate(R.layout.grid_view_layout,lin,true);
                gridView=(GridView)findViewById(R.id.planets_grid_view);
                MyGridViewAdapter adapterGrid = new MyGridViewAdapter(this,generateGridData());
                gridView.setAdapter(adapterGrid);
                break;
            case 3:
                Toast.makeText(getApplicationContext(), "Spinner", Toast.LENGTH_SHORT).show();
                inflater.inflate(R.layout.spinner,lin,true);
                output  = (TextView)findViewById(R.id.output);
                spinner = (Spinner) findViewById(R.id.planets_spinner);
                //MySpinnerAdapter adapterSpinner =new MySpinnerAdapter(this,generateData());

                MyInterface mi=new MyController();
                MyNewSpinnerAdapter adapterSpinner =new MyNewSpinnerAdapter(this,generateData(),mi);
                // Specify the layout to use when the list of choices appears
                adapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                // Apply the adapter to the spinner
                spinner.setAdapter(adapterSpinner);
                /*spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {
                        // Get selected row data to show on screen
                        TextView planet=(TextView) findViewById(R.id.spinner_textView);
                        TextView output=(TextView) findViewById(R.id.output);
                        output.setText(planet.getText().toString());
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parentView) {
                        // your code here
                    }

                });*/
                break;
            case 4:
                Toast.makeText(getApplicationContext(), "ExpandableListView", Toast.LENGTH_SHORT).show();
                /*Intent intent=new Intent(SecondActivity.this,ExpandedActivity.class);
                startActivity(intent);*/
                inflater.inflate(R.layout.expandable_list_view,lin,true);

                createGroupList();

                createCollection();

                expListView = (ExpandableListView) findViewById(R.id.laptop_list);

                final ExpandableListAdapter expListAdapter = new ExpandableListAdapter(
                        this, groupList, laptopCollection);
                expListView.setAdapter(expListAdapter);
                break;
            default:
        }
    }
    private ArrayList<Item> generateData(){
        ArrayList<Item> items = new ArrayList<Item>();
        String[] planets=null;
        try {
            planets = getResources().getStringArray(R.array.planets_array);
        }catch (Exception e){
            e.printStackTrace();
        }
        for(int i=0;i<planets.length;i++){
            items.add(new Item(planets[i]));
        }
        return items;
    }
    private ArrayList<GridItem> generateGridData(){
        ArrayList<GridItem> items = new ArrayList<GridItem>();
        String[] planets=null;
        try {
            planets = getResources().getStringArray(R.array.planets_array);
        }catch (Exception e){
            e.printStackTrace();
        }

        Bitmap[] icons=new Bitmap[8];
        icons[0]=BitmapFactory.decodeResource(this.getResources(), R.drawable.mercury);
        icons[1]=BitmapFactory.decodeResource(this.getResources(), R.drawable.venus);
        icons[2]=BitmapFactory.decodeResource(this.getResources(), R.drawable.earth);
        icons[3]=BitmapFactory.decodeResource(this.getResources(), R.drawable.mars);
        icons[4]=BitmapFactory.decodeResource(this.getResources(), R.drawable.jupiter);
        icons[5]=BitmapFactory.decodeResource(this.getResources(), R.drawable.saturn);
        icons[6]=BitmapFactory.decodeResource(this.getResources(), R.drawable.uranus);
        icons[7]=BitmapFactory.decodeResource(this.getResources(), R.drawable.neptune);

        for(int i=0;i<planets.length;i++){
            items.add(new GridItem(icons[i],planets[i]));
        }
        return items;
    }
    private void createGroupList() {
        groupList = new ArrayList<String>();
        groupList.add("HP");
        groupList.add("Dell");
        groupList.add("Lenovo");
        groupList.add("Sony");
        groupList.add("HCL");
        groupList.add("Samsung");
    }
    private void createCollection() {
        // preparing laptops collection(child)
        String[] hpModels = { "HP Pavilion G6-2014TX", "ProBook HP 4540",
                "HP Envy 4-1025TX" };
        String[] hclModels = { "HCL S2101", "HCL L2102", "HCL V2002" };
        String[] lenovoModels = { "IdeaPad Z Series", "Essential G Series",
                "ThinkPad X Series", "Ideapad Z Series" };
        String[] sonyModels = { "VAIO E Series", "VAIO Z Series",
                "VAIO S Series", "VAIO YB Series" };
        String[] dellModels = { "Inspiron", "Vostro", "XPS" };
        String[] samsungModels = { "NP Series", "Series 5", "SF Series" };

        laptopCollection = new LinkedHashMap<String, List<String>>();

        for (String laptop : groupList) {
            if (laptop.equals("HP")) {
                loadChild(hpModels);
            } else if (laptop.equals("Dell"))
                loadChild(dellModels);
            else if (laptop.equals("Sony"))
                loadChild(sonyModels);
            else if (laptop.equals("HCL"))
                loadChild(hclModels);
            else if (laptop.equals("Samsung"))
                loadChild(samsungModels);
            else
                loadChild(lenovoModels);

            laptopCollection.put(laptop, childList);
        }
    }
    private void loadChild(String[] laptopModels) {
        childList = new ArrayList<String>();
        for (String model : laptopModels)
            childList.add(model);
    }
}
