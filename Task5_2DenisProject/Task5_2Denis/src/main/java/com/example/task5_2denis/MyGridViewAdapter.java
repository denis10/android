package com.example.task5_2denis;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by dev1 on 2/3/14.
 */
public class MyGridViewAdapter extends ArrayAdapter<GridItem> {

    private final Context context;
    private final ArrayList<GridItem> itemsArrayList;

    public MyGridViewAdapter(Context context, ArrayList<GridItem> itemsArrayList) {

        super(context, R.layout.grid_row, itemsArrayList);

        this.context = context;
        this.itemsArrayList = itemsArrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // 1. Create inflater
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // 2. Get rowView from inflater
        View rowView = inflater.inflate(R.layout.grid_row, parent, false);

        // 3. Get the text view from the rowView
        TextView labelView = (TextView) rowView.findViewById(R.id.item_text);

        // 4. Set the text for textView
        labelView.setText(itemsArrayList.get(position).getLabel());

        //
        ImageView imageView=(ImageView)rowView.findViewById(R.id.item_image);
        //
        imageView.setImageBitmap(itemsArrayList.get(position).getBitmap());
        // 5. retrn rowView
        return rowView;
    }
}