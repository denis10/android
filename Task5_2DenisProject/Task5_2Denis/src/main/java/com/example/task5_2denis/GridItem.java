package com.example.task5_2denis;

import android.graphics.Bitmap;

/**
 * Created by dev1 on 2/3/14.
 */
public class GridItem {
    private  String label;
    private Bitmap bitmap;

    public GridItem(Bitmap bitmap,String label){
        this.bitmap=bitmap;

        this.label=label;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
}
