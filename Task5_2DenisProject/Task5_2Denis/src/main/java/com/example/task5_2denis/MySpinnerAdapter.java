package com.example.task5_2denis;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by dev1 on 2/3/14.
 */
public class MySpinnerAdapter extends ArrayAdapter<Item> {

    private final Context context;
    private final ArrayList<Item> itemsArrayList;

    public MySpinnerAdapter(Context context, ArrayList<Item> itemsArrayList) {

        super(context, R.layout.spinner_row, itemsArrayList);

        this.context = context;
        this.itemsArrayList = itemsArrayList;
    }

    @Override
    public Item getItem(int position) {

        return itemsArrayList.get(position);
    }

    @Override
    public int getCount() {
        return itemsArrayList.size();
    }
    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if(convertView==null)
        {
            // 1. Create inflater
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.spinner_row,null);
            viewHolder=new ViewHolder();
            viewHolder.spinner=(Spinner)convertView.findViewById(R.id.planets_spinner);
            //View rowView = inflater.inflate(R.layout.spinner_row, parent, false);
            viewHolder.rowView= inflater.inflate(R.layout.spinner_row, parent, false);
            //final TextView labelView = (TextView) viewHolder.rowView.findViewById(R.id.spinner_textView);
            viewHolder.labelView=(TextView) viewHolder.rowView.findViewById(R.id.spinner_textView);

           /* // store the holder with the view.
            viewHolder.rowView.setTag(position);
            viewHolder.labelView.setTag(position);
            viewHolder.spinner.setTag(position);*/

            convertView.setTag(viewHolder);
        }
        else{
            viewHolder=(ViewHolder)convertView.getTag();
        }
        Item item = itemsArrayList.get(position);

        // assign values if the object is not null
        if(item != null) {
            viewHolder.labelView.setText(item.getLabel());
        }



/*
        viewHolder.radioButton    =(RadioButton)viewHolder.rowView.findViewById(R.id.spinner_radio_button);
        viewHolder.radioButton.setOnClickListener(new View.OnClickListener(){

            public void onClick(View v){

                Toast.makeText(getContext(),"CLICKED:"+labelView.getText(),Toast.LENGTH_SHORT).show();

            }
        });*/

        return convertView;
    }

    static class ViewHolder {
        View rowView;
        TextView labelView;
        RadioButton radioButton;
        Spinner spinner;
    }
}