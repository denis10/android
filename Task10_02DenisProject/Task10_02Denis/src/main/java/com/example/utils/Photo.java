package com.example.utils;

/**
 * Created by dev1 on 2/10/14.
 */
public class Photo {
    public static int LOADED=1;

    private long id;
    private String path;
    private double longitude;
    private double latitude;
    private int loaded;
    public Photo(String path,double longtitude,double latitude,int loaded){
        this.path=path;
        this.longitude =longtitude;
        this.latitude=latitude;
        this.loaded=loaded;
    }

    public Photo(long id,String path,double longtitude,double latitude,int loaded){
        this.id=id;
        this.path=path;
        this.longitude =longtitude;
        this.latitude=latitude;
        this.loaded=loaded;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public int getLoaded() {

        return loaded;
    }

    public void setLoaded(int loaded) {
        this.loaded = loaded;
    }
}
