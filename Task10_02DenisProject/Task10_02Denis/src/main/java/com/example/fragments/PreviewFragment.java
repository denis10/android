package com.example.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.example.helpers.DbCommonHelper;
import com.example.task10_02denis.R;
import com.example.utils.DbUtils;
import com.example.utils.FileUtils;
import com.example.utils.ImageUtils;

import org.apache.commons.io.FilenameUtils;

import java.io.File;

/**
 * Created by dev1 on 2/10/14.
 */
public class PreviewFragment extends Fragment {
    private final Handler mHandler = new Handler();
    private String mPath;
    private ImageView mImageView;
    private Bitmap mBitmap;
    //private long mId;
    private boolean mFirstTimeView;
    private boolean mLoadable;
    private DbCommonHelper mDBHelper;
    private boolean mLittle;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();
        mPath = bundle.getString("path");
        //mId = bundle.getLong("id");
        mFirstTimeView = bundle.getBoolean("firstTime");
        mLittle = bundle.getBoolean("little");
        View view;
        if (mLittle) {
            view = inflater.inflate(R.layout.preview_little_fragment,
                    container, false);
        } else {
            view = inflater.inflate(R.layout.preview_fragment,
                    container, false);
        }
        mDBHelper = new DbCommonHelper(getActivity());
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;


        Log.i("log_tag", "mPath in on Resume=" + FileUtils.getRealPathFromURI(getActivity(),Uri.parse(mPath)));
        mBitmap = ImageUtils.getImage(FileUtils.getRealPathFromURI(getActivity(),Uri.parse(mPath)), height/4 , width/4);
        //mBitmap = ImageUtils.getImage(mPath, height / 4, width / 4);

        //mBitmap = ImageUtils.getImage("file://storage/sdcard0/Pictures/JPEG_20140214_115335.jpg", height / 4, width / 4);
        mImageView.setImageBitmap(mBitmap);
    }

    @Override
    public void onPause() {
        super.onPause();
        mBitmap = null;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mImageView = (ImageView) getView().findViewById(R.id.image_view);

        if (savedInstanceState != null) {
            mLoadable = savedInstanceState.getBoolean("mLoadable");
            showButton();
        } else {
            if (mFirstTimeView) {
                mLoadable = true;
                showButton();
            } else {
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... voids) {

                        SQLiteDatabase db = mDBHelper.getWritableDatabase();
                        synchronized (DbCommonHelper.class) {
                            String query = "SELECT * FROM " + DbCommonHelper.TABLE_PHOTO +
                                    " WHERE " + DbCommonHelper.PATH + "=?" +
                                    //" WHERE " + DbCommonHelper.PATH + "=" + "'" + mPath + "'"+
                                    ";";
                            Log.i("log_tag", "mPhoto preview=" + mPath);
                            Cursor c = db.rawQuery(query, new String[]{mPath});
                            //Cursor c = db.rawQuery(query, null);
                            if (c != null) {
                                if (c.moveToFirst()) {
                                    int loadedColumn = c.getColumnIndex("loaded");
                                    int loaded;
                                    int pathIndex = c.getColumnIndex("path");
                                    //do {
                                    loaded = c.getInt(loadedColumn);
                                    Log.i("log_tag", "path in cursor=" + c.getString(pathIndex));
                                    Log.i("log_tag", "loaded in cursor=" + c.getInt(loadedColumn));
                                    //} while (c.moveToNext());
                                    if (loaded == 0) {
                                        mLoadable = true;
                                    } else {
                                        mLoadable = false;
                                    }
                                } else {
                                    Log.d("log", "0 rows");
                                }
                            }
                            c.close();
                            db.close();
                        }
                        return null;
                    }

                    protected void onPostExecute(Void voids) {
                        showButton();
                    }
                }.execute();
            }
        }


    }

    private void showButton() {
        final Button b1 = (Button) getView().findViewById(R.id.upload_photo);
        b1.setEnabled(mLoadable);
        b1.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLoadable = false;
                b1.setEnabled(mLoadable);

                showDialog();

                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... voids) {
                        //ImageUtils.postImageUpload("https://api.parse.com/1/files/" + s1 + "." + s2, mBitmap);
                        ImageUtils.postImageUpload("https://api.parse.com/1/files/" + new File(Uri.parse(mPath).getPath()).getName(), mBitmap);

                        //Log.e("log_tag", "upload button");
                        DbUtils.setLoaded(getActivity(), mPath);
                        return null;
                    }

                    protected void onPostExecute(Void voids) {
                        hideDialog();
                    }
                }.execute();
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("mLoadable", mLoadable);
    }

    private void showDialog() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }

        // Create and show the dialog.
        DialogFragment newFragment = new MyAlertDialog();
        newFragment.show(ft, "dialog");
    }

    private void hideDialog() {
        mHandler.post(new Runnable() {

            @Override
            public void run() {
                FragmentTransaction ft = getFragmentManager()
                        .beginTransaction();
                Fragment prev = getFragmentManager()
                        .findFragmentByTag("dialog");
                if (prev != null) {
                    ft.remove(prev).commit();
                }
            }
        });
    }

    private static class MyAlertDialog extends DialogFragment {
        /*
         * All subclasses of Fragment must include a public empty constructor.
         * The framework will often re-instantiate a fragment class when needed,
         * in particular during state restore, and needs to be able to find this
         * constructor to instantiate it. If the empty constructor is not
         * available, a runtime exception will occur in some cases during state
         * restore.
         */
        public MyAlertDialog() {
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            ProgressDialog progress = new ProgressDialog(getActivity());
            progress.setMessage(getString(R.string.loading));
            return progress;
        }
    }
}
