package com.example.fragments;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.helpers.DbCommonHelper;
import com.example.task10_02denis.R;
import com.example.utils.FileUtils;
import com.example.utils.Photo;

import java.io.File;
import java.io.IOException;


/**
 * Created by dev1 on 2/10/14.
 */
public class MainFragment extends Fragment {
    //final private int REQUEST_IMAGE_CAPTURE = 1;
    private static final int REQUEST_TAKE_PHOTO = 1;

    private double mLongitude = 0;
    private double mLatitude = 0;
    private LocationManager mLocManager;
    private MyLocationListener mLocListener;
    private DbCommonHelper mDBHelper;
    private Photo mPhoto;
    private String mPath;
    private Button mB1;
    private Button mB2;
    private Button mB3;
    private long mId;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment,
                container, false);
        mDBHelper = new DbCommonHelper(getActivity());
        if (savedInstanceState != null) {
            mLongitude = savedInstanceState.getDouble("mLongitude", mLongitude);
            mLatitude = savedInstanceState.getDouble("mLatitude", mLatitude);
        }

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putDouble("mLongitude", mLongitude);
        outState.putDouble("mLatitude", mLatitude);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mLocManager = (LocationManager)
                getActivity().getSystemService(getActivity().LOCATION_SERVICE);

        // Creating a criteria object to retrieve provider
        Criteria criteria = new Criteria();
        // Getting the name of the best provider
        String provider = mLocManager.getBestProvider(criteria, true);

        // Getting Current Location
        Location location = mLocManager.getLastKnownLocation(provider);
        if (location != null) {
            mLongitude = location.getLongitude();
            mLatitude = location.getLatitude();
            mLocListener = new MyLocationListener();
            mLocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mLocListener);
        }

        mB1 = (Button) getView().findViewById(R.id.make_photo);
        mB1.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                capturePhoto();
            }
        });
        mB2 = (Button) getView().findViewById(R.id.view_photo);
        mB2.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPhotos();
            }
        });
        mB3 = (Button) getView().findViewById(R.id.view_map);
        mB3.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewMap();
            }
        });
    }

    private void capturePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = FileUtils.createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Log.i("log_tag", "Abs path in capture=" + photoFile.getAbsolutePath());

                mPath = Uri.fromFile(photoFile).getPath();
                //mPath=photoFile.toURI().toString();
               /* Log.i("log_tag", "Uri in capture=" + photoFile.toURI());
                Log.i("log_tag", "file name in capture=" + photoFile.getName());
                Log.i("log_tag", "file path in capture=" + photoFile.getPath());*/
                Log.i("log_tag", "mPath in capture=" + mPath);

                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(intent, REQUEST_TAKE_PHOTO);
            }

        }
    }

    private void viewPhotos() {
        ImageGridFragment fragment = new ImageGridFragment();
        getFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(null)
                .commit();
    }

    private void viewMap() {
        MapFragment fragment = new MapFragment();
        getFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // If the request went well (OK) and the request was PICK_CONTACT_REQUEST
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_TAKE_PHOTO:

                    /*String text = "My current location is: " + "Latitude = " + mLatitude + "Longitude = " + mLongitude;
                    Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();*/

                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {
                            SQLiteDatabase db = mDBHelper.getWritableDatabase();
                            synchronized (DbCommonHelper.class) {
                                mPhoto = new Photo(mPath, mLongitude, mLatitude, 0);
                                Log.i("log_tag", "mPhoto in Photo=" + mPhoto.getPath());
                                mId = db.insert(DbCommonHelper.TABLE_PHOTO, null, DbCommonHelper.photoToCv(mPhoto));
                                db.close();
                            }
                            return null;
                        }

                        protected void onPostExecute(Void voids) {

                        }
                    }.execute();

                    if (mLocManager != null) {
                        mLocManager.removeUpdates(mLocListener);
                    }

                    Bundle bundle = new Bundle();
                    bundle.putString("path", mPath);
                    //bundle.putLong("id", mId);
                    bundle.putBoolean("firstTime", true);
                    PreviewFragment fragment = new PreviewFragment();
                    fragment.setArguments(bundle);
                    getFragmentManager().beginTransaction()
                            .replace(R.id.container, fragment)
                            .addToBackStack(null)
                            .commit();
                    break;
                default:
                    break;
            }
        }
    }


    private class MyLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location loc) {
            mLongitude = (float) loc.getLatitude();
            mLatitude = (float) loc.getLongitude();
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {
            /*Toast.makeText(getActivity(), "Enable",
                    Toast.LENGTH_SHORT).show();*/
        }

        @Override
        public void onProviderDisabled(String s) {
          /*  Toast.makeText(getActivity(), "Disable",
                    Toast.LENGTH_SHORT).show();*/
        }
    }
}
