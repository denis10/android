package com.example.fragments;

import android.app.Dialog;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.task10_02denis.R;
import com.example.utils.DbUtils;
import com.example.utils.FileUtils;
import com.example.utils.Photo;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.commons.io.FilenameUtils;

import java.util.ArrayList;

/**
 * Created by dev1 on 2/13/14.
 */
public class MapFragment extends Fragment implements LocationListener, GoogleMap.OnMarkerClickListener {

    private GoogleMap mGoogleMap;
    private SupportMapFragment mFragment;
    private ArrayList<Photo> mDataPhoto;
    private ArrayList<Marker> mMarkers=new ArrayList<Marker>();
    //private boolean mDestroyMap;
    private static View view;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        /*View view = inflater.inflate(R.layout.map_fragment,
                container, false);*/
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.map_fragment, container, false);
        } catch (InflateException e) {
        /* map is already there, just return view as it is */
        }


        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        // Showing status
        if (status != ConnectionResult.SUCCESS) { // Google Play Services are not available
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, getActivity(), requestCode);
            dialog.show();

        } else {
            new AsyncTask<Void, Void, Void>() {
                //ArrayList<Photo> dataPhoto;

                @Override
                protected Void doInBackground(Void... voids) {

                    mDataPhoto = DbUtils.getPhotosFromSD(getActivity());
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    //mDataPhoto = dataPhoto;
                    setUpMap();
                    setLocation();
                    for (int i = 0; i < mDataPhoto.size(); i++) {
                        setMark(new LatLng(mDataPhoto.get(i).getLatitude(),
                                mDataPhoto.get(i).getLongitude()),
                                FilenameUtils.getBaseName(FileUtils.getRealPathFromURI(getActivity(),Uri.parse(mDataPhoto.get(i).getPath()))) +
                                        "." +
                                        FilenameUtils.getExtension(FileUtils.getRealPathFromURI(getActivity(),Uri.parse(mDataPhoto.get(i).getPath())))
                        );
                    }
                }
            }.execute();
        }
    }

    private void setUpMap() {
        if (mGoogleMap == null) {

            mFragment = (SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.map);
            mGoogleMap = mFragment.getMap();

            if (mGoogleMap != null) {
                Log.e("", "Into full map");
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                mGoogleMap.setMyLocationEnabled(true);
                mGoogleMap.setOnMarkerClickListener(this);
            }
        }
    }

    private void setLocation() {
        // Getting LocationManager object from System Service LOCATION_SERVICE
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(getActivity().LOCATION_SERVICE);

        // Creating a criteria object to retrieve provider
        Criteria criteria = new Criteria();

        // Getting the name of the best provider
        String provider = locationManager.getBestProvider(criteria, true);

        // Getting Current Location
        Location location = locationManager.getLastKnownLocation(provider);

        if (location != null) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            LatLng latLng = new LatLng(latitude, longitude);
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            // Zoom in the Google Map
            mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
            onLocationChanged(location);
            locationManager.requestLocationUpdates(provider, 60000, 0, this);
        }
    }

    private void setMark(LatLng latLng, String title) {
        // create marker
        MarkerOptions marker = new MarkerOptions().position(latLng).title(title);
        marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        // adding marker
        mMarkers.add(mGoogleMap.addMarker(marker));
    }

    @Override
    public void onLocationChanged(Location location) {
        // Getting latitude of the current location
        double latitude = location.getLatitude();

        // Getting longitude of the current location
        double longitude = location.getLongitude();

        // Creating a LatLng object for the current location
        LatLng latLng = new LatLng(latitude, longitude);

        // Showing the current location in Google Map
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        // Zoom in the Google Map
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
    }

    public void onDestroyView() {
        super.onDestroyView();
       // if (mDestroyMap){
/*        Fragment fragment = (getActivity().getSupportFragmentManager().findFragmentById(R.id.map));
        android.support.v4.app.FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.remove(fragment);
        ft.commit();*/
    //}
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        //mDestroyMap=true;

        for (int i=0;i<mMarkers.size();i++) {
            if (mMarkers.get(i).equals(marker)) {
                Bundle bundle = new Bundle();
                bundle.putString("path", mDataPhoto.get(i).getPath());
                bundle.putBoolean("firstTime", false);
                bundle.putBoolean("little", true);
                PreviewFragment fragment = new PreviewFragment();
                fragment.setArguments(bundle);
                getFragmentManager().beginTransaction()
                        .replace(R.id.container, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        }
        return false;
    }
}