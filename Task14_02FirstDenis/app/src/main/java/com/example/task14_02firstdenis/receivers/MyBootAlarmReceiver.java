package com.example.task14_02firstdenis.receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.task14_02firstdenis.services.ServiceBootUp;

import java.util.Calendar;

/**
 * Created by denis on 16.02.14.
 */
public class MyBootAlarmReceiver extends BroadcastReceiver {

    // restart service every 30 seconds
    private static final long REPEAT_TIME = 1000 * 30;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("log_tag", " = MyBootAlarmReceiver");
        AlarmManager service = (AlarmManager) context
                .getSystemService(Context.ALARM_SERVICE);

        Intent i = new Intent(context, MyStartBootAlarmReceiver.class);
        PendingIntent pending = PendingIntent.getBroadcast(context, 0, i,
                PendingIntent.FLAG_CANCEL_CURRENT);

        Calendar cal = Calendar.getInstance();
        // start 30 seconds after boot completed
        cal.add(Calendar.SECOND, 10);

        service.set(AlarmManager.RTC_WAKEUP,
                cal.getTimeInMillis(), pending);
        // fetch every 30 seconds
        // InexactRepeating allows Android to optimize the energy consumption
       /* service.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                cal.getTimeInMillis(), REPEAT_TIME, pending);*/

        // service.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
        // REPEAT_TIME, pending);

    }
}

