package com.example.task14_02firstdenis.services;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.example.task14_02firstdenis.app.MainActivity;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Denis on 16.02.14.
 */
public class ServicePending extends Service {
    private int result = Activity.RESULT_CANCELED;
    public static final String RESULT = "result";
    final String LOG_TAG = "log_tag";
    ExecutorService es;

    public void onCreate() {
        super.onCreate();
        es = Executors.newFixedThreadPool(2);
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {

        int value = intent.getExtras().getInt(MainActivity.PARAM_RESULT);
        Log.i(LOG_TAG, "onStart value = " +value);
        PendingIntent pi = intent.getParcelableExtra(MainActivity.PARAM_INTENT);

        MyRun mr = new MyRun(value, startId, pi);
        es.execute(mr);

        return super.onStartCommand(intent, flags, startId);
    }

    public IBinder onBind(Intent arg0) {
        return null;
    }

    class MyRun implements Runnable {

        int value;
        int startId;
        PendingIntent pi;

        public MyRun(int value, int startId, PendingIntent pi) {
            this.value = value;
            this.startId = startId;
            this.pi = pi;
        }

        public void run() {
            try {

                int buf = 1;
                for (int i = 2; i <= value; i++) {
                    buf *= i;
                }
                Log.i(LOG_TAG, "buf = "+buf);

                result = Activity.RESULT_OK;

                Bundle bundle=new Bundle();
                bundle.putLong(MainActivity.PARAM_RESULT, buf);
                //bundle.putInt(RESULT, result);
                Intent intent = new Intent();
                intent.putExtras(bundle);

                pi.send(ServicePending.this, MainActivity.STATUS_FINISH, intent);
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            }
            stop();
        }

        void stop() {

        }
    }
}