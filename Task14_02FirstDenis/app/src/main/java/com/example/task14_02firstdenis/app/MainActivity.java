package com.example.task14_02firstdenis.app;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.task14_02firstdenis.receivers.MyAlarmReceiver;
import com.example.task14_02firstdenis.receivers.MyStartAlarmServiceReceiver;
import com.example.task14_02firstdenis.receivers.PendingBroadcastReceiver;
import com.example.task14_02firstdenis.services.HelloIntentService;
import com.example.task14_02firstdenis.services.ServiceLocalBinding;
import com.example.task14_02firstdenis.services.ServiceNoBinding;
import com.example.task14_02firstdenis.services.ServicePending;

public class MainActivity extends ActionBarActivity {
    public static final String ALARM_NOTIFICATION = "com.example.task14_02firstdenis.app.MainActivity";
    public static final String ACTION = "1";
    final String LOG_TAG = "log_tag";
    public final static String PARAM_INTENT = "pendingIntent";
    public final static int STATUS_FINISH = 200;
    public final static String PARAM_RESULT = "edit";

    public final static int PENDING_CODE = 1;

    private int mNumber;
    private ServiceLocalBinding serviceLocalBinding;

   // private MyAlarmReceiver myAlarmReceiver=new MyAlarmReceiver();

        private BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    long value = bundle.getLong(HelloIntentService.VALUE);
                    int resultCode = bundle.getInt(HelloIntentService.RESULT);
                    if (resultCode == RESULT_OK) {
                        Toast.makeText(MainActivity.this,
                                "Factorial=" + value,
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(MainActivity.this, "please input correct data",
                                Toast.LENGTH_LONG).show();
                    }
                }
            }
        };

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(HelloIntentService.NOTIFICATION));

        Intent intent = new Intent(this, ServiceLocalBinding.class);
        bindService(intent, mConnection,
                Context.BIND_AUTO_CREATE);

        /*registerReceiver(myAlarmReceiver, new IntentFilter(
                HelloIntentService.NOTIFICATION));*/

    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);

        unbindService(mConnection);
       /* unregisterReceiver(myAlarmReceiver);*/

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button b1 = (Button) findViewById(R.id.btn1);
        b1.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText edt1 = (EditText) findViewById(R.id.edt1);
                if (checkInput(edt1.getText().toString())) {
                    Intent intent = new Intent(MainActivity.this, HelloIntentService.class);
                    intent.putExtra("edit", mNumber);
                    startService(intent);
                }
            }
        });

        Button b2 = (Button) findViewById(R.id.btn2);
        b2.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText edt1 = (EditText) findViewById(R.id.edt1);
                if (checkInput(edt1.getText().toString())) {
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(MainActivity.this, ServiceNoBinding.class);
                            intent.putExtra("edit", mNumber);
                            startService(intent);
                        }
                    };
                    new Thread(runnable).start();
                }
            }
        });
        Button b3 = (Button) findViewById(R.id.btn3);
        b3.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText edt1 = (EditText) findViewById(R.id.edt1);
                if (checkInput(edt1.getText().toString())) {
                    Toast.makeText(MainActivity.this, "factorial=" + serviceLocalBinding.getFactorial(mNumber),
                            Toast.LENGTH_LONG).show();
                }
            }
        });

        Button b4 = (Button) findViewById(R.id.btn4);
        b4.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText edt1 = (EditText) findViewById(R.id.edt1);
                if (checkInput(edt1.getText().toString())) {
                    /*//this is the intent that will be broadcasted by service.
                    Intent broadcastReceiverIntent = new Intent(getApplicationContext(), PendingBroadcastReceiver.class);
                    //create pending intent for broadcasting the PendingBroadcastReceiver
                    PendingIntent pi = PendingIntent.getBroadcast(getApplicationContext(), 0, broadcastReceiverIntent, 0);*/
                    PendingIntent pi = createPendingResult(PENDING_CODE, new Intent(), 0);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(PARAM_INTENT, pi);
                    //we want to start our service (for handling our time-consuming operation)
                    Intent serviceIntent = new Intent(getApplicationContext(), ServicePending.class);
                    serviceIntent.putExtras(bundle).putExtra(PARAM_RESULT, mNumber);;
                    startService(serviceIntent);
                }
            }
        });
        Button b5 = (Button) findViewById(R.id.btn5);
        b5.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText edt1 = (EditText) findViewById(R.id.edt1);
                if (checkInput(edt1.getText().toString())) {
                    Intent intent = new Intent(ALARM_NOTIFICATION);
                    intent.putExtra("edit", mNumber);
                    sendBroadcast(intent);
                }
            }
        });
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className,
                                       IBinder binder) {
            ServiceLocalBinding.MyBinder b = (ServiceLocalBinding.MyBinder) binder;
            serviceLocalBinding = b.getService();
            /*Toast.makeText(MainActivity.this, "Connected", Toast.LENGTH_SHORT)
                    .show();*/
        }

        public void onServiceDisconnected(ComponentName className) {
            serviceLocalBinding = null;
        }
    };

    private boolean checkInput(String edit) {
        //boolean res=false;
        try {
            mNumber = Integer.parseInt(edit);
            return true;
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Please put correct data", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("log_tag", "requestCode = " + requestCode + ", resultCode = "
                + resultCode);
        Bundle bundle = data.getExtras();
        if (bundle != null) {
            //int resultCode = bundle.getInt(HelloIntentService.RESULT);
            long factorial = bundle.getLong(MainActivity.PARAM_RESULT);
            //int resultCode = bundle.getInt(HelloIntentService.RESULT);
            Log.i("log_tag", "factorial = " + factorial + ", resultCode = "
                    + resultCode);
            if (resultCode == STATUS_FINISH) {
                switch (requestCode) {
                    case PENDING_CODE:
                        Toast.makeText(getApplicationContext(), "Result pending=" + factorial, Toast.LENGTH_SHORT).show();
                        break;
                }
            }

        }
    }
}


/*

    http://www.vogella.com/tutorials/AndroidServices/article.html
    http://www.techotopia.com/index.php/Android_Local_Bound_Services_%E2%80%93_A_Worked_Example
    http://startandroid.ru/ru/uroki/vse-uroki-spiskom/163-urok-98-service-lokalnyj-binding
    http://stackoverflow.com/questions/9152924/android-start-service-with-context-startservice-vs-pendingintent-getservice
    http://startandroid.ru/ru/uroki/vse-uroki-spiskom/160-urok-95-service-obratnaja-svjaz-s-pomoschju-pendingintent
    http://stackoverflow.com/questions/6099364/how-to-use-pendingintent-to-communicate-from-a-service-to-a-client-activity

*/