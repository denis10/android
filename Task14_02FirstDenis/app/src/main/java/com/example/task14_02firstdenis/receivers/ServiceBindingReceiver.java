package com.example.task14_02firstdenis.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.task14_02firstdenis.services.ServiceLocalBinding;

/**
 * Created by Denis on 16.02.14.
 */
public class ServiceBindingReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent service = new Intent(context, ServiceLocalBinding.class);
        context.startService(service);
    }
}