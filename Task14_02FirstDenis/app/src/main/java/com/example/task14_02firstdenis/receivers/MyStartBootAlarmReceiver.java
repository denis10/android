package com.example.task14_02firstdenis.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.example.task14_02firstdenis.services.ServiceBootUp;
import com.example.task14_02firstdenis.services.ServiceLocalBinding;

/**
 * Created by denis on 16.02.14.
 */
public class MyStartBootAlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("log_tag", " = MyStartBootAlarmReceiver");
        Intent service = new Intent(context, ServiceBootUp.class);
        service.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        context.startService(service);
    }
}