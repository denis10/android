package com.example.task14_02firstdenis.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.example.task14_02firstdenis.app.R;

/**
 * Created by Denis on 16.02.14.
 */
public class ServiceLocalBinding extends Service {
    private final IBinder mBinder = new MyBinder();


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("log_tag", " onStartCommand ServiceLocalBinding");
        return Service.START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return mBinder;
    }

    public static long getFactorial(int number) {
        int buf = 1;
        for (int i = 2; i <= number; i++) {
            buf *= i;
        }
        return buf;
    }

    public class MyBinder extends Binder {
        public ServiceLocalBinding getService() {
            return ServiceLocalBinding.this;
        }
    }
}