package com.example.task14_02firstdenis.services;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Denis on 15.02.14.
 */

public class HelloIntentService extends IntentService {
    public static final String NOTIFICATION = "com.example.task14_02firstdenis.app";
    private int result = Activity.RESULT_CANCELED;
    public static final String RESULT = "result";
    public static final String VALUE = "value";

    /**
     * A constructor is required, and must call the super IntentService(String)
     * constructor with a name for the worker thread.
     */
    public HelloIntentService() {
        super("HelloIntentService");
    }

    /**
     * The IntentService calls this method from the default worker thread with
     * the intent that started the service. When this method returns, IntentService
     * stops the service, as appropriate.
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        // Toast.makeText(this, "onHandleIntent", Toast.LENGTH_SHORT).show();
        long buf = 1;
        //String editValue = intent.getStringExtra("edit");
        int N = intent.getExtras().getInt("edit");
        for (int i = 2; i <= N; i++) {
            buf *= i;
        }
        result = Activity.RESULT_OK;
        publishResults(buf, result);
    }

    private void publishResults(long fac, int result) {
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(VALUE, fac);
        intent.putExtra(RESULT, result);
        sendBroadcast(intent);
    }
}