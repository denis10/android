package com.example.task14_02firstdenis.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.widget.EditText;
import android.widget.Toast;

import com.example.task14_02firstdenis.app.R;
import com.example.task14_02firstdenis.services.ServiceLocalBinding;
import com.example.task14_02firstdenis.services.ServiceNoBinding;

/**
 * Created by denis on 16.02.14.
 */
public class MyStartAlarmServiceReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("log_tag", " = MyStartAlarmServiceReceiver");
        Intent service = new Intent(context, ServiceLocalBinding.class);
        service.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //intent.putExtra("edit", 5);
        int N = intent.getExtras().getInt("edit");
        Toast toast=new Toast(context);
        toast.setGravity(Gravity.TOP|Gravity.LEFT, 0, 0);

        toast.makeText(context, "alarm factorial=" + ServiceLocalBinding.getFactorial(N),
                Toast.LENGTH_SHORT).show();
        context.startService(service);
    }
}