package com.example.task2_1denis;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.Button;

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button b1=(Button)findViewById(R.id.button1);
        b1.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View view) {
                dial(MainActivity.this);
            }
        });
        Button b2=(Button)findViewById(R.id.button2);
        b2.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View view) {
                sendSMS(MainActivity.this);
            }
        });
        Button b3=(Button)findViewById(R.id.button3);
        b3.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View view) {
                sendEmail(MainActivity.this);
            }
        });
        Button b4=(Button)findViewById(R.id.button4);
        b4.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View view) {
                openBrowser(MainActivity.this);
            }
        });
    }

    public void dial(Activity activity) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        activity.startActivity(intent);
    }

    public void sendSMS(Activity activity) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("sms:"));
        activity.startActivity(intent);
    }
    public void sendEmail(Activity activity) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("plain/text");
        activity.startActivity(Intent.createChooser(intent, "Send mail..."));
    }
    public void openBrowser(Activity activity) {
        String url = "http://www.google.com";
        Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(url));
        activity.startActivity(intent);

    }
 }
