package com.example.task3_2denis;

import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }


    public void onCalcButtonClick(View view){
        new CalculateFactorial().execute("");
    }
    private class CalculateFactorial extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... strings) {
            EditText edit1=(EditText)findViewById(R.id.edt1);
            int N=0;
            boolean correctNumber=false;
            try{
                N=Integer.parseInt(edit1.getText().toString());
                correctNumber=true;
            }catch (Exception e){
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Please put correct data", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            if(correctNumber){
                int buf=1;
                for (int i=2;i<=N;i++){
                    buf*=i;
                }
                return ""+buf;
            }
            return null;
        }
        protected void onPostExecute(String result) {
            TextView v1 = (TextView) findViewById(R.id.view1);
            v1.setText(result);
        }

    }
 }
