package com.example.task3_4denis;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
    private static TextView v1;
    private CalculateFactorial cf;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        v1 = (TextView) findViewById(R.id.view1);


        cf = (CalculateFactorial) getLastNonConfigurationInstance();
        if (cf != null) {
            //Toast.makeText(getApplicationContext(), cf == null?"null":"not null", Toast.LENGTH_SHORT).show();
            cf.link(this);
        }


    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("view_txt", v1.getText().toString());

        super.onSaveInstanceState(outState);
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {

        v1.setText(savedInstanceState.getString("view_txt"));

        super.onRestoreInstanceState(savedInstanceState);
    }

    public Object onRetainNonConfigurationInstance() {
        if(cf!=null){
            cf.unLink();
        }
        return cf;
    }

    public void onCalcButtonClick(View view){
        v1.setText("");
        cf = (CalculateFactorial) getLastNonConfigurationInstance();
        if (cf == null) {
            cf =(CalculateFactorial) new CalculateFactorial().execute("");
        }
        cf.link(this);
    }
    private static class CalculateFactorial extends AsyncTask<String, Void, String> {

        MainActivity activity;

        void link(MainActivity act) {
            activity = act;
        }

        void unLink() {
            activity = null;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "Success!";
        }
        protected void onPostExecute(String result) {
            v1.setText(result);
        }

    }
}
