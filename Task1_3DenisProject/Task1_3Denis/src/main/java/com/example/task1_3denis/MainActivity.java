package com.example.task1_3denis;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.CheckBox;

public class MainActivity extends ActionBarActivity {
private CheckBox ch1;
private CheckBox ch2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ch1=(CheckBox)findViewById(R.id.check1);
        ch2=(CheckBox)findViewById(R.id.check2);

    }




    @Override
    public void onSaveInstanceState(Bundle outState) {
        //CheckBox ch1=(CheckBox)findViewById(R.id.check1);
        outState.putBoolean("checkbox1", ch1.isChecked());
        outState.putBoolean("checkbox2", ch2.isChecked());
        super.onSaveInstanceState(outState);
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
       // if (savedInstanceState != null && savedInstanceState.containsKey("checkbox1")) {
            //CheckBox ch1=(CheckBox)findViewById(R.id.check1);
            ch1.setChecked(savedInstanceState.getBoolean("checkbox1"));
            ch2.setChecked(savedInstanceState.getBoolean("checkbox2"));

        //}
        super.onRestoreInstanceState(savedInstanceState);
    }


}
