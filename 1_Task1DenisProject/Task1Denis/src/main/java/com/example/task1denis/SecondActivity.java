package com.example.task1denis;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SecondActivity extends ActionBarActivity {

    Button b2;
    Intent firstIntent;
    EditText editTxt1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_main);
        editTxt1=(EditText)findViewById(R.id.edt_txt1);

        //final Bundle sendBundle = new Bundle();
        //final Bundle sendBundle = this.getIntent().getExtras();

        b2=(Button)findViewById(R.id.button2);
        b2.setOnClickListener(new View.OnClickListener()
        {   public void onClick(View v)
            {
                /*firstIntent = new Intent(SecondActivity.this, task1.class);
                sendBundle.putString("value", editTxt1.getText().toString());
                firstIntent.putExtras(sendBundle);
                startActivity(firstIntent);*/
                Intent intent = new Intent();
                intent.putExtra("value", editTxt1.getText().toString());
                setResult(RESULT_OK, intent);
                finish();


            }
        });

    }
}
