package com.example.task1denis;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class task1 extends ActionBarActivity {
private final int REQUEST_CODE=1;
    Button b1;
    Intent secondIntent;
    TextView txtView1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*Bundle receiveBundle = this.getIntent().getExtras();
        if(receiveBundle!=null)
        {
            final String receiveValue = receiveBundle.getString("value","");
            Toast.makeText(getApplicationContext(), receiveBundle.getString("value",""), Toast.LENGTH_SHORT).show();
            txtView1=(TextView)findViewById(R.id.txt_view1);
            txtView1.setText(receiveValue);
        }*/



        b1=(Button)findViewById(R.id.button1);
        b1.setOnClickListener(new View.OnClickListener()
        {   public void onClick(View v)
            {
                secondIntent = new Intent(task1.this, SecondActivity.class);
                startActivityForResult(secondIntent,REQUEST_CODE);
                //finish();
            }
        });


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK ){

            txtView1=(TextView)findViewById(R.id.txt_view1);
            txtView1.setText(data.getStringExtra("value"));

        }
    }
}
