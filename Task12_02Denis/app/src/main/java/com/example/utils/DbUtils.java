package com.example.utils;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.helpers.DbCommonHelper;

import java.util.ArrayList;

/**
 * Created by dev1 on 2/12/14.
 */
public class DbUtils {
    private static DbCommonHelper mDBHelper;


    public static Photo getPhotoObject(Context context, String mPath) {
        Photo photo = new Photo();

        mDBHelper = new DbCommonHelper(context);
        SQLiteDatabase db = mDBHelper.getWritableDatabase();
        synchronized (DbCommonHelper.class) {
            String query = "SELECT * FROM " + DbCommonHelper.TABLE_PHOTO +
                    " WHERE " + DbCommonHelper.PATH + "=?" +
                    ";";

            Cursor c = db.rawQuery(query, new String[]{mPath});
            if (c != null) {
                if (c.moveToFirst()) {
                    int idColIndex = c.getColumnIndex("_id");
                    int pathIndex = c.getColumnIndex("path");
                    int longitudeIndex = c.getColumnIndex("longitude");
                    int latitudeIndex = c.getColumnIndex("latitude");
                    int loadedColumn = c.getColumnIndex("loaded");
                    //do {
                    photo = new Photo(c.getLong(idColIndex), c.getString(pathIndex), c.getFloat(longitudeIndex), c.getFloat(latitudeIndex), c.getInt(loadedColumn));
                    //} while (c.moveToNext());

                } else {
                    Log.d("log", "0 rows");
                }
                c.close();
            }

            db.close();
        }
        return photo;
    }

    public static ArrayList<String> getPathesFromDB(Context context) {
        ArrayList<String> pathPhoto = new ArrayList<String>();

        mDBHelper = new DbCommonHelper(context);
        SQLiteDatabase db = mDBHelper.getWritableDatabase();
        synchronized (DbCommonHelper.class) {
            String query = "SELECT * FROM " + DbCommonHelper.TABLE_PHOTO +
                    ";";

            Cursor c = db.rawQuery(query, null);
            if (c != null) {
                if (c.moveToFirst()) {
                    int pathIndex = c.getColumnIndex("path");
                    do {
                        pathPhoto.add(c.getString(pathIndex));
                    } while (c.moveToNext());

                } else {
                    Log.d("log", "0 rows");
                }
                c.close();
            }

            db.close();
        }
        return pathPhoto;
    }

    public static ArrayList<Photo> getPhotosFromDB(Context context) {
        ArrayList<Photo> dataPhoto = new ArrayList<Photo>();

        mDBHelper = new DbCommonHelper(context);
        SQLiteDatabase db = mDBHelper.getWritableDatabase();
        synchronized (DbCommonHelper.class) {
            String query = "SELECT * FROM " + DbCommonHelper.TABLE_PHOTO +
                    ";";

            Cursor c = db.rawQuery(query, null);
            if (c != null) {
                if (c.moveToFirst()) {
                    int idColIndex = c.getColumnIndex("_id");
                    int pathIndex = c.getColumnIndex("path");
                    int longitudeIndex = c.getColumnIndex("longitude");
                    int latitudeIndex = c.getColumnIndex("latitude");
                    int loadedColumn = c.getColumnIndex("loaded");
                    do {
                        dataPhoto.add(new Photo(c.getLong(idColIndex), c.getString(pathIndex), c.getFloat(longitudeIndex), c.getFloat(latitudeIndex), c.getInt(loadedColumn)));
                    } while (c.moveToNext());

                } else {
                    Log.d("log", "0 rows");
                }
                c.close();
            }

            db.close();
        }
        return dataPhoto;
    }

    public static void setLoaded(Context context, String path) {
        final String mPath = path;
        mDBHelper = new DbCommonHelper(context);
        SQLiteDatabase db = mDBHelper.getWritableDatabase();
        synchronized (DbCommonHelper.class) {
            db.execSQL("UPDATE " + DbCommonHelper.TABLE_PHOTO + " SET " + DbCommonHelper.LOADED + "=" + Photo.LOADED +
                    " WHERE " + DbCommonHelper.PATH + "=?" + "", new String[]{mPath});
            db.close();
        }
    }
}
