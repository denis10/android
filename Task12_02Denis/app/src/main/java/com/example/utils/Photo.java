package com.example.utils;

/**
 * Created by dev1 on 2/10/14.
 */
public class Photo {
    public static int LOADED=1;

    private long id;
    private String path;
    private double longitude;
    private double latitude;
    private boolean loaded;
    private String url;

    public Photo(){

    }
    public Photo(String path,double longtitude,double latitude,String url){
       this.path=path;
       this.url=url;
    }

    public Photo(String path,double longtitude,double latitude,int loaded){
        this.path=path;
        this.longitude =longtitude;
        this.latitude=latitude;
        if (loaded==1){
            this.loaded=true;
        }else{
            this.loaded=false;
        }
    }

    public Photo(long id,String path,double longtitude,double latitude,int loaded){
        this.id=id;
        this.path=path;
        this.longitude =longtitude;
        this.latitude=latitude;
        if (loaded==1){
            this.loaded=true;
        }else{
            this.loaded=false;
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public double getLongitude() {
        return longitude;
    }


    public double getLatitude() {
        return latitude;
    }


    public int getLoaded() {
        if (loaded){
            return 1;
        }
        return 0;
    }

    public void setLoaded(boolean loaded) {
        this.loaded = loaded;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
