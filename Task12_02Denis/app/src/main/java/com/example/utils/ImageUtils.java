package com.example.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.helpers.DbCommonHelper;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by dev1 on 2/12/14.
 */
public class ImageUtils {
    private static DbCommonHelper mDBHelper;

    public static Bitmap decodeSampledBitmapFromResourceMemOpt(
            InputStream inputStream, int reqWidth, int reqHeight) {

        byte[] byteArr = new byte[0];
        byte[] buffer = new byte[16000];
        int len;
        int count = 0;

        try {
            while ((len = inputStream.read(buffer)) > -1) {
                if (len != 0) {
                    if (count + len > byteArr.length) {
                        byte[] newbuf = new byte[(count + len) * 2];
                        System.arraycopy(byteArr, 0, newbuf, 0, count);
                        byteArr = newbuf;
                        newbuf = null;
                    }

                    System.arraycopy(buffer, 0, byteArr, count, len);
                    count += len;
                }
            }
            buffer = null;
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(byteArr, 0, count, options);

            options.inSampleSize = calculateInSampleSize(options, reqWidth,
                    reqHeight);
            options.inPurgeable = true;
            options.inInputShareable = true;
            options.inJustDecodeBounds = false;
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;

            return BitmapFactory.decodeByteArray(byteArr, 0, count, options);

        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }

    public static Bitmap getImage(String file, int reqWidth, int reqHeight) {
        // Log.i("log_tag", "mPath in getImage=" + file);
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        options.inJustDecodeBounds = false;

        Bitmap bitmap = BitmapFactory.decodeFile(file, options);
        //Log.i("log_tag", "bitmap in getImage=" + (bitmap != null ? true : false));
        return BitmapFactory.decodeFile(file, options);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        return inSampleSize;
    }

    public static void postImageUpload(String url, Bitmap bmp, String mPath, Context context) {
        uploadPhoto(url, bmp, mPath, context);
    }

    private static void uploadPhoto(String url, Bitmap bmp, String mPath, Context context) {
        ByteArrayOutputStream bao = new ByteArrayOutputStream();

        bmp.compress(Bitmap.CompressFormat.JPEG, 95, bao);
        try {
            int timeout = 100000;
            byte[] data = bao.toByteArray();
            //http://hmkcode.com/android-send-json-data-to-server/
            // 1. create HttpClient
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeout);
            HttpConnectionParams.setSoTimeout(httpParameters, timeout);
            HttpClient httpClient = new DefaultHttpClient(httpParameters);

            // 2. make POST request to the given URL
            HttpPost postRequest = new HttpPost(url);

            postRequest.setEntity(new ByteArrayEntity(data));
            // 7. Set some headers to inform server about the type of the content
            /*postRequest.setHeader("Accept", "application/json");
            postRequest.setHeader("Content-type", "application/json");*/
            //
            postRequest.setHeader("X-Parse-Application-Id", "EJeLp1mwTNMWfPn6JBCEcUUtdj3TuNER7pCK3INU");
            postRequest.setHeader("X-Parse-REST-API-Key", "fp0yGLN6hiHnkwtTTQRE1dpJY53I5ZN3gq9GfnLC");
            postRequest.setHeader("Content-Type:", "image/jpeg");

            HttpResponse response = httpClient.execute(postRequest);

            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            String sResponse;
            StringBuilder s = new StringBuilder();
            while ((sResponse = reader.readLine()) != null) {
                s = s.append(sResponse);
            }
            // Log.i("log_tag", "Send response:\n" + s);
            String photoUrl = s.substring(s.indexOf("http"), s.indexOf(".jpg") + 4);
            Log.i("log_tag", "photoUrl:\n" + photoUrl);

            DbUtils.setLoaded(context, mPath);

            uploadObject(photoUrl, mPath, context);

        } catch (Exception e) {

            Log.e("log_tag", "Error in http connection " + e.toString());
        }
    }

    private static void uploadObject(String photoUrl, String mPath, Context context) {
        try {
            int timeout = 100000;
            Photo photo = DbUtils.getPhotoObject(context, mPath);
            JSONObject jsonObject = new JSONObject();
            String url = "https://api.parse.com/1/classes/" + jsonObject.getClass().getSimpleName();
            // 1. create HttpClient
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeout);
            HttpConnectionParams.setSoTimeout(httpParameters, timeout);
            HttpClient httpClient = new DefaultHttpClient(httpParameters);

            // 2. make POST request to the given URL
            HttpPost postRequest = new HttpPost(url);

            String json = "";

            // 3. build jsonObject

            jsonObject.accumulate("id", photo.getId());
            jsonObject.accumulate("path", photo.getPath());
            jsonObject.accumulate("longitude", photo.getLongitude());
            jsonObject.accumulate("latitude", photo.getLatitude());
            photo.setLoaded(true);
            jsonObject.accumulate("loaded", photo.getLoaded());
            jsonObject.accumulate("url", photoUrl);

            // 4. convert JSONObject to JSON to String
            json = jsonObject.toString();

            // 5. set json to StringEntity
            StringEntity se = new StringEntity(json);

            // 6. set httpPost Entity
            postRequest.setEntity(se);

            //postRequest.setEntity(new ByteArrayEntity(data));
            // 7. Set some headers to inform server about the type of the content
            postRequest.setHeader("Accept", "application/json");
            postRequest.setHeader("Content-type", "application/json");
            //
            postRequest.setHeader("X-Parse-Application-Id", "EJeLp1mwTNMWfPn6JBCEcUUtdj3TuNER7pCK3INU");
            postRequest.setHeader("X-Parse-REST-API-Key", "fp0yGLN6hiHnkwtTTQRE1dpJY53I5ZN3gq9GfnLC");
            //postRequest.setHeader("Content-Type:", "image/jpeg");

            HttpResponse response = httpClient.execute(postRequest);

            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            String sResponse;
            StringBuilder s = new StringBuilder();
            while ((sResponse = reader.readLine()) != null) {
                s = s.append(sResponse);
            }
            Log.i("log_tag", "Send response:\n" + s);

        } catch (Exception e) {
            Log.e("log_tag", "Error in http connection " + e.toString());
        }
    }

    public static void uploadAll(Context context2) {
        final String addressString = "https://api.parse.com/1/files/";
        final ArrayList<String> uploadPhoto = new ArrayList<String>();
        final Context context = context2;
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                ArrayList<Photo> dataPhoto = DbUtils.getPhotosFromDB(context);
                File file;
                for (int i = 0; i < dataPhoto.size(); i++) {
                    //file=new File(Uri.parse(imageUrls[i]).getPath());
                    file = new File(dataPhoto.get(i).getPath());
                    if (file.exists()) {
                        if (dataPhoto.get(i).getLoaded() == 0) {
                            uploadPhoto.add(dataPhoto.get(i).getPath());
                        }
                    }
                }

                for (int i = 0; i < uploadPhoto.size(); i++) {
                    postImageUpload(addressString + new File(Uri.parse(uploadPhoto.get(i)).getPath()).getName(),
                            ImageUtils.getBitmap(context, uploadPhoto.get(i)),
                            uploadPhoto.get(i),
                            context);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Toast.makeText(context, "All photos uploaded", Toast.LENGTH_SHORT).show();
            }
        }.execute();

    }

    public static void getSynchronize(Context context) {
        final Context contextInner = context;
        final ArrayList<String> pathPhoto = DbUtils.getPathesFromDB(context);
        //final String[] imageUrls = pathPhoto.toArray(new String[pathPhoto.size()]);
        final ArrayList<String> pathUpdatePhoto = new ArrayList<String>();
        final ArrayList<Photo> updatePhoto = new ArrayList<Photo>();
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                File file;
                mDBHelper = new DbCommonHelper(contextInner);
                int timeout = 100000;
                // 1. create HttpClient
                HttpParams httpParameters = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(httpParameters, timeout);
                HttpConnectionParams.setSoTimeout(httpParameters, timeout);
                HttpClient httpClient = new DefaultHttpClient(httpParameters);

                JSONObject jsonObject = new JSONObject();
                String url = "https://api.parse.com/1/classes/" + jsonObject.getClass().getSimpleName();

                HttpGet get = new HttpGet(url);
                get.setHeader("Accept", "application/json");
                get.setHeader("Content-type", "application/json");
                //
                get.setHeader("X-Parse-Application-Id", "EJeLp1mwTNMWfPn6JBCEcUUtdj3TuNER7pCK3INU");
                get.setHeader("X-Parse-REST-API-Key", "fp0yGLN6hiHnkwtTTQRE1dpJY53I5ZN3gq9GfnLC");

                HttpResponse getResponse = null;
                try {
                    getResponse = httpClient.execute(get);
                } catch (ClientProtocolException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (getResponse != null) {
                    HttpEntity responseEntity = getResponse.getEntity();

                    BufferedHttpEntity httpEntity = null;
                    BufferedReader reader = null;
                    String sResponse;
                    StringBuilder s = new StringBuilder();
                    try {
                        httpEntity = new BufferedHttpEntity(responseEntity);
                        reader = new BufferedReader(new InputStreamReader(httpEntity.getContent(), "UTF-8"));
                        while ((sResponse = reader.readLine()) != null) {
                            s = s.append(sResponse);
                        }
                    } catch (IOException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    //Log.i("log_tag", "get responce:\n" + s);

                    JSONArray jsonArray = null;
                    try {
                        JSONObject obj = new JSONObject(s.toString());
                        jsonArray = obj.getJSONArray("results");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                            file = new File(jsonObject2.getString("path"));
                            if (file.exists()) {
                                boolean insert = true;
                                for (int j = 0; j < pathPhoto.size(); j++) {
                                    if (jsonObject2.getString("path").equals(pathPhoto.get(j))) {
                                        insert = false;
                                        break;
                                    }
                                }
                                if (insert) {
                                    SQLiteDatabase db = mDBHelper.getWritableDatabase();
                                    synchronized (DbCommonHelper.class) {
                                        Photo mPhoto = new Photo(jsonObject2.getString("path"), jsonObject2.getDouble("longitude"),
                                                jsonObject2.getDouble("longitude"), 1);
                                        //Log.i("log_tag", "mPhoto in Photo=" + mPhoto.getPath());
                                        db.insert(DbCommonHelper.TABLE_PHOTO, null, DbCommonHelper.photoToCv(mPhoto));
                                        db.close();
                                    }
                                }
                            } else {
                                updatePhoto.add(new Photo(jsonObject2.getString("path"), jsonObject2.getDouble("longitude"),
                                        jsonObject2.getDouble("longitude"), jsonObject2.getString("url")));
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.w("log_tag", "" + e.getLocalizedMessage());
                    }
                }


             /*   StringBuilder buf=new StringBuilder();
                String photoPath;// = s.substring(s.indexOf("path") + 7, s.indexOf(".jpg") + 4);

                String path="path";
                Pattern p = Pattern.compile(path);
                Matcher m = p.matcher(s);
                int counter = 0;
                while(m.find()) {
                    //Log.i("log_tag", "index photo:\n" + m.start());
                    photoPath = s.substring(m.start(), s.substring(m.start()).indexOf(".jpg") + 4);
                    Log.i("log_tag", "get pathFromSitePhoto:\n" + photoPath);
                    counter++;
                }*/

                //Log.i("log_tag", "counter photo:\n" + counter);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                //Log.i("log_tag", "getSynchronize onPostExecute");
                downloadPhotos(contextInner, updatePhoto);
            }
        }.execute();
    }

    private static void downloadPhotos(Context context, ArrayList<Photo> photos) {
        final Context contextInner = context;
        final ArrayList<String> pathPhoto = DbUtils.getPathesFromDB(context);
        //Log.i("log_tag", "downloadPhotos");
        final ArrayList<Photo> updatePhoto = photos;
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                // Log.i("log_tag", "downloadPhotos doInBackground");
                mDBHelper = new DbCommonHelper(contextInner);
                for (Photo photo : updatePhoto) {
                    Bitmap img;
                    HttpURLConnection connection = null;
                    try {
                        URL url = new URL(photo.getUrl());
                        connection = (HttpURLConnection) url.openConnection();
                        InputStream is = connection.getInputStream();
                        img = BitmapFactory.decodeStream(is);

                        // Log.i("log_tag", "file path=" + new File(photo.getPath()).getAbsolutePath());

                        File file = new File(new File(photo.getPath()).getAbsolutePath());
                        if (file.exists()) {
                            file.delete();
                        }
                        FileOutputStream out = new FileOutputStream(file);
                        img.compress(Bitmap.CompressFormat.JPEG, 100, out);
                        out.flush();
                        out.close();

                        SQLiteDatabase db = mDBHelper.getWritableDatabase();
                        boolean insert = true;
                        for (int j = 0; j < pathPhoto.size(); j++) {
                            if (photo.getPath().equals(pathPhoto.get(j))) {
                                insert = false;
                                break;
                            }
                        }
                        if (insert) {
                            synchronized (DbCommonHelper.class) {
                                Photo mPhoto = new Photo(photo.getPath(), photo.getLongitude(), photo.getLongitude(), 1);
                                //Log.i("log_tag", "mPhoto in Photo=" + mPhoto.getPath());
                                db.insert(DbCommonHelper.TABLE_PHOTO, null, DbCommonHelper.photoToCv(mPhoto));
                                db.close();
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                Toast.makeText(contextInner, "Synchronized", Toast.LENGTH_SHORT).show();
            }
        }.execute();
    }

    public static Bitmap getBitmap(Context context, String path) {
        int height = 50;
        int width = 50;
        Bitmap bitmap = ImageUtils.getImage(FileUtils.getRealPathFromURI(context, Uri.parse(path)), height, width);
        return bitmap;
    }
}

