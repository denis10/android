package com.example.utils;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

//import android.net.Uri;

/**
 * Created by dev1 on 2/13/14.
 */
public class FileUtils {

    public static String getRealPathFromURI(Context context,Uri contentURI) {
        Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            //Log.i("log_tag", "getRealPathFromURI (cursor null)=" +contentURI.getPath() );
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
           // Log.i("log_tag", "getRealPathFromURI=" +cursor.getString(idx) );
            return cursor.getString(idx);
        }
    }

    //Convert the image URI to the direct file system path of the image file
    /*public static String getRealPathFromURI(String contentUri) {

        Log.i("log_tag", "getRealPathFromURI=" + Uri.parse(contentUri).getPath());
        return Uri.parse(contentUri).getPath();

    }*/
    public static int count=0;
    public static File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        //File storageDir = new File("/storage/ext_sd/DCIM");
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        //mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }
}
