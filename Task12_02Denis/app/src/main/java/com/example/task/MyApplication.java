package com.example.task;

import android.app.Application;

import com.example.utils.ImageUtils;

/**
 * Created by dev1 on 2/21/14.
 */
public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        ImageUtils.getSynchronize(getApplicationContext());
    }
}
