package com.example.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.utils.Photo;

/**
 * Created by dev1 on 2/4/14.
 */
public class DbCommonHelper extends SQLiteOpenHelper {
    public static final int DB_VERSION = 1;
    public static final String DB_NAME = "m10_02_01.db";

    public static final String TABLE_PHOTO = "photo";
    public static final String PATH = "path";
    public static final String LONGITUDE = "longitude";
    public static final String LATITUDE = "latitude";
    public static final String LOADED = "loaded";

    private static final String CREATE_TABLE_PHOTOS = "CREATE TABLE " + TABLE_PHOTO + " (_id integer primary key autoincrement, "
            + PATH + " TEXT, "
            + LONGITUDE + " REAL, "
            + LATITUDE + " REAL, "
            + LOADED + " INTEGER"
            +");";

    public DbCommonHelper(Context context) {
        super(context, DB_NAME, null,DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_PHOTOS);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS" + TABLE_PHOTO);
        onCreate(sqLiteDatabase);
    }

    public static ContentValues photoToCv(Photo p){
        ContentValues cv = new ContentValues();
        cv.put(DbCommonHelper.PATH,p.getPath());
        cv.put(DbCommonHelper.LONGITUDE,p.getLongitude());
        cv.put(DbCommonHelper.LATITUDE,p.getLatitude());
        cv.put(DbCommonHelper.LOADED,p.getLoaded());
        return cv;
    }

}
