package com.example.fragments;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.task.R;
import com.example.utils.DbUtils;
import com.example.utils.FileUtils;
import com.example.utils.ImageUtils;
import com.example.utils.Photo;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.commons.io.FilenameUtils;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by dev1 on 2/13/14.
 */
public class MapFragment extends Fragment implements LocationListener, GoogleMap.OnInfoWindowClickListener {

    private static View view;
    private GoogleMap mGoogleMap;
    private SupportMapFragment mFragment;
    private ArrayList<Photo> mDataPhoto;
    private ArrayList<Marker> mMarkers = new ArrayList<Marker>();
    //private boolean mDestroyMap;
    private ActionBar actionBar;
    Hashtable<String, String> markers=new Hashtable<String, String>();

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        /*View view = inflater.inflate(R.layout.map_fragment,
                container, false);*/
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.map_fragment, container, false);
        } catch (InflateException e) {
        /* map is already there, just return view as it is */
        }
        actionBar = ((ActionBarActivity) getActivity()).getSupportActionBar();
        actionBar.setTitle(this.getClass().getSimpleName());

        return view;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());

        // Showing status
        if (status != ConnectionResult.SUCCESS) { // Google Play Services are not available
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, getActivity(), requestCode);
            dialog.show();
        } else {
            if (mGoogleMap == null) {
                new AsyncTask<Void, Void, Void>() {
                    //ArrayList<Photo> dataPhoto;

                    @Override
                    protected Void doInBackground(Void... voids) {
                        mDataPhoto = DbUtils.getPhotosFromDB(getActivity());
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {

                        setUpMap();
                        setLocation();

                        for (int i = 0; i < mDataPhoto.size(); i++) {
                            setMark(new LatLng(mDataPhoto.get(i).getLatitude(),
                                    mDataPhoto.get(i).getLongitude()),
                                    FilenameUtils.getBaseName(FileUtils.getRealPathFromURI(getActivity(), Uri.parse(mDataPhoto.get(i).getPath()))) +
                                            "." +
                                            FilenameUtils.getExtension(FileUtils.getRealPathFromURI(getActivity(), Uri.parse(mDataPhoto.get(i).getPath()))),
                                    mDataPhoto.get(i).getPath()
                            );
                        }
                    }

                }.execute();
            }
        }
    }

    private void setUpMap() {
        if (mGoogleMap == null) {

            mFragment = (SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.map);
            mGoogleMap = mFragment.getMap();

            if (mGoogleMap != null) {
                Log.e("", "Into full map");
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                mGoogleMap.setMyLocationEnabled(true);
                mGoogleMap.setOnInfoWindowClickListener(this);
                mGoogleMap.setInfoWindowAdapter(new MyInfoWindowAdapter());
            }
        }
    }

    private void setLocation() {
        // Getting LocationManager object from System Service LOCATION_SERVICE
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(getActivity().LOCATION_SERVICE);

        // Creating a criteria object to retrieve provider
        Criteria criteria = new Criteria();

        // Getting the name of the best provider
        String provider = locationManager.getBestProvider(criteria, true);

        // Getting Current Location
        Location location = locationManager.getLastKnownLocation(provider);

        if (location != null) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            LatLng latLng = new LatLng(latitude, longitude);
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            // Zoom in the Google Map
            mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
            onLocationChanged(location);

            locationManager.requestSingleUpdate(provider, this, null);

            if (locationManager != null) {
                locationManager.removeUpdates(this);
            }
            //locationManager.requestLocationUpdates(provider, 60000, 0, this);
        }
    }

    private void setMark(LatLng latLng, String title, String path) {
        // create marker
        MarkerOptions markerOptions = new MarkerOptions().position(latLng).title(title);
        //markerOptions.icon(BitmapDescriptorFactory.fromBitmap(getBitmap(path)));
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));


        // adding marker
        Marker marker = mGoogleMap.addMarker(markerOptions);
        //marker.showInfoWindow();
        mMarkers.add(marker);

        markers.put(marker.getId(),path);
    }

    @Override
    public void onLocationChanged(Location location) {
        // Getting latitude of the current location
        double latitude = location.getLatitude();

        // Getting longitude of the current location
        double longitude = location.getLongitude();

        // Creating a LatLng object for the current location
        LatLng latLng = new LatLng(latitude, longitude);

        // Showing the current location in Google Map
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        // Zoom in the Google Map
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
    }


    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        //Log.i("log_tag", "mMarkers.size=" + mMarkers.size() + " mDataPhoto.size=" + mDataPhoto.size());
        for (int i = 0; i < mMarkers.size(); i++) {
            if (mMarkers.get(i).equals(marker)) {
                Bundle bundle = new Bundle();
                bundle.putString("path", mDataPhoto.get(i).getPath());
                bundle.putBoolean("firstTime", false);
                bundle.putBoolean("little", false);
                PreviewFragment fragment = new PreviewFragment();
                fragment.setArguments(bundle);
                getFragmentManager().beginTransaction()
                        .replace(R.id.container, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        }
    }



    public class MyInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;

        MyInfoWindowAdapter() {
            myContentsView = getActivity().getLayoutInflater().inflate(R.layout.custom_info, null);
        }

        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            String url = null;

            if (marker.getId() != null && markers != null && markers.size() > 0) {
                if ( markers.get(marker.getId()) != null &&
                        markers.get(marker.getId()) != null) {
                    url = markers.get(marker.getId());
                }
            }
            if (url != null && !url.equalsIgnoreCase("null")
                    && !url.equalsIgnoreCase("")) {
            ImageView imageView = (ImageView) myContentsView.findViewById(R.id.info_image);
            imageView.setImageBitmap(ImageUtils.getBitmap(getActivity(),url));
            }
        /*TextView tvTitle = ((TextView)myContentsView.findViewById(R.id.title));
        tvTitle.setText(marker.getTitle());*/
        /*TextView tvSnippet = ((TextView)myContentsView.findViewById(R.id.snippet));
        tvSnippet.setText(marker.getSnippet());*/

            return myContentsView;
        }
    }
}