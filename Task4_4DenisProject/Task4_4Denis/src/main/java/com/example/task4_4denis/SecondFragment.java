package com.example.task4_4denis;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by dev1 on 1/31/14.
 */
public class SecondFragment extends Fragment implements IConstants {

    private String color1;
    private String color2;
    private SecondReceiver secondReceiver = new SecondReceiver();
    private boolean changedSecond = false;
    private boolean send = true;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.second_fragment_layout,
                container, false);


        if (savedInstanceState == null) {
            //view.setBackgroundColor(Color.parseColor("#0000ff"));
            color1 = getActivity().getIntent().getExtras().getString("edit1");
            color2 = getActivity().getIntent().getExtras().getString("edit2");
            view.setBackgroundColor(Color.parseColor(color2));
            getActivity().registerReceiver(secondReceiver, new IntentFilter(
                    ACTION_2));
            getActivity().sendBroadcast(new Intent(ACTION_1));
        } else {
            // Restore last state for checked position.
            changedSecond = savedInstanceState.getBoolean("curState");
            send = false;
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(secondReceiver);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("curState", changedSecond);
    }

    private class SecondReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            if (intent.getAction().equals(ACTION_2)) {
                new Thread(new Runnable() {
                    public void run() {
                        if (send) {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            if (!changedSecond) {
                                changedSecond = true;
                                view.post(new Runnable() {
                                    public void run() {
                                        view.setBackgroundColor(Color.parseColor(color1));

                                    }
                                });
                            } else {
                                changedSecond = false;
                                view.post(new Runnable() {
                                    public void run() {
                                        view.setBackgroundColor(Color.parseColor(color2));
                                    }
                                });
                            }
                            getActivity().sendBroadcast(new Intent(ACTION_1));
                        } else {
                            send = true;
                        }
                    }
                }).start();
            }
        }
    }
}
