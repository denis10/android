package com.example.task4_4denis;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button b1=(Button)findViewById(R.id.btn1);
        b1.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText edit1=(EditText)findViewById(R.id.edt1);
                EditText edit2=(EditText)findViewById(R.id.edt2);
                Intent intent=new Intent(MainActivity.this, SecondActivity.class);

                try {
                    String s1=edit1.getText().toString();
                    String s2=edit2.getText().toString();
                    if (s1.length()==0||s2.length()==0){
                        throw new ZeroException();
                    }
                    Color.parseColor(s1);
                    Color.parseColor(s2);
                    intent.putExtra("edit1", edit1.getText().toString());
                    intent.putExtra("edit2", edit2.getText().toString());
                    startActivity(intent);
                }catch (ZeroException ze){
                    Toast.makeText(getApplicationContext(), "fill fields", Toast.LENGTH_SHORT).show();
                }catch (IllegalArgumentException e){
                    Toast.makeText(getApplicationContext(), "put correct data", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
}
