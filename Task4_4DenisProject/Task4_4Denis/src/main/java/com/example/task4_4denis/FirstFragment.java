package com.example.task4_4denis;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by dev1 on 1/31/14.
 */
public class FirstFragment extends Fragment implements IConstants {

    private FirstReceiver firstReceiver = new FirstReceiver();
    private String color1;
    private String color2;
    private boolean changedFirst = false;
    private boolean send = true;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.first_fragment_layout,
                container, false);


        if (savedInstanceState == null) {
            color1 = getActivity().getIntent().getExtras().getString("edit1");
            color2 = getActivity().getIntent().getExtras().getString("edit2");
            view.setBackgroundColor(Color.parseColor(color1));
            getActivity().registerReceiver(firstReceiver, new IntentFilter(
                    ACTION_1));
            getActivity().sendBroadcast(new Intent(ACTION_2));
        } else {
            // Restore last state for checked position.
            changedFirst = savedInstanceState.getBoolean("curState");
            send = false;
        }


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();

        getActivity().unregisterReceiver(firstReceiver);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("curState", changedFirst);
    }

    private class FirstReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
        /*Intent activityIntent = new Intent(context, ResultActivity.class);
        activityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(activityIntent);*/
            if (intent.getAction().equals(ACTION_1)) {
                //Toast.makeText(context, "First Fragment", Toast.LENGTH_SHORT).show();

                new Thread(new Runnable() {
                    public void run() {
                        if (send) {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            if (!changedFirst) {
                                changedFirst = true;
                                view.post(new Runnable() {
                                    public void run() {
                                        view.setBackgroundColor(Color.parseColor(color2));

                                    }
                                });
                            } else {
                                changedFirst = false;
                                view.post(new Runnable() {
                                    public void run() {
                                        view.setBackgroundColor(Color.parseColor(color1));

                                    }
                                });
                            }
                            getActivity().sendBroadcast(new Intent(ACTION_2));
                        } else {
                            send = true;
                        }
                    }
                }).start();

            }
        }
    }
}
