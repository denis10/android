package com.example.task4_4denis;

/**
 * Created by Denis on 09.02.14.
 */
public class ZeroException extends Exception {
    public ZeroException() {

    }

    public ZeroException(String message) {
        super(message);
    }
}
