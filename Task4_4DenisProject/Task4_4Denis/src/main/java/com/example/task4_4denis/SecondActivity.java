package com.example.task4_4denis;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.LinearLayout;
import android.widget.Toast;


public class SecondActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        //getIntent().getStringExtra("edit1");
        FirstFragment firstFragment=new FirstFragment();
        //firstFragment.setArguments(getIntent().getExtras());
        SecondFragment secondFragment=new SecondFragment();
        //secondFragment.setArguments(getIntent().getExtras());

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.lin2, secondFragment)
                .replace(R.id.lin1, firstFragment)
                .commit();
    }

}
