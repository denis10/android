package com.example.task5_1denis;

import android.app.Activity;
import android.app.ListActivity;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.ArrayAdapter;

public class MyListActivity extends ListActivity {
    private ArrayAdapter<CharSequence> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_list_activity);
        adapter = ArrayAdapter.createFromResource(this, R.array.planets_array, android.R.layout.simple_list_item_1);
        /*String[] bases=null;
        try {
            bases = getResources().getStringArray(R.array.planets_array);
        }catch (Exception e){
            e.printStackTrace();
        }
        adapter = new ArrayAdapter<CharSequence>(this,R.layout.activity_my_list_activity, R.id.list,bases);*/
        setListAdapter(adapter);
    }

}
