package com.example.task5_1denis;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SecondActivity extends ActionBarActivity {
    private Spinner spinner;
    private ArrayAdapter<CharSequence> adapter;
    private LinearLayout lin;
    private LayoutInflater inflater;
    private ListView listView;
    private GridView gridView;
    private ExpandableListView exListView;
    private ArrayList<Map<String, String>> groupData;
    private ArrayList<Map<String, String>> childDataItem;
    private ArrayList<ArrayList<Map<String, String>>> childData;
    private Map<String, String> m;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        final int res=getIntent().getExtras().getInt("value");
        lin=(LinearLayout)findViewById(R.id.lin_second);

        inflater =(LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (res){
            case 1:
                Toast.makeText(getApplicationContext(), "ListView", Toast.LENGTH_SHORT).show();
                inflater.inflate(R.layout.list_view_layout,lin,true);
                listView=(ListView)findViewById(R.id.planets_list_view);
                adapter =ArrayAdapter.createFromResource(this,R.array.planets_array, android.R.layout.simple_list_item_1);
                listView.setAdapter(adapter);
                break;
            case 2:
                Toast.makeText(getApplicationContext(), "GridView", Toast.LENGTH_SHORT).show();
                inflater.inflate(R.layout.grid_view_layout,lin,true);
                gridView=(GridView)findViewById(R.id.planets_grid_view);
                adapter =ArrayAdapter.createFromResource(this,R.array.planets_array, android.R.layout.simple_gallery_item);
                gridView.setAdapter(adapter);
                break;
            case 3:
                Toast.makeText(getApplicationContext(), "Spinner", Toast.LENGTH_SHORT).show();
                inflater.inflate(R.layout.spinner,lin,true);
                spinner = (Spinner) findViewById(R.id.planets_spinner);
                adapter =// new ArrayAdapter<CharSequence>(this,R.array.planets_array, android.R.layout.simple_spinner_item);
                        ArrayAdapter.createFromResource(this,R.array.planets_array, android.R.layout.simple_spinner_item);
                // Specify the layout to use when the list of choices appears
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                // Apply the adapter to the spinner
                spinner.setAdapter(adapter);
                break;
            case 4:
                Toast.makeText(getApplicationContext(), "ListActivity", Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(SecondActivity.this,MyListActivity.class);
                startActivity(intent);
                break;
            case 5:
                Toast.makeText(getApplicationContext(), "ListFragment", Toast.LENGTH_SHORT).show();
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.lin_second, new MyListFragment())
                        .commit();
                break;
            case 6:
                Toast.makeText(getApplicationContext(), "ExpandableListView", Toast.LENGTH_SHORT).show();
                inflater.inflate(R.layout.expandable_list_view,lin,true);
                exListView =(ExpandableListView)findViewById(R.id.ex_list_view);

                String[] planets=null;
                try {
                    planets = getResources().getStringArray(R.array.planets_array);
                }catch (Exception e){
                    e.printStackTrace();
                }
                String[] earthSatellites=null;
                try {
                    earthSatellites = getResources().getStringArray(R.array.earth_array);
                }catch (Exception e){
                    e.printStackTrace();
                }
                String[] marsSatellites=null;
                try {
                    marsSatellites = getResources().getStringArray(R.array.mars_array);
                }catch (Exception e){
                    e.printStackTrace();
                }
                groupData = new ArrayList<Map<String, String>>();
                for (String group : planets) {
                    m = new HashMap<String, String>();
                    m.put("groupName", group);
                    groupData.add(m);
                }
                String groupFrom[] = new String[] { "groupName" };
                int groupTo[] = new int[] { android.R.id.text1 };

                childData = new ArrayList<ArrayList<Map<String, String>>>();
                childData.add(new ArrayList<Map<String, String>>());//empty
                childData.add(new ArrayList<Map<String, String>>());//empty

                childDataItem = new ArrayList<Map<String, String>>();

                for (String satellite : earthSatellites) {
                    m = new HashMap<String, String>();
                    m.put("satelliteName", satellite);
                    childDataItem.add(m);
                }
                childData.add(childDataItem);

                childDataItem = new ArrayList<Map<String, String>>();
                for (String satellite : marsSatellites) {
                    m = new HashMap<String, String>();
                    m.put("satelliteName", satellite);
                    childDataItem.add(m);
                }
                childData.add(childDataItem);

                for (int i=4;i<groupData.size();i++){
                    childData.add(new ArrayList<Map<String, String>>());//empty
                }

                String childFrom[] = new String[] { "satelliteName" };
                int childTo[] = new int[] { android.R.id.text1 };

                SimpleExpandableListAdapter exAdapter = new SimpleExpandableListAdapter(
                        this, groupData,
                        android.R.layout.simple_expandable_list_item_1, groupFrom,
                        groupTo, childData, android.R.layout.simple_list_item_1,
                        childFrom, childTo);

                exListView.setAdapter(exAdapter);
                break;
            default:
        }
    }
}
