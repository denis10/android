package com.example.task5_1denis;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.Button;

public class MainActivity extends ActionBarActivity {
    private final int ListView=1;
    private final int GridView=2;
    private final int Spinner=3;
    private final int ListActivity=4;
    private final int ListFragment=5;
    private final int ExpandableListView=6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Intent intent=new Intent(MainActivity.this,SecondActivity.class);
        Button btn1=(Button)findViewById(R.id.btn1);
        btn1.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent.putExtra("value", ListView);
                startActivity(intent);
            }
        });
        Button btn2=(Button)findViewById(R.id.btn2);
        btn2.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent.putExtra("value", GridView);
                startActivity(intent);
            }
        });
        Button btn3=(Button)findViewById(R.id.btn3);
        btn3.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent.putExtra("value", Spinner);
                startActivity(intent);
            }
        });
        Button btn4=(Button)findViewById(R.id.btn4);
        btn4.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent.putExtra("value", ListActivity);
                startActivity(intent);
            }
        });
        Button btn5=(Button)findViewById(R.id.btn5);
        btn5.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent.putExtra("value", ListFragment);
                startActivity(intent);
            }
        });
        Button btn6=(Button)findViewById(R.id.btn6);
        btn6.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent.putExtra("value", ExpandableListView);
                startActivity(intent);
            }
        });

    }

}
