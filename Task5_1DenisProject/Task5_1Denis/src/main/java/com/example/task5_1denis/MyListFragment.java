package com.example.task5_1denis;


import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

/**
 * Created by dev1 on 2/3/14.
 */
public class MyListFragment extends ListFragment {
    private ArrayAdapter<CharSequence> adapter;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_fragment_layout,
                container, false);
        adapter = ArrayAdapter.createFromResource(getActivity(), R.array.planets_array, android.R.layout.simple_list_item_1);
        /*String[] bases=null;
        try {
            bases = getResources().getStringArray(R.array.planets_array);
        }catch (Exception e){
            e.printStackTrace();
        }
        //getActivity().getSupportFragmentManager().findFragmentById(R.id.my_fragment).getView().findViewById(R.id.list)
        adapter = new ArrayAdapter<CharSequence>(getActivity(),android.R.layout.simple_list_item_1,bases);*/
        setListAdapter(adapter);
        return view;
    }
}
