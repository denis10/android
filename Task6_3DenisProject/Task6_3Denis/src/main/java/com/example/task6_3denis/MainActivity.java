package com.example.task6_3denis;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import com.example.fragments.MainListFragment;


public class MainActivity extends ActionBarActivity{// implements LoaderManager.LoaderCallbacks<Cursor>{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.lin_second, new MainListFragment())
                    .commit();
        }
    }
}
