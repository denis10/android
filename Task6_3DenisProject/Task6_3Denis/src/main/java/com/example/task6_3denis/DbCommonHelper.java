package com.example.task6_3denis;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.main_objects.Detention;
import com.example.main_objects.District;
import com.example.main_objects.Officer;
import com.example.main_objects.Suspect;

/**
 * Created by dev1 on 2/4/14.
 */
public class DbCommonHelper extends SQLiteOpenHelper {
    public static final int DB_VERSION = 1;
    public static final String DB_NAME = "my7.db";

    public static final String TABLE_OFFICERS = "officers";
    public static final String OFFICER_NAME = "officer_name";
    private static final String CREATE_TABLE_OFFICERS = "CREATE TABLE " + TABLE_OFFICERS + " (_id integer primary key autoincrement, "
            + OFFICER_NAME + " TEXT);";

    public static final String TABLE_SUSPECTS = "suspects";
    public static final String SUSPECT_NAME = "suspect_name";
    private static final String CREATE_TABLE_SUSPECTS = "create table " + TABLE_SUSPECTS + " ( _id integer primary key autoincrement, "
            + SUSPECT_NAME + " TEXT)";

    public static final String TABLE_DISTRICTS = "districts";
    public static final String DISTRICT_NAME = "district_name";
    private static final String CREATE_TABLE_DISTRICTS = "create table " + TABLE_DISTRICTS + " ( _id integer primary key autoincrement, "
            + DISTRICT_NAME + " TEXT)";

    public static final String TABLE_DETENTIONS = "detentions";
    public static final String OFFICER_ID = "officer_id";
    public static final String SUSPECT_ID = "suspect_id";
    public static final String DISTRICT_ID = "district_id";

    private static final String CREATE_TABLE_DETENTIONS = "CREATE TABLE " + TABLE_DETENTIONS + " (_id integer primary key autoincrement, "
            + OFFICER_ID+" INTEGER, "
            + SUSPECT_ID+" INTEGER, "
            + DISTRICT_ID+" INTEGER, "
            + "FOREIGN KEY ("+OFFICER_ID+") REFERENCES "+TABLE_OFFICERS+" (_id) ON DELETE CASCADE, "
            + "FOREIGN KEY ("+SUSPECT_ID+") REFERENCES "+TABLE_SUSPECTS+" (_id) ON DELETE CASCADE, "
            + "FOREIGN KEY ("+DISTRICT_ID+") REFERENCES "+TABLE_DISTRICTS+" (_id) ON DELETE CASCADE"
            +");";

    public DbCommonHelper(Context context) {
        super(context, DB_NAME, null,DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_OFFICERS);
        sqLiteDatabase.execSQL(CREATE_TABLE_SUSPECTS);
        sqLiteDatabase.execSQL(CREATE_TABLE_DISTRICTS);
        sqLiteDatabase.execSQL(CREATE_TABLE_DETENTIONS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS" + TABLE_OFFICERS);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS" + TABLE_SUSPECTS);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS" + TABLE_DISTRICTS);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS" + TABLE_DETENTIONS);
        onCreate(sqLiteDatabase);
    }

    public static ContentValues officerToCv(Officer o){
        ContentValues cv = new ContentValues();
        cv.put(DbCommonHelper.OFFICER_NAME,o.getOfficerName());
        return cv;
    }
    public static ContentValues suspectToCv(Suspect s){
        ContentValues cv = new ContentValues();
        cv.put(DbCommonHelper.SUSPECT_NAME,s.getSuspectName());
        return cv;
    }
    public static ContentValues districtToCv(District d){
        ContentValues cv = new ContentValues();
        cv.put(DbCommonHelper.DISTRICT_NAME, d.getDistrictName());
        return cv;
    }
    public static ContentValues detentionToCv(Detention det){
        ContentValues cv = new ContentValues();
        cv.put(DbCommonHelper.OFFICER_ID,det.getIdOfficer());
        cv.put(DbCommonHelper.SUSPECT_ID,det.getIdSuspect());
        cv.put(DbCommonHelper.DISTRICT_ID,det.getIdDistrict());
        return cv;
    }
}


