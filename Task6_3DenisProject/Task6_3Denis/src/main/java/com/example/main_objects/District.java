package com.example.main_objects;

/**
 * Created by dev1 on 2/5/14.
 */
public class District {
    private String districtName;
    public District(String districtName){
        this.districtName=districtName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }
}
