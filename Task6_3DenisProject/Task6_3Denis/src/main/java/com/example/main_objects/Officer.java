package com.example.main_objects;

/**
 * Created by dev1 on 2/5/14.
 */
public class Officer {
    private String officerName;
    public Officer(String officerName){
        this.officerName=officerName;
    }

    public String getOfficerName() {
        return officerName;
    }

    public void setOfficerName(String officerName) {
        this.officerName = officerName;
    }
}
