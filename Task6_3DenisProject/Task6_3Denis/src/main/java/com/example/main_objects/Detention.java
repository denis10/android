package com.example.main_objects;

/**
 * Created by dev1 on 2/5/14.
 */
public class Detention {
    private long idOfficer;
    private long idSuspect;
    private long idDistrict;
    public Detention(long idOfficer,long idSuspect,long idDistrict){
        this.idOfficer=idOfficer;
        this.idSuspect=idSuspect;
        this.idDistrict=idDistrict;
    }

    public long getIdDistrict() {
        return idDistrict;
    }

    public void setIdDistrict(long idDistrict) {
        this.idDistrict = idDistrict;
    }

    public long getIdSuspect() {
        return idSuspect;
    }

    public void setIdSuspect(long idSuspect) {
        this.idSuspect = idSuspect;
    }

    public long getIdOfficer() {

        return idOfficer;
    }

    public void setIdOfficer(long idOfficer) {
        this.idOfficer = idOfficer;
    }
}
