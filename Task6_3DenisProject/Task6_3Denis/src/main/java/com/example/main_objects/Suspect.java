package com.example.main_objects;

/**
 * Created by dev1 on 2/5/14.
 */
public class Suspect {
    private String suspectName;
    public Suspect(String suspectName){
        this.suspectName =suspectName;
    }

    public String getSuspectName() {
        return suspectName;
    }

    public void setSuspectName(String suspectName) {
        this.suspectName = suspectName;
    }
}
