package com.example.fragments;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;

import com.example.task6_3denis.DbCommonHelper;
import com.example.task6_3denis.R;

/**
 * Created by dev1 on 2/7/14.
 */
public class DistrictFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor>{
    private static final int UNIQUE_FRAGMENT_GROUP_ID = 3;
    private static final int CM_DELETE_ID = 1;
    private static final int CM_EDIT_ID = 2;

    private SimpleCursorAdapter scAdapter;
    private DbCommonHelper mDBHelper;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.district_list_layout,
                container, false);

        mDBHelper = new DbCommonHelper(view.getContext());

        String[] from = new String[] { DbCommonHelper.DISTRICT_NAME};
        int[] to = new int[] { android.R.id.text1};

        scAdapter = new SimpleCursorAdapter(view.getContext(), android.R.layout.simple_list_item_1, null, from, to,UNIQUE_FRAGMENT_GROUP_ID);
        setListAdapter(scAdapter);

        getActivity().getSupportLoaderManager().initLoader(UNIQUE_FRAGMENT_GROUP_ID, null, this).forceLoad();
        return view;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        registerForContextMenu(getListView());
        getActivity().getSupportLoaderManager().getLoader(UNIQUE_FRAGMENT_GROUP_ID).forceLoad();//update
        Button b1=(Button)getView().findViewById(R.id.add_district);
        b1.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Bundle bundle = new Bundle();
                bundle.putBoolean("edit",false);

                EditDistrict fragment=new EditDistrict();
                fragment.setArguments(bundle);
                getFragmentManager().beginTransaction()
                        .replace(R.id.lin_second, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
    }
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(UNIQUE_FRAGMENT_GROUP_ID, CM_DELETE_ID, 0, R.string.delete_record);
        menu.add(UNIQUE_FRAGMENT_GROUP_ID, CM_EDIT_ID, 0, R.string.edit_record);
    }
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId() == CM_DELETE_ID) {
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item
                    .getMenuInfo();

            SQLiteDatabase db = mDBHelper.getWritableDatabase();
            synchronized(db){
                db.delete(DbCommonHelper.TABLE_DISTRICTS, "_id" + "=" + acmi.id, null);
                getActivity().getSupportLoaderManager().getLoader(UNIQUE_FRAGMENT_GROUP_ID).forceLoad();
                db.close();
            }
            return true;
        }else if (item.getItemId() == CM_EDIT_ID){
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item
                    .getMenuInfo();
            final Bundle bundle = new Bundle();
            bundle.putLong("id", acmi.id);
            String  selectedWord = ((TextView) acmi.targetView).getText().toString();
            bundle.putString("name",selectedWord);
            bundle.putBoolean("edit",true);

            EditDistrict fragment=new EditDistrict();
            fragment.setArguments(bundle);
            getFragmentManager().beginTransaction()
                    .replace(R.id.lin_second, fragment)
                    .addToBackStack(null)
                    .commit();
        }
        return super.onContextItemSelected(item);
    }
    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        SQLiteDatabase db = mDBHelper.getWritableDatabase();
        synchronized(db){
            return new MyCursorLoader(getActivity(),mDBHelper);
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        scAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }
    private static class MyCursorLoader extends CursorLoader {

        DbCommonHelper mDBHelper;

        public MyCursorLoader(Context context,DbCommonHelper mDBHelper) {
            super(context);
            this.mDBHelper = mDBHelper;
        }

        @Override
        public Cursor loadInBackground() {
            String query="SELECT * FROM "+DbCommonHelper.TABLE_DISTRICTS+
                    ";";
            SQLiteDatabase db = mDBHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            return cursor;
        }
    }
}
