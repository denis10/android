package com.example.fragments;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;

import com.example.fragments.SpinnerAdapter;
import com.example.fragments.SpinnerModel;
import com.example.task6_3denis.DbCommonHelper;
import com.example.task6_3denis.R;
import com.example.main_objects.Detention;
import com.example.main_objects.Officer;

import java.util.ArrayList;

/**
 * Created by dev1 on 2/7/14.
 */
public class EditDetention extends Fragment {

    private long id;
    private String name;
    private Officer officer;
    private DbCommonHelper mDBHelper;
    private Spinner spinnerOfficer;
    private Spinner spinnerSuspect;
    private Spinner spinnerDistrict;
    private ArrayList<SpinnerModel> dataOfficer;
    private ArrayList<SpinnerModel> dataSuspect;
    private ArrayList<SpinnerModel> dataDistrict;
    private SpinnerAdapter adapter;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_detention,
                container, false);
        mDBHelper = new DbCommonHelper(view.getContext());

        return view;
    }
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle args = getArguments();
        id = args.getLong("id");

        SQLiteDatabase db = mDBHelper.getWritableDatabase();
        dataOfficer=new ArrayList<SpinnerModel>();
        synchronized(db){
            String query="SELECT * FROM "+DbCommonHelper.TABLE_OFFICERS+
                    ";";
            Cursor c = db.rawQuery(query, null);

            if (c.moveToFirst()) {
                int idColIndex = c.getColumnIndex("_id");
                int nameColIndex = c.getColumnIndex("officer_name");
                do {
                    dataOfficer.add(new SpinnerModel(c.getLong(idColIndex),c.getString(nameColIndex)));
                } while (c.moveToNext());
            } else
                Log.d("log", "0 rows");
            c.close();
            db.close();
        }
        adapter =new SpinnerAdapter(getView().getContext(),android.R.layout.simple_spinner_item,dataOfficer);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerOfficer = (Spinner) getView().findViewById(R.id.spinner_officer);
        spinnerOfficer.setAdapter(adapter);


        db = mDBHelper.getWritableDatabase();
        dataSuspect=new ArrayList<SpinnerModel>();
        synchronized(db){
            String query="SELECT * FROM "+DbCommonHelper.TABLE_SUSPECTS+
                    ";";
            Cursor c = db.rawQuery(query, null);

            if (c.moveToFirst()) {
                int idColIndex = c.getColumnIndex("_id");
                int nameColIndex = c.getColumnIndex("suspect_name");
                do {
                    dataSuspect.add(new SpinnerModel(c.getLong(idColIndex),c.getString(nameColIndex)));
                } while (c.moveToNext());
            } else
                Log.d("log", "0 rows");
            c.close();
            db.close();
        }
        adapter =new SpinnerAdapter(getView().getContext(),android.R.layout.simple_spinner_item,dataSuspect);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSuspect = (Spinner) getView().findViewById(R.id.spinner_suspect);
        spinnerSuspect.setAdapter(adapter);

        db = mDBHelper.getWritableDatabase();
        dataDistrict=new ArrayList<SpinnerModel>();
        synchronized(db){
            String query="SELECT * FROM "+DbCommonHelper.TABLE_DISTRICTS+
                    ";";
            Cursor c = db.rawQuery(query, null);

            if (c.moveToFirst()) {
                int idColIndex = c.getColumnIndex("_id");
                int nameColIndex = c.getColumnIndex("district_name");
                do {
                    dataDistrict.add(new SpinnerModel(c.getLong(idColIndex),c.getString(nameColIndex)));
                } while (c.moveToNext());
            } else
                Log.d("log", "0 rows");
            c.close();
            db.close();
        }
        adapter =new SpinnerAdapter(getView().getContext(),android.R.layout.simple_spinner_item,dataDistrict);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDistrict = (Spinner) getView().findViewById(R.id.spinner_district);
        spinnerDistrict.setAdapter(adapter);


        Button b1=(Button)getView().findViewById(R.id.ok_btn_detention);
        b1.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                spinnerOfficer.getSelectedItem();
                SQLiteDatabase db = mDBHelper.getWritableDatabase();
                synchronized(db){
                    Detention detention=new Detention(dataOfficer.get(spinnerOfficer.getSelectedItemPosition()).getId(),
                            dataSuspect.get(spinnerSuspect.getSelectedItemPosition()).getId(),
                            dataDistrict.get(spinnerDistrict.getSelectedItemPosition()).getId());

                    db.update(DbCommonHelper.TABLE_DETENTIONS,DbCommonHelper.detentionToCv(detention),"_id" + "=" + id,null);
                    //db.insert(DbCommonHelper.TABLE_DETENTIONS, null, DbCommonHelper.detentionToCv(detention));
                    db.close();
                }

                //Toast.makeText(getView().getContext(), "CLICKED:" + dataOfficer.get(spinnerOfficer.getSelectedItemPosition()).getId(), Toast.LENGTH_SHORT).show();
                getFragmentManager()
                        .popBackStack();
               /* getFragmentManager().beginTransaction()
                        .replace(R.id.lin_second, new MainListFragment())
                        .addToBackStack(null)
                        .commit();*/
            }
        });
    }
}
