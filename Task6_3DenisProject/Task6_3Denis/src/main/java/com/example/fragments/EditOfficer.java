package com.example.fragments;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.task6_3denis.DbCommonHelper;
import com.example.task6_3denis.R;
import com.example.main_objects.Officer;

/**
 * Created by dev1 on 2/6/14.
 */
public class EditOfficer extends Fragment {
    private DbCommonHelper mDBHelper;
    private long id;
    private String name;
    private Officer officer;
    private boolean mEdit;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_officer,
                container, false);
        mDBHelper = new DbCommonHelper(view.getContext());

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle args = getArguments();
        mEdit = args.getBoolean("edit");

        final EditText edtOfficer = (EditText) getView().findViewById(R.id.edt_officer);
        if (mEdit) {
            id = args.getLong("id");
            name = args.getString("name");
            edtOfficer.setText(name);
        }
        Button b1 = (Button) getView().findViewById(R.id.ok_btn);
        b1.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                officer = new Officer(edtOfficer.getText().toString());
                if (!officer.getOfficerName().equals("")) {
                    SQLiteDatabase db = mDBHelper.getWritableDatabase();
                    if (mEdit) {
                        synchronized (db) {
                            db.update(DbCommonHelper.TABLE_OFFICERS, DbCommonHelper.officerToCv(officer), "_id" + "=" + id, null);
                            db.close();
                        }
                    } else {
                        synchronized (db) {
                            db.insert(DbCommonHelper.TABLE_OFFICERS, null, DbCommonHelper.officerToCv(officer));
                            db.close();
                        }
                    }
                } else {
                    Toast.makeText(getActivity(), "put correct data", Toast.LENGTH_LONG).show();
                }
                InputMethodManager imm = (InputMethodManager) getView().getContext().getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edtOfficer.getWindowToken(), 0);
                getFragmentManager()
                        .popBackStack();
            }
        });
    }
}
