package com.example.fragments;

/**
 * Created by dev1 on 2/6/14.
 */
public class SpinnerModel {
    private long id;
    private String name;
    public SpinnerModel(long id,String name){
        this.id=id;
        this.name=name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
