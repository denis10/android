package com.example.fragments;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Toast;

import com.example.fragments.*;
import com.example.task6_3denis.DbCommonHelper;
import com.example.task6_3denis.R;

/**
 * Created by dev1 on 2/6/14.
 */

public class MainListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final int UNIQUE_FRAGMENT_GROUP_ID = 0;
    private static final int CM_DELETE_ID = 1;
    private static final int CM_EDIT_ID = 2;

    private SimpleCursorAdapter scAdapter;
    private DbCommonHelper mDBHelper;
    private boolean mFirstRun = true;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_list_fragment,
                container, false);

        mDBHelper = new DbCommonHelper(view.getContext());

        if (scAdapter == null) {
            String[] from = new String[]{DbCommonHelper.OFFICER_NAME, DbCommonHelper.SUSPECT_NAME, DbCommonHelper.DISTRICT_NAME};
            int[] to = new int[]{R.id.officer_txt, R.id.suspect_txt, R.id.district_txt};

            scAdapter = new SimpleCursorAdapter(view.getContext(), R.layout.item, null, from, to, UNIQUE_FRAGMENT_GROUP_ID);
        }
        setListAdapter(scAdapter);


        //getActivity().getSupportLoaderManager().initLoader(0, null, this).forceLoad();

        return view;
    }

    protected void startLoading() {
        // first time we call this loader, so we need to create a new one
        getActivity().getSupportLoaderManager().initLoader(UNIQUE_FRAGMENT_GROUP_ID, null, this).forceLoad();
    }

    protected void restartLoading() {
        scAdapter.notifyDataSetChanged();
        getListView().invalidateViews();
        // --------- the other magic lines ----------
        // call restart because we want the background work to be executed
        // again
        getActivity().getSupportLoaderManager().restartLoader(UNIQUE_FRAGMENT_GROUP_ID, null, this);
        // --------- end the other magic lines --------
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);//from Frament dialog app
        registerForContextMenu(getListView());
        // ---- magic lines starting here -----
        // call this to re-connect with an existing
        // loader (after screen configuration changes for e.g!)
        LoaderManager lm = getLoaderManager();
        if (lm.getLoader(UNIQUE_FRAGMENT_GROUP_ID) != null) {
            lm.initLoader(UNIQUE_FRAGMENT_GROUP_ID, null, this);
        }
        // ----- end magic lines -----

        // first time
        if (mFirstRun) {
            mFirstRun = false;
            startLoading();
        }
        // already started once
        else {
            restartLoading();
        }

        Button b1 = (Button) getView().findViewById(R.id.officers);
        b1.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction()
                        .replace(R.id.lin_second, new OfficerFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });
        Button b2 = (Button) getView().findViewById(R.id.suspects);
        b2.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction()
                        .replace(R.id.lin_second, new SuspectFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });
        Button b3 = (Button) getView().findViewById(R.id.districts);
        b3.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction()
                        .replace(R.id.lin_second, new DistrictFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });
        Button b4 = (Button) getView().findViewById(R.id.add_detention);
        b4.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction()
                        .replace(R.id.lin_second, new AddDetentionFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });
    }

    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(UNIQUE_FRAGMENT_GROUP_ID, CM_DELETE_ID, 0, R.string.delete_record);
        menu.add(UNIQUE_FRAGMENT_GROUP_ID, CM_EDIT_ID, 0, R.string.edit_record);
    }

    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId() == CM_DELETE_ID) {
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item
                    .getMenuInfo();


            SQLiteDatabase db = mDBHelper.getWritableDatabase();

            Cursor c = scAdapter.getCursor();
            if (c != null) {
                c.moveToPosition(acmi.position);

                synchronized (db) {
                    db.delete(DbCommonHelper.TABLE_DETENTIONS, "_id" + "=" + c.getLong(0), null);
                    getActivity().getSupportLoaderManager().getLoader(UNIQUE_FRAGMENT_GROUP_ID).forceLoad();

                    db.close();
                }
               // c.close();
            }
            Toast.makeText(getActivity(), "id delete=" + c.getLong(0), Toast.LENGTH_SHORT).show();
            Toast.makeText(getActivity(), "position delete=" + acmi.position, Toast.LENGTH_SHORT).show();
            return true;
        } else if (item.getItemId() == CM_EDIT_ID) {
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item
                    .getMenuInfo();
            final Bundle bundle = new Bundle();
            Cursor c = scAdapter.getCursor();
            if (c != null) {
                c.moveToPosition(acmi.position);
                bundle.putLong("id", c.getLong(0));
               // c.close();
                EditDetention fragment = new EditDetention();
                fragment.setArguments(bundle);
                getFragmentManager().beginTransaction()
                        .replace(R.id.lin_second, fragment)
                        .addToBackStack(null)
                        .commit();
            }
            return true;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (scAdapter.getCursor() != null && !scAdapter.getCursor().isClosed()) {
            scAdapter.getCursor().close();
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        SQLiteDatabase db = mDBHelper.getWritableDatabase();
        synchronized (db) {
            return new MyCursorLoader(getActivity(), mDBHelper);
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        //scAdapter.changeCursor(cursor);
        scAdapter.swapCursor(cursor);
        scAdapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        scAdapter.changeCursor(null);
    }

    private static class MyCursorLoader extends CursorLoader {

        DbCommonHelper mDBHelper;

        public MyCursorLoader(Context context, DbCommonHelper mDBHelper) {
            super(context);
            this.mDBHelper = mDBHelper;
        }

        @Override
        public Cursor loadInBackground() {
            String query = "SELECT * FROM " + DbCommonHelper.TABLE_DETENTIONS +
                    " INNER JOIN " + DbCommonHelper.TABLE_OFFICERS + " ON " +
                    DbCommonHelper.TABLE_DETENTIONS + "." + DbCommonHelper.OFFICER_ID + "=" + DbCommonHelper.TABLE_OFFICERS + "._id" +
                    " INNER JOIN " + DbCommonHelper.TABLE_SUSPECTS + " ON " +
                    DbCommonHelper.TABLE_DETENTIONS + "." + DbCommonHelper.SUSPECT_ID + "=" + DbCommonHelper.TABLE_SUSPECTS + "._id" +
                    " INNER JOIN " + DbCommonHelper.TABLE_DISTRICTS + " ON " +
                    DbCommonHelper.TABLE_DETENTIONS + "." + DbCommonHelper.DISTRICT_ID + "=" + DbCommonHelper.TABLE_DISTRICTS + "._id" +
                    ";";
            SQLiteDatabase db = mDBHelper.getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            return cursor;
        }
    }
}
