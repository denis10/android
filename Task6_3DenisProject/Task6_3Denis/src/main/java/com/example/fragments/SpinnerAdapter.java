package com.example.fragments;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.fragments.SpinnerModel;

import java.util.ArrayList;

/**
 * Created by dev1 on 2/6/14.
 */
public class SpinnerAdapter extends ArrayAdapter<SpinnerModel> {
    private Context context;
    private int resourceId;
    private ArrayList<SpinnerModel> objects;
    public SpinnerAdapter(Context context,int resourceId, ArrayList<SpinnerModel> objects){
        super(context, resourceId, objects);
        this.context=context;
        this.resourceId=resourceId;
        this.objects=objects;

    }
    @Override
    public SpinnerModel getItem(int position) {

        return objects.get(position);
    }

    @Override
    public int getCount() {
        return objects.size();
    }
    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
    public View getCustomView(int position, View convertView,
                              ViewGroup parent) {
        TextView textView = (TextView) View.inflate(context, android.R.layout.simple_spinner_item, null);
        textView.setText(objects.get(position).getName());
        return textView;
    }
}
