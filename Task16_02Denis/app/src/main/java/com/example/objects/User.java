package com.example.objects;

import com.example.helpers.DbCommonHelper;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Denis on 02.03.14.
 */
public class User {
    @SerializedName(DbCommonHelper.USER_ID)
    protected long id;

    @SerializedName(DbCommonHelper.USER_LOGIN)
    protected String login;

    @SerializedName(DbCommonHelper.USER_PASSWORD)
    protected String password;

    @SerializedName(DbCommonHelper.USER_UNIQUE_ID)
    protected String userUniqueId;

    public User(){

    }
    public User(long id, String login, String password,String uniqueId) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.userUniqueId =uniqueId;
    }

    public User(String login, String password,String uniqueId) {
        this.login = login;
        this.password = password;
        this.userUniqueId =uniqueId;
    }
    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getUserUniqueId() {
        return userUniqueId;
    }

    public void setUserUniqueId(String userUniqueId) {
        this.userUniqueId = userUniqueId;
    }

    public String getLogin() {
        return login;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
