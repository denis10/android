package com.example.objects;

import com.example.helpers.DbCommonHelper;
import com.example.parsing.DebtorResult;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by dev1 on 2/18/14.
 */
public class Debtor implements Serializable {
    @SerializedName(DbCommonHelper.DEBTOR_ID)
    protected long id;

    @SerializedName(DbCommonHelper.DEBTOR_NAME)
    protected String name;

    @SerializedName(DbCommonHelper.DEBTOR_SECONDNAME)
    protected String secondName;

    @SerializedName(DbCommonHelper.DEBTOR_PHONE)
    protected String phone;

    @SerializedName(DbCommonHelper.DEBTOR_EMAIL)
    protected String email;

    @SerializedName(DbCommonHelper.DEBTOR_PHOTO)
    protected String photo;

    @SerializedName(DbCommonHelper.DEBTOR_UNIQUE_ID)
    protected String debtorUniqueId;

    @SerializedName(DbCommonHelper.USER_UNIQUE_ID)
    protected String userUniqueId;

    public Debtor() {
    }

    public Debtor(long id, String name, String secondName, String phone, String email, String photo, String debtorUniqueId, String userUniqueId) {
        this.id = id;
        this.name = name;
        this.secondName = secondName;
        this.phone = phone;
        this.email = email;
        this.photo = photo;
        this.debtorUniqueId = debtorUniqueId;
        this.userUniqueId = userUniqueId;
    }

    public Debtor(String name, String secondName, String phone, String email, String photo, String debtorUniqueId, String userUniqueId) {
        this.name = name;
        this.secondName = secondName;
        this.phone = phone;
        this.email = email;
        this.photo = photo;
        this.debtorUniqueId = debtorUniqueId;
        this.userUniqueId = userUniqueId;
    }

    @Override
    public boolean equals(Object o) {
        DebtorResult result=(DebtorResult)o;
        if (!name.equals(result.name)) {
            return false;
        }
        if (!secondName.equals(result.secondName)) {
            return false;
        }
        if (!phone.equals(result.phone)) {
            return false;
        }
        if (!email.equals(result.email)) {
            return false;
        }
        if (photo == null && result.photo != null) {
            return false;
        }
        if (photo != null && result.photo == null) {
            return false;
        }
        if (photo != null && result.photo != null && !photo.equals(result.photo)) {
            return false;
        }
        return true;
    }

    public String getDebtorUniqueId() {
        return debtorUniqueId;
    }

    public void setDebtorUniqueId(String debtorUniqueId) {
        this.debtorUniqueId = debtorUniqueId;
    }

    public String getUserUniqueId() {
        return userUniqueId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setUserUniqueId(String userUniqueId) {
        this.userUniqueId = userUniqueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
