package com.example.objects;

import com.example.utils.SynchronizeUtils;
import com.google.gson.annotations.SerializedName;

/**
 * Created by dev1 on 3/11/14.
 */
public class WebDebtor extends Debtor {
    @SerializedName(SynchronizeUtils.URL)
    private String webUri;
    public WebDebtor(String webUri,Debtor debtor){
        this.id = debtor.id;
        this.name = debtor.name;
        this.secondName = debtor.secondName;
        this.phone = debtor.phone;
        this.email = debtor.email;
        this.photo = debtor.photo;
        this.debtorUniqueId=debtor.debtorUniqueId;
        this.userUniqueId = debtor.userUniqueId;
        this.webUri=webUri;
    }
}
