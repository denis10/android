package com.example.objects;

import com.example.helpers.DbCommonHelper;
import com.example.parsing.DebtResult;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by dev1 on 2/18/14.
 */
public class Debt implements Serializable{
    @SerializedName(DbCommonHelper.DEBT_ID)
    protected long id;

    @SerializedName(DbCommonHelper.DEBT_UNIQUE_ID)
    protected String debtUniqueId;

    @SerializedName(DbCommonHelper.DEBTOR_UNIQUE_ID)
    protected String debtorUniqueId;

    @SerializedName(DbCommonHelper.USER_UNIQUE_ID)
    protected String userUniqueID;

    @SerializedName(DbCommonHelper.DEBT_START)
    protected String debtStart;

    @SerializedName(DbCommonHelper.DEBT_END)
    protected String debtEnd;

    @SerializedName(DbCommonHelper.DEBT_TOTAL)
    protected float debtTotal;

    @SerializedName(DbCommonHelper.DEBT_PHOTO_PATH)
    protected String debtPhotoPath;

    @SerializedName(DbCommonHelper.DEBT_COUNT)
    protected int debtCount;

    @SerializedName(DbCommonHelper.DEBT_NAME)
    protected String debtName;

    public Debt() {
    }

    public Debt(long id, String debtUniqueId,String debtorUniqueId,String userUniqueID, String debtStart, String debtEnd, float debtTotal, String debtPhotoPath, int debtCount,String debtName) {
        this.id = id;
        this.debtUniqueId = debtUniqueId;
        this.debtorUniqueId=debtorUniqueId;
        this.userUniqueID=userUniqueID;
        this.debtStart = debtStart;
        this.debtEnd = debtEnd;
        this.debtTotal = debtTotal;
        this.debtPhotoPath = debtPhotoPath;
        this.debtCount = debtCount;
        this.debtName=debtName;
    }

    public Debt(String debtUniqueId,String debtorUniqueId,String userUniqueID,String debtStart,String debtEnd,float debtTotal,String debtPhotoPath,int debtCount,String debtName){
        this.debtUniqueId=debtUniqueId;
        this.debtorUniqueId=debtorUniqueId;
        this.userUniqueID=userUniqueID;
        this.debtStart=debtStart;
        this.debtEnd=debtEnd;
        this.debtTotal=debtTotal;
        this.debtPhotoPath=debtPhotoPath;
        this.debtCount=debtCount;
        this.debtName=debtName;
    }

    @Override
    public boolean equals(Object o) {
        DebtResult result=(DebtResult)o;
        if (!debtStart.equals(result.debtStart)) {
            return false;
        }
        if (!debtEnd.equals(result.debtEnd)) {
            return false;
        }
        if (debtTotal != result.debtTotal) {
            return false;
        }
        if (debtPhotoPath == null && result.debtPhotoPath != null) {
            return false;
        }
        if (debtPhotoPath != null && result.debtPhotoPath == null) {
            return false;
        }
        if (debtPhotoPath != null && result.debtPhotoPath != null && !debtPhotoPath.equals(result.debtPhotoPath)) {
            return false;
        }
        if (debtCount != result.debtCount) {
            return false;
        }
        if (!debtName.equals(result.debtName)) {
            return false;
        }
        return true;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDebtUniqueId() {
        return debtUniqueId;
    }

    public void setDebtUniqueId(String debtUniqueId) {
        this.debtUniqueId = debtUniqueId;
    }

    public String getDebtorUniqueId() {
        return debtorUniqueId;
    }

    public void setDebtorUniqueId(String debtorUniqueId) {
        this.debtorUniqueId = debtorUniqueId;
    }

    public String getUserUniqueID() {
        return userUniqueID;
    }

    public void setUserUniqueID(String userUniqueID) {
        this.userUniqueID = userUniqueID;
    }

    public String getDebtStart() {
        return debtStart;
    }

    public void setDebtStart(String debtStart) {
        this.debtStart = debtStart;
    }

    public String getDebtEnd() {
        return debtEnd;
    }

    public void setDebtEnd(String debtEnd) {
        this.debtEnd = debtEnd;
    }

    public float getDebtTotal() {
        return debtTotal;
    }

    public void setDebtTotal(float debtTotal) {
        this.debtTotal = debtTotal;
    }

    public String getDebtPhotoPath() {
        return debtPhotoPath;
    }

    public void setDebtPhotoPath(String debtPhotoPath) {
        this.debtPhotoPath = debtPhotoPath;
    }

    public int getDebtCount() {
        return debtCount;
    }

    public void setDebtCount(int debtCount) {
        this.debtCount = debtCount;
    }

    public String getDebtName() {
        return debtName;
    }

    public void setDebtName(String debtName) {
        this.debtName = debtName;
    }
}
