package com.example.objects;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.provider.CalendarContract;

import java.io.Serializable;

/**
 * Created by dev1 on 3/7/14.
 */
public class Reminder implements Serializable {
    private String title;
    private String description;
    private long startTime;
    private long endTime;
    private Activity activity;


    public Reminder(String title, String description, long startTime, long endTime, Activity activity) {
        this.title = title;
        this.description = description;
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity = activity;
    }

    public void setReminder() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            Intent intent = new Intent(Intent.ACTION_EDIT)
                    .setType("vnd.android.cursor.item/event")
                    .putExtra("beginTime", startTime)
                    .putExtra("allDay", endTime)
                    .putExtra("title", title)
                    .putExtra("description", description)
                    .putExtra("eventLocation", "location")
                    .putExtra("allDay", 0);
            activity.startActivity(intent);
        } else {
            Intent intent = new Intent(Intent.ACTION_INSERT)
                    .setData(CalendarContract.Events.CONTENT_URI)
                    .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startTime)
                    .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime)
                    .putExtra(CalendarContract.Events.TITLE, title)
                    .putExtra(CalendarContract.Events.DESCRIPTION, description)
                    .putExtra(CalendarContract.Events.EVENT_LOCATION, "location")
                    .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);
            activity.startActivity(intent);
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }
}
