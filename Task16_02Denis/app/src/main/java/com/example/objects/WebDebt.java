package com.example.objects;

import com.example.utils.SynchronizeUtils;
import com.google.gson.annotations.SerializedName;

/**
 * Created by dev1 on 3/11/14.
 */
public class WebDebt extends Debt {
    @SerializedName(SynchronizeUtils.URL)
    private String webUri;

    public WebDebt(String webUri,Debt debt){
        this.id = debt.id;
        this.debtUniqueId = debt.debtUniqueId;
        this.debtorUniqueId=debt.debtorUniqueId;
        this.userUniqueID=debt.userUniqueID;
        this.debtStart = debt.debtStart;
        this.debtEnd = debt.debtEnd;
        this.debtTotal = debt.debtTotal;
        this.debtPhotoPath = debt.debtPhotoPath;
        this.debtCount = debt.debtCount;
        this.debtName=debt.debtName;
        this.webUri=webUri;
    }
}
