package com.example.objects;

/**
 * Created by dev1 on 2/26/14.
 */
public class FullDebt extends Debt {
    private String name;
    private String secondName;
    private String phone;
    private String email;
    private String photo;

    public FullDebt(Debtor debtor, Debt debt) {
        this.debtStart = debt.debtStart;
        this.debtEnd = debt.debtEnd;
        this.debtTotal = debt.debtTotal;
        this.debtPhotoPath = debt.debtPhotoPath;
        this.debtCount = debt.debtCount;
        this.debtName = debt.debtName;
        this.name = debtor.name;
        this.secondName = debtor.secondName;
        this.phone = debtor.phone;
        this.email = debtor.email;
        this.photo = debtor.photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
