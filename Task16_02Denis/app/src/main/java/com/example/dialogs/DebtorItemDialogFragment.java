package com.example.dialogs;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.helpers.DbCommonHelper;
import com.example.objects.Debtor;
import com.example.task16_02denis.app.R;

/**
 * Created by dev1 on 2/24/14.
 */
public class DebtorItemDialogFragment extends DialogFragment {
    private TextView mName;
    private TextView mSecondName;
    private TextView mPhone;
    private TextView mEmail;
    private View mLayout;
    private View mPhoneLayout;
    private TextView mEmailTitle;

    public static DebtorItemDialogFragment newInstance(Debtor debtor) {
        DebtorItemDialogFragment f = new DebtorItemDialogFragment();
        Bundle args = new Bundle();
        args.putSerializable(DbCommonHelper.DEBTOR_UNIQUE_ID, debtor);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Panel);
        //setStyle(DialogFragment.STYLE_NORMAL, R.style.MyDialog);
        //setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Holo_Dialog);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        getDialog().setTitle(R.string.title_debtor_dialog);

        View v = inflater.inflate(R.layout.debtor_info_item_dialog, null);
        mName = (TextView) v.findViewById(R.id.debtor_name_list_dialog_txt);
        mSecondName = (TextView) v.findViewById(R.id.debtor_secondname_list_dialog_txt);
        mPhone = (TextView) v.findViewById(R.id.debtor_phone_list_dialog_txt);
        mEmail = (TextView) v.findViewById(R.id.debtor_email_list_dialog_txt);
        mLayout = v.findViewById(R.id.debtor_info_dialog_layout);
        mPhoneLayout = v.findViewById(R.id.phone_layout);
        mEmailTitle = (TextView) v.findViewById(R.id.email_title);

        Debtor debtor = (Debtor) getArguments().getSerializable(DbCommonHelper.DEBTOR_UNIQUE_ID);
        mName.setText(debtor.getName());
        mSecondName.setText(debtor.getSecondName());
        if (TextUtils.isEmpty(debtor.getPhone())) {
            mPhoneLayout.setVisibility(View.GONE);
        } else {
            mPhone.setText(debtor.getPhone());
        }
        if (TextUtils.isEmpty(debtor.getEmail())) {
            mEmailTitle.setVisibility(View.GONE);
            mEmail.setVisibility(View.GONE);
        } else {
            mEmail.setText(debtor.getEmail());
        }

        getDialog().setCanceledOnTouchOutside(true);
        mLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });

        return v;
    }
}
