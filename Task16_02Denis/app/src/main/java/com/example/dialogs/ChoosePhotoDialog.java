package com.example.dialogs;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.fragments.EditDebtFragment;
import com.example.fragments.EditDebtorFragment;
import com.example.interfaces.OnCapturePhotoListener;
import com.example.interfaces.OnSdImageListener;
import com.example.task16_02denis.app.R;

/**
 * Created by dev1 on 2/28/14.
 */
public class ChoosePhotoDialog extends DialogFragment {
    public OnSdImageListener mSdListener;
    public OnCapturePhotoListener mCaptureListener;

    private Button mBtn1;
    private Button mBtn2;

    public static ChoosePhotoDialog newInstance() {
        ChoosePhotoDialog f = new ChoosePhotoDialog();
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        getDialog().setTitle(R.string.source);

        View v = inflater.inflate(R.layout.choose_photo_dialog, null);
        mBtn1 = (Button) v.findViewById(R.id.choose_sd_btn);
        mBtn2 = (Button) v.findViewById(R.id.choose_capture_btn);

        mBtn1.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSdListener.showSdImage();
                getDialog().dismiss();
            }
        });

        mBtn2.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCaptureListener.capturePhoto();
                getDialog().dismiss();
            }
        });

        return v;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        EditDebtorFragment.dialog = false;
        EditDebtFragment.dialog = false;
    }
}
