package com.example.dialogs;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.helpers.DbCommonHelper;
import com.example.task16_02denis.app.R;
import com.example.utils.FileUtils;
import com.example.utils.ImageUtils;

import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * Created by dev1 on 2/20/14.
 */
public class PhotoDialogFragment extends DialogFragment {
    private String mUri;
    private LinearLayout mLinearLayout;
    private ImageView mImageView;

    public static PhotoDialogFragment newInstance(String uri) {
        PhotoDialogFragment f = new PhotoDialogFragment();
        Bundle args = new Bundle();
        args.putString(DbCommonHelper.DEBT_PHOTO_PATH, uri);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Panel);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Light_Panel);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.photo_dialog, null);
        mUri = getArguments().getString(DbCommonHelper.DEBT_PHOTO_PATH);
        mLinearLayout = (LinearLayout) v.findViewById(R.id.photo_dialog_layout);
        mImageView = (ImageView) v.findViewById(R.id.image_photo_dialog);

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        if (mUri.contains("content://com.android.contacts/contacts/")) {
            try {
                InputStream input = getActivity().getContentResolver().openInputStream(Uri.parse(mUri));
                Bitmap bitmap = BitmapFactory.decodeStream(input);
                mImageView.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            Bitmap bitmap = ImageUtils.getImage(FileUtils.getRealPathFromURI(getActivity(), Uri.parse(mUri)), height, width);
            if (bitmap!=null) {
                mImageView.setImageBitmap(bitmap);
            }
        }

        getDialog().setCanceledOnTouchOutside(true);
        mLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });

        return v;
    }
}
