package com.example.dialogs;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.objects.Reminder;
import com.example.task16_02denis.app.R;

/**
 * Created by dev1 on 3/7/14.
 */
public class SetReminderDialog extends DialogFragment {
    private Button mBtn1;
    private Button mBtn2;

    public static SetReminderDialog newInstance(Reminder reminder) {
        SetReminderDialog f = new SetReminderDialog();
        Bundle args = new Bundle();
        args.putSerializable("reminder", reminder);
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        getDialog().setTitle(R.string.title_reminder_dialog);

        View v = inflater.inflate(R.layout.choose_photo_dialog, null);
        mBtn1 = (Button) v.findViewById(R.id.choose_sd_btn);
        mBtn1.setText(R.string.ok);
        mBtn2 = (Button) v.findViewById(R.id.choose_capture_btn);
        mBtn2.setText(R.string.no);

        mBtn1.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                Reminder reminder = (Reminder) getArguments().getSerializable("reminder");
                reminder.setReminder();
                myDismiss();
                getDialog().dismiss();
            }
        });

        mBtn2.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                myDismiss();
                getDialog().dismiss();
            }
        });

        getDialog().setCanceledOnTouchOutside(false);

        return v;
    }

    private void myDismiss() {
        Intent intent = new Intent();
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().finish();
    }

}
