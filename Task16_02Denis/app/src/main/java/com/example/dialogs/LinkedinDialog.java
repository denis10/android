package com.example.dialogs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Picture;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebView.PictureListener;
import android.webkit.WebViewClient;

import com.example.task16_02denis.app.R;
import com.example.utils.SocialUtils;
import com.google.code.linkedinapi.client.LinkedInApiClientFactory;
import com.google.code.linkedinapi.client.oauth.LinkedInOAuthService;
import com.google.code.linkedinapi.client.oauth.LinkedInOAuthServiceException;
import com.google.code.linkedinapi.client.oauth.LinkedInOAuthServiceFactory;
import com.google.code.linkedinapi.client.oauth.LinkedInRequestToken;

import java.util.ArrayList;
import java.util.List;

/**
 * Linkedin dialog
 *
 * @author Mukesh Kumar Yadav
 */
public class LinkedinDialog extends Dialog {
    public static LinkedInApiClientFactory factory;
    public static LinkedInOAuthService oAuthService;
    public static LinkedInRequestToken liToken;
    private ProgressDialog progressDialog = null;
    private WebView mWebView;
    /**
     * List of listener.
     */
    private List<OnVerifyListener> listeners = new ArrayList<OnVerifyListener>();

    /**
     * Construct a new LinkedIn dialog
     *
     * @param context        activity {@link Context}
     * @param progressDialog {@link ProgressDialog}
     */
    public LinkedinDialog(Context context, ProgressDialog progressDialog) {
        super(context);
        this.progressDialog = progressDialog;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);// must call before super.
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ln_dialog);
        this.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        mWebView = (WebView) findViewById(R.id.webkitWebView1);
        mWebView.getSettings().setJavaScriptEnabled(true);

        setWebView();
    }

    /**
     * set webview.
     */
    private void setWebView() {
        LinkedinDialog.oAuthService = LinkedInOAuthServiceFactory.getInstance()
                .createLinkedInOAuthService(SocialUtils.ConfigLinkedin.LINKEDIN_CONSUMER_KEY,
                        SocialUtils.ConfigLinkedin.LINKEDIN_CONSUMER_SECRET);
        LinkedinDialog.factory = LinkedInApiClientFactory.newInstance(
                SocialUtils.ConfigLinkedin.LINKEDIN_CONSUMER_KEY, SocialUtils.ConfigLinkedin.LINKEDIN_CONSUMER_SECRET);

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    LinkedinDialog.liToken = LinkedinDialog.oAuthService
                            .getOAuthRequestToken(SocialUtils.ConfigLinkedin.OAUTH_CALLBACK_URL);
                } catch (LinkedInOAuthServiceException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (LinkedinDialog.liToken.getAuthorizationUrl()!=null) {
                    mWebView.loadUrl(LinkedinDialog.liToken.getAuthorizationUrl());
                    mWebView.setWebViewClient(new HelloWebViewClient());

                    mWebView.setPictureListener(new PictureListener() {
                        @Override
                        public void onNewPicture(WebView view, Picture picture) {
                            if (progressDialog != null && progressDialog.isShowing()) {
                                try {
                                    progressDialog.dismiss();
                                    progressDialog = null;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.e("", "Dismiss dialog fault. Let it be :)");
                                }
                            }
                        }
                    });
                }else{
                    if (progressDialog != null && progressDialog.isShowing()) {
                        try {
                            progressDialog.dismiss();
                            progressDialog = null;
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e("", "Dismiss dialog fault. Let it be :)");
                        }
                    }
                }
            }
        }.execute();
    }

    /**
     * Register a callback to be invoked when authentication have finished.
     *
     * @param data The callback that will run
     */
    public void setVerifierListener(OnVerifyListener data) {
        listeners.add(data);
    }

    /**
     * Listener for oauth_verifier.
     */
    public interface OnVerifyListener {
        /**
         * invoked when authentication have finished.
         *
         * @param verifier oauth_verifier code.
         */
        public void onVerify(String verifier);
    }

    /**
     * webview client for internal url loading
     * callback url: "https://www.linkedin.com/uas/oauth/mukeshyadav4u.blogspot.in"
     */
    class HelloWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.contains(SocialUtils.ConfigLinkedin.OAUTH_CALLBACK_URL)) {
                Uri uri = Uri.parse(url);
                String verifier = uri.getQueryParameter("oauth_verifier");

                cancel();

                for (OnVerifyListener d : listeners) {
                    // call listener method
                    d.onVerify(verifier);
                }
            } else if (url
                    .contains("https://www.linkedin.com/uas/oauth/mukeshyadav4u.blogspot.in")) {
                cancel();
            } else {
                Log.i("LinkedinSample", "url: " + url);
                view.loadUrl(url);
            }

            return true;
        }
    }
}