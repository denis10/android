package com.example.dialogs;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.helpers.DbCommonHelper;
import com.example.objects.FullDebt;
import com.example.task16_02denis.app.R;
import com.example.utils.FileUtils;
import com.example.utils.ImageUtils;

import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * Created by dev1 on 2/26/14.
 */

public class DebtItemDialogFragment extends DialogFragment {
    private TextView mEnd;
    private TextView mName;
    private TextView mSecondName;
    private TextView mPhone;
    private TextView mEmail;
    private TextView mDebtName;
    private View mLayout;
    private ImageView mImageView;
    private View mDebtNameLayout;
    private View mDebtorPhoneLayout;
    private TextView mDebtorEmailTitle;
    private TextView mDebtorPhotoTitle;

    public static DebtItemDialogFragment newInstance(FullDebt debt) {
        DebtItemDialogFragment f = new DebtItemDialogFragment();
        Bundle args = new Bundle();
        args.putSerializable(DbCommonHelper.DEBT_UNIQUE_ID, debt);
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        getDialog().setTitle(R.string.title_debt_dialog);

        View v = inflater.inflate(R.layout.debt_info_item_dialog, null);
        mEnd = (TextView) v.findViewById(R.id.debt_end_dialog_txt);
        mName = (TextView) v.findViewById(R.id.debtor_name_list_dialog_txt);
        mSecondName = (TextView) v.findViewById(R.id.debtor_secondname_list_dialog_txt);
        mPhone = (TextView) v.findViewById(R.id.debtor_phone_list_dialog_txt);
        mEmail = (TextView) v.findViewById(R.id.debtor_email_list_dialog_txt);
        mDebtName = (TextView) v.findViewById(R.id.debt_name_list_dialog_txt);
        mLayout = v.findViewById(R.id.debt_info_dialog_layout);
        mDebtNameLayout = v.findViewById(R.id.debt_name_layout);
        mDebtorPhoneLayout = v.findViewById(R.id.debtor_phone_layout);
        mDebtorEmailTitle = (TextView) v.findViewById(R.id.debtor_email_title);
        mDebtorPhotoTitle= (TextView) v.findViewById(R.id.debtor_photo_title);

        getDialog().setCanceledOnTouchOutside(true);
        mImageView = (ImageView) v.findViewById(R.id.debtor_photo_dialog_view);
        mLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });

        FullDebt debt = (FullDebt) getArguments().getSerializable(DbCommonHelper.DEBT_UNIQUE_ID);
        mEnd.setText(debt.getDebtEnd());
        if (TextUtils.isEmpty(debt.getDebtName())) {
            mDebtNameLayout.setVisibility(View.GONE);
        } else {
            mDebtName.setText(debt.getDebtName());
        }
        mName.setText(debt.getName());
        mSecondName.setText(debt.getSecondName());
        if (TextUtils.isEmpty(debt.getPhone())) {
            mDebtorPhoneLayout.setVisibility(View.GONE);
        } else {
            mPhone.setText(debt.getPhone());
        }
        if (TextUtils.isEmpty(debt.getEmail())) {
            mDebtorEmailTitle.setVisibility(View.GONE);
            mEmail.setVisibility(View.GONE);
        } else {
            mEmail.setText(debt.getEmail());
        }
        String uri = debt.getPhoto();
        if(TextUtils.isEmpty(uri)){
            mDebtorPhotoTitle.setVisibility(View.GONE);
            mImageView.setVisibility(View.GONE);
        }else {
            setDebtImageView(uri);
        }


        return v;
    }

    private void setDebtImageView(String uri) {
        int height = 100;
        int width = 100;
        if (uri != null) {
            if (uri.contains("content://com.android.contacts/contacts/")) {
                try {
                    InputStream input = getActivity().getContentResolver().openInputStream(Uri.parse(uri));
                    Bitmap bitmap = BitmapFactory.decodeStream(input);
                    mImageView.setImageBitmap(bitmap);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else {
                Bitmap bitmap = ImageUtils.getImage(FileUtils.getRealPathFromURI(getActivity(), Uri.parse(uri)), height, width);

                mImageView.setImageBitmap(bitmap);
            }
        }
    }
}
