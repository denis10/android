package com.example.dialogs;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.interfaces.OnSaveContactListener;
import com.example.task16_02denis.app.R;

/**
 * Created by dev1 on 3/6/14.
 */
public class SaveContactDialog extends DialogFragment {
    public OnSaveContactListener mContactListener;
    private Button btn1;
    private Button btn2;

    public static SaveContactDialog newInstance() {
        SaveContactDialog f = new SaveContactDialog();
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        getDialog().setTitle(R.string.save_in_phone_book);

        View v = inflater.inflate(R.layout.choose_photo_dialog, null);
        btn1 = (Button) v.findViewById(R.id.choose_sd_btn);
        btn1.setText(R.string.ok);
        btn2 = (Button) v.findViewById(R.id.choose_capture_btn);
        btn2.setText(R.string.no);

        btn1.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContactListener.addContact();
                myDismiss();
                getDialog().dismiss();
            }
        });

        btn2.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                myDismiss();
                getDialog().dismiss();
            }
        });

        getDialog().setCanceledOnTouchOutside(false);

        return v;
    }

    private void myDismiss() {
        Intent intent = new Intent();
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().finish();
    }

}
