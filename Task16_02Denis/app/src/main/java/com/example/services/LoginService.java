package com.example.services;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;

import com.example.helpers.DbCommonHelper;
import com.example.objects.User;
import com.example.task16_02denis.app.LoginActivity;
import com.example.utils.LoginUtils;
import com.example.utils.SynchronizeUtils;

import java.util.UUID;

/**
 * Created by dev1 on 3/7/14.
 */
public class LoginService extends IntentService {
    private int result = Activity.RESULT_CANCELED;

    public LoginService() {
        super("LoginService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        boolean found = false;
        boolean connectionError = false;
        String mUserUniqueId = "";
        String mEmail = intent.getExtras().getString(LoginActivity.EMAIL_USER);
        String mPassword = intent.getExtras().getString(LoginActivity.PASSWORD_USER);

        User user = DbCommonHelper.getUserObject(mEmail, this);
        if (user != null) {//search in db
            mUserUniqueId = user.getUserUniqueId();
            found = user.getPassword().equals(mPassword);
        } else {
            if (SynchronizeUtils.isNetworkConnected(this)) {//try to connect
                user = new User(mEmail, mPassword);
                user = LoginUtils.findUserByLogin(user, this);
                if (user == null) {//new user
                    mUserUniqueId = UUID.randomUUID().toString();
                    user = new User(mEmail, mPassword, mUserUniqueId);
                    if (LoginUtils.postUserUpload(user, this)) {//if created
                        DbCommonHelper.insertUser(user, this);//insert in db
                        found = true;
                    } else {
                        found = false;
                    }
                } else {//found on site
                    if (user.getPassword().equals(mPassword)) {
                        mUserUniqueId = user.getUserUniqueId();
                        found = true;
                        DbCommonHelper.insertUser(user, this);//present on site, absent in db
                    } else {
                        found = false;
                    }
                }
            } else {
                found = false;
                connectionError = true;
            }
        }
        result = Activity.RESULT_OK;

        publishResults(mUserUniqueId, found, connectionError, result);
    }

    private void publishResults(String uniqueId, boolean found, boolean connectionError, int result) {
        Intent intent = new Intent(LoginActivity.NOTIFICATION);
        intent.putExtra(LoginActivity.UNIQUE_USER_ID, uniqueId);
        intent.putExtra(LoginActivity.FIND_USER, found);
        intent.putExtra(LoginActivity.CONNECTION_ERROR, connectionError);
        intent.putExtra(LoginActivity.RESULT, result);
        sendBroadcast(intent);
    }
}
