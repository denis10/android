package com.example.services;

import android.app.IntentService;
import android.content.Intent;

import com.example.objects.Debt;
import com.example.objects.Debtor;
import com.example.utils.SynchronizeUtils;

/**
 * Created by dev1 on 3/7/14.
 */
public class ChangeObjectIntentService extends IntentService {

    public ChangeObjectIntentService() {
        super("ChangeObjectIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        int actionType=intent.getExtras().getInt(SynchronizeUtils.ACTION_TYPE);
        int objectType=intent.getExtras().getInt(SynchronizeUtils.OBJECT_TYPE);
        Debtor debtor;
        Debt debt;
        switch (actionType){
            case SynchronizeUtils.OBJECT_UPLOAD:
                if (objectType== SynchronizeUtils.DEBTOR){
                    debtor=(Debtor)intent.getExtras().getSerializable(SynchronizeUtils.DEBTOR_OBJECT_ID);
                    SynchronizeUtils.postDebtorUpload(debtor, this);
                }else{
                    debt=(Debt)intent.getExtras().getSerializable(SynchronizeUtils.DEBT_OBJECT_ID);
                    SynchronizeUtils.postDebtUpload(debt, this);
                }
                break;
            case SynchronizeUtils.OBJECT_UPDATE:
                if (objectType== SynchronizeUtils.DEBTOR){
                    debtor=(Debtor)intent.getExtras().getSerializable(SynchronizeUtils.DEBTOR_OBJECT_ID);
                    SynchronizeUtils.updateDeleteDebtor(debtor, this, actionType);
                }else{
                    debt=(Debt)intent.getExtras().getSerializable(SynchronizeUtils.DEBT_OBJECT_ID);
                    SynchronizeUtils.updateDeleteDebt(debt, this, actionType);
                }
                break;
            case SynchronizeUtils.OBJECT_DELETE:
                if (objectType== SynchronizeUtils.DEBTOR){
                    debtor=(Debtor)intent.getExtras().getSerializable(SynchronizeUtils.DEBTOR_OBJECT_ID);
                    SynchronizeUtils.updateDeleteDebtor(debtor, this, actionType);
                }else{
                    debt=(Debt)intent.getExtras().getSerializable(SynchronizeUtils.DEBT_OBJECT_ID);
                    SynchronizeUtils.updateDeleteDebt(debt, this, actionType);
                }
                break;
        }
    }
}
