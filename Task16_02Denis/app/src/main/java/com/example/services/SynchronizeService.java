package com.example.services;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;

import com.example.fragments.DebtListFragment;
import com.example.task16_02denis.app.LoginActivity;
import com.example.task16_02denis.app.MainActivity;
import com.example.utils.LoginUtils;
import com.example.utils.SynchronizeUtils;

/**
 * Created by dev1 on 3/7/14.
 */
public class SynchronizeService extends IntentService {
    private int result = Activity.RESULT_CANCELED;
    public SynchronizeService() {
        super("SynchronizeService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        SynchronizeUtils.dataSynchronize(this);

        result = Activity.RESULT_OK;
        publishResults(result);
    }

    private void publishResults(int result) {
        Intent intent = new Intent(DebtListFragment.NOTIFICATION);
        intent.putExtra(LoginActivity.RESULT, result);
        sendBroadcast(intent);
    }
}
