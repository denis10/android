package com.example.utils;

import com.example.task16_02denis.app.R;

/**
 * Created by denis on 23.03.14.
 */
public class SocialUtils {
    public interface ConfigLinkedin {

        public static String LINKEDIN_CONSUMER_KEY = "77z64wy43fbxeo";
        public static String LINKEDIN_CONSUMER_SECRET = "cyPZ2f7KlzMO3iHc";

        public static String scopeParams = "rw_nus+r_basicprofile";

        public static String OAUTH_CALLBACK_SCHEME = "x-oauthflow-linkedin";
        public static String OAUTH_CALLBACK_HOST = "callback";
        public static String OAUTH_CALLBACK_URL = OAUTH_CALLBACK_SCHEME + "://" + OAUTH_CALLBACK_HOST;
    }
    public interface Constants {
        java.lang.String FACEBOOK = "facebook";
        java.lang.String CONTACT = "contact";
        java.lang.String FOURSQUARE = "foursquare";
        java.lang.String GOOGLE = "google";
        java.lang.String HOTMAIL = "hotmail";
        java.lang.String LINKEDIN = "linkedin";
        java.lang.String MYSPACE = "myspace";
        java.lang.String TWITTER = "twitter";
        java.lang.String YAHOO = "yahoo";
        java.lang.String SALESFORCE = "salesforce";
        java.lang.String YAMMER = "yammer";
        java.lang.String MENDELEY = "mendeley";
        java.lang.String RUNKEEPER = "runkeeper";
        java.lang.String INSTAGRAM = "instagram";
        java.lang.String GOOGLE_PLUS = "googleplus";
    }
    public enum Provider {
        FACEBOOK(Constants.FACEBOOK, "fbconnect://success", "fbconnect://success?error_reason"),
        CONTACT(Constants.CONTACT, "fbconnect://success", "fbconnect://success?error_reason"),
        TWITTER(
                Constants.TWITTER, "http://socialauth.in/socialauthdemo/socialAuthSuccessAction.do",
                "http://socialauth.in/socialauthdemo/socialAuthSuccessAction.do?denied"), LINKEDIN(Constants.LINKEDIN,
                "http://socialauth.in/socialauthdemo/socialAuthSuccessAction.do",
                "http://socialauth.in/socialauthdemo/socialAuthSuccessAction.do?oauth_problem"), MYSPACE(
                Constants.MYSPACE, "http://socialauth.in", "http://socialauth.in/?oauth_problem"), RUNKEEPER(
                Constants.RUNKEEPER, "http://socialauth.in/socialauthdemo/socialauthSuccessAction.do",
                "http://socialauth.in/socialauthdemo/socialauthSuccessAction.do/?error"), YAHOO(Constants.YAHOO,
                "http://socialauth.in/socialauthdemo", "http://socialauth.in/socialauthdemo/?oauth_problem"), FOURSQUARE(
                Constants.FOURSQUARE, "http://socialauth.in/socialauthdemo/socialAuthSuccessAction.do",
                "http://socialauth.in/socialauthdemo/socialAuthSuccessAction.do/?oauth_problem"), GOOGLE(
                Constants.GOOGLE, "http://socialauth.in/socialauthdemo",
                "http://socialauth.in/socialauthdemo/?oauth_problem"), YAMMER(Constants.YAMMER,
                "http://socialauth.in/socialauthdemo/socialAuthSuccessAction.do",
                "http://socialauth.in/socialauthdemo/socialAuthSuccessAction.do/?oauth_problem"), SALESFORCE(
                Constants.SALESFORCE, "https://socialauth.in:8443/socialauthdemo/socialAuthSuccessAction.do",
                "http://socialauth.in/socialauthdemo/socialAuthSuccessAction.do/?oauth_problem"), GOOGLEPLUS(
                Constants.GOOGLE_PLUS, "http://socialauth.in/socialauthdemo/socialAuthSuccessAction.do",
                "http://socialauth.in/socialauthdemo/socialAuthSuccessAction.do/?oauth_problem");

        private String name;
        private String cancelUri;
        private String callbackUri;

        Provider(String name, String callbackUri, String cancelUri) {
            this.name = name;
            this.cancelUri = cancelUri;
            this.callbackUri = callbackUri;
        }
        String getCancelUri() {
            return this.cancelUri;
        }
        String getCallBackUri() {
            return this.callbackUri;
        }
        public void setCallBackUri(String callbackUri) {
            this.callbackUri = callbackUri;
        }
        @Override
        public String toString() {
            return name;
        }
    }
    private static Provider authProviders[]= new Provider[Provider.values().length];
    private static int authProviderLogos[] =new int[Provider.values().length];
    private static int providerCount;
    private static String[] providerNames;
    private static int[] providerLogos;
    public static void init (){
        providerCount=0;//clear!

        addProvider(Provider.FACEBOOK,R.drawable.facebook);
        addProvider(Provider.CONTACT,R.drawable.vk);
        addProvider(Provider.LINKEDIN,R.drawable.linkedin);
        addProvider(Provider.GOOGLEPLUS,R.drawable.googleplus);

        providerNames = new String[providerCount];
        providerLogos = new int[providerCount];

        for (int i = 0; i < providerCount; i++) {
            providerNames[i] = authProviders[i].toString();
            providerLogos[i] = authProviderLogos[i];
        }
    }
    private static void addProvider(Provider provider, int logo) {
        authProviders[providerCount] = provider;
        authProviderLogos[providerCount] = logo;
        providerCount++;
    }
    public static String[] getProviderNames(){
        return providerNames;
    }
    public static int[] getProviderLogos(){
        return providerLogos;
    }

}
