package com.example.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.example.helpers.DbCommonHelper;
import com.example.objects.Debt;
import com.example.objects.Debtor;
import com.example.objects.WebDebt;
import com.example.objects.WebDebtor;
import com.example.parsing.DebtResult;
import com.example.parsing.DebtorResult;
import com.example.parsing.SearchDebtResponse;
import com.example.parsing.SearchDebtorResponse;
import com.example.task16_02denis.app.LoginActivity;
import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dev1 on 3/3/14.
 */
public class SynchronizeUtils {
    public static final String URL = "url";
    public static final String DEBTOR_OBJECT_ID = "objectId";
    public static final String DEBT_OBJECT_ID = "objectId";

    public static final int OBJECT_UPLOAD = 1;
    public static final int OBJECT_UPDATE = 2;
    public static final int OBJECT_DELETE = 3;

    public static final String OBJECT_TYPE = "objectType";
    public static final String ACTION_TYPE = "actionType";
    public static final int DEBTOR = 1;
    public static final int DEBT = 2;

    public static final String APPLICATION_ID = "Yy8hiZxZIsDjkjk4B1sC8M4k6timVxHEBYIVEJ8T";
    public static final String REST_API_KEY = "4cvhvqUJcz70LGz2oCbIvjF8TaHSjW57eXQsGVlB";

    private static final String DEBTOR_CLASS_NAME = "debtor";
    private static final String DEBTOR_CLASS_URL = "https://api.parse.com/1/classes/" + DEBTOR_CLASS_NAME;
    private static final String DEBT_CLASS_NAME = "debt";
    private static final String DEBT_CLASS_URL = "https://api.parse.com/1/classes/" + DEBT_CLASS_NAME;
    private static final String FILES = "https://api.parse.com/1/files/";

    private final static int TIMEOUT = 10000;

    public static void postDebtorUpload(Debtor debtor, Context context) {
        String photoUrl = "";
        if (!TextUtils.isEmpty(debtor.getPhoto())) {
            String url = FILES + new File(Uri.parse(debtor.getPhoto()).getPath()).getName();
            Bitmap bmp = ImageUtils.getBitmap(context, debtor.getPhoto());
            if (bmp != null) {
                photoUrl = uploadPhoto(url, bmp, context);
            }
        }
        changeObject(DEBTOR_CLASS_URL, new WebDebtor(photoUrl, debtor), context, OBJECT_UPLOAD);
    }

    private static void postDebtorUpload(Debtor debtor, DebtorResult result, Context context) {
        String url = FILES + new File(Uri.parse(debtor.getPhoto()).getPath()).getName();
        Bitmap bmp = ImageUtils.getBitmap(context, debtor.getPhoto());
        String photoUrl = uploadPhoto(url, bmp, context);
        String urlUpdateDebtor = DEBTOR_CLASS_URL + "/" + result.debtorObjectId;
        changeObject(urlUpdateDebtor, new WebDebtor(photoUrl, debtor), context, OBJECT_UPDATE);
    }

    public static void updateDeleteDebtor(Debtor debtor, Context context, int actionType) {
        HttpResponse getResponse = prepareResponse(DEBTOR_CLASS_URL);
        if (getResponse != null) {
            HttpEntity responseEntity = getResponse.getEntity();
            BufferedHttpEntity httpEntity = null;
            BufferedReader reader = null;
            try {
                httpEntity = new BufferedHttpEntity(responseEntity);
                reader = new BufferedReader(new InputStreamReader(httpEntity.getContent(), "UTF-8"));
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            //Log.i("log_tag", "get responce:\n" + s);
            Gson gson = new Gson();
            SearchDebtorResponse response = gson.fromJson(reader, SearchDebtorResponse.class);
            List<DebtorResult> results = response.results;
            for (DebtorResult result : results) {
                //Log.i("log_tag", "result.name=" + result.name);
                if (result.getDebtorUniqueId().equals(debtor.getDebtorUniqueId())) {
                    if (actionType == OBJECT_DELETE) {
                        String url = DEBTOR_CLASS_URL + "/" + result.debtorObjectId;
                        deleteObject(url, context);
                        updateDeleteDebtsFromDebtor(debtor, context, OBJECT_DELETE);
                        break;
                    } else if (actionType == OBJECT_UPDATE) {
                        if (debtor.getPhoto() == null || result.getPhoto().equals(debtor.getPhoto())) {
                            String urlUpdateDebtor = DEBTOR_CLASS_URL + "/" + result.debtorObjectId;
                            changeObject(urlUpdateDebtor, new WebDebtor(result.url, debtor), context, OBJECT_UPDATE);
                        } else {
                            postDebtorUpload(debtor, result, context);
                        }
                        break;
                    }
                }
            }
        }
    }

    //-------------------------------------------------------------------------------------------------------
    public static void postDebtUpload(Debt debt, Context context) {
        String photoUrl = "";
        if (!TextUtils.isEmpty(debt.getDebtPhotoPath())) {
            String url = FILES + new File(Uri.parse(debt.getDebtPhotoPath()).getPath()).getName();
            Bitmap bmp = ImageUtils.getBitmap(context, debt.getDebtPhotoPath());
            if (bmp != null) {
                photoUrl = uploadPhoto(url, bmp, context);
            }
        }
        changeObject(DEBT_CLASS_URL, new WebDebt(photoUrl, debt), context, OBJECT_UPLOAD);
    }

    private static void postDebtUpload(Debt debt, DebtResult result, Context context) {
        String url = FILES + new File(Uri.parse(debt.getDebtPhotoPath()).getPath()).getName();
        Bitmap bmp = ImageUtils.getBitmap(context, debt.getDebtPhotoPath());
        String photoUrl = uploadPhoto(url, bmp, context);
        String urlUpdateDebt = DEBT_CLASS_URL + "/" + result.getDebtObjectId();
        changeObject(urlUpdateDebt, new WebDebt(photoUrl, debt), context, OBJECT_UPDATE);
    }

    public static void updateDeleteDebt(Debt debt, Context context, int actionType) {
        HttpResponse getResponse = prepareResponse(DEBT_CLASS_URL);
        if (getResponse != null) {
            HttpEntity responseEntity = getResponse.getEntity();

            BufferedHttpEntity httpEntity = null;
            BufferedReader reader = null;
            try {
                httpEntity = new BufferedHttpEntity(responseEntity);
                reader = new BufferedReader(new InputStreamReader(httpEntity.getContent(), "UTF-8"));
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            //Log.i("log_tag", "get responce:\n" + s);
            Gson gson = new Gson();
            SearchDebtResponse response = gson.fromJson(reader, SearchDebtResponse.class);

            List<DebtResult> results = response.results;
            for (DebtResult result : results) {
                //Log.i("log_tag", "result.name=" + result.name);
                if (result.getDebtUniqueId().equals(debt.getDebtUniqueId())) {
                    if (actionType == OBJECT_DELETE) {
                        String url = DEBT_CLASS_URL + "/" + result.getDebtObjectId();
                        deleteObject(url, context);
                        break;
                    } else if (actionType == OBJECT_UPDATE) {
                        if (debt.getDebtPhotoPath() == null || result.getDebtPhotoPath().equals(debt.getDebtPhotoPath())) {
                            String urlUpdateDebt = DEBT_CLASS_URL + "/" + result.getDebtObjectId();
                            changeObject(urlUpdateDebt, new WebDebt(result.getUrl(), debt), context, OBJECT_UPDATE);
                        } else {
                            postDebtUpload(debt, result, context);
                        }
                        break;
                    }
                }
            }
        }
    }

    private static void updateDeleteDebtsFromDebtor(Debtor debtor, Context context, int actionType) {
        HttpResponse getResponse = prepareResponse(DEBT_CLASS_URL);
        if (getResponse != null) {
            HttpEntity responseEntity = getResponse.getEntity();

            BufferedHttpEntity httpEntity = null;
            BufferedReader reader = null;
            try {
                httpEntity = new BufferedHttpEntity(responseEntity);
                reader = new BufferedReader(new InputStreamReader(httpEntity.getContent(), "UTF-8"));
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            //Log.i("log_tag", "get responce:\n" + s);
            Gson gson = new Gson();
            SearchDebtResponse response = gson.fromJson(reader, SearchDebtResponse.class);
            List<DebtResult> results = response.results;
            for (DebtResult result : results) {
                //Log.i("log_tag", "result.name=" + result.name);
                if (result.getDebtorUniqueId().equals(debtor.getDebtorUniqueId())) {
                    if (actionType == OBJECT_DELETE) {
                        String url = DEBT_CLASS_URL + "/" + result.getDebtObjectId();
                        deleteObject(url, context);
                    }
                }
            }
        }
    }

    private static void deleteObject(String url, Context context) {
        HttpClient httpClient = prepareHttpClient();
        HttpDelete httpType = new HttpDelete(url);

        if (httpType != null) {
            httpType.setHeader("Accept", "application/json");
            httpType.setHeader("Content-type", "application/json");
            //
            httpType.setHeader("X-Parse-Application-Id", APPLICATION_ID);
            httpType.setHeader("X-Parse-REST-API-Key", REST_API_KEY);

            HttpResponse response;
            try {
                response = httpClient.execute(httpType);
                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                String sResponse;
                StringBuilder s = new StringBuilder();
                while ((sResponse = reader.readLine()) != null) {
                    s = s.append(sResponse);
                }
                Log.i("log_tag", "Send response:\n" + s);
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("log_tag", "Error in http connection " + e.toString());
            }
        }
    }

    private static void changeObject(String urlObjectItem, Object object, Context context, int actionType) {
        HttpClient httpClient = prepareHttpClient();
        HttpEntityEnclosingRequestBase httpType = null;
        switch (actionType) {
            case OBJECT_UPLOAD:
                httpType = new HttpPost(urlObjectItem);
                break;
            case OBJECT_UPDATE:
                httpType = new HttpPut(urlObjectItem);
                break;
        }
        if (httpType != null) {
            httpType.setHeader("Accept", "application/json");
            httpType.setHeader("Content-type", "application/json");
            //
            httpType.setHeader("X-Parse-Application-Id", APPLICATION_ID);
            httpType.setHeader("X-Parse-REST-API-Key", REST_API_KEY);
            HttpResponse response;
            try {
                //String json = jsonObject.toString();
                String json = new Gson().toJson(object);
                httpType.setEntity(new StringEntity(json, HTTP.UTF_8));
                response = httpClient.execute(httpType);
                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                String sResponse;
                StringBuilder s = new StringBuilder();
                while ((sResponse = reader.readLine()) != null) {
                    s = s.append(sResponse);
                }
                Log.i("log_tag", "Send response:\n" + s);
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("log_tag", "Error in http connection " + e.toString());
            }
        }
    }

    //--------------------------------------------------------------------------------------------------------
    private static String uploadPhoto(String url, Bitmap bmp, Context context) {
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        String photoUrl = null;
        bmp.compress(Bitmap.CompressFormat.JPEG, 95, bao);
        try {
            byte[] data = bao.toByteArray();
            //http://hmkcode.com/android-send-json-data-to-server/
            // 1. create HttpClient
            HttpClient httpClient = prepareHttpClient();
            // 2. make POST request to the given URL
            HttpPost postRequest = new HttpPost(url);
            postRequest.setEntity(new ByteArrayEntity(data));
            // 7. Set some headers to inform server about the type of the content
            //
            postRequest.setHeader("X-Parse-Application-Id", APPLICATION_ID);
            postRequest.setHeader("X-Parse-REST-API-Key", REST_API_KEY);
            postRequest.setHeader("Content-Type:", "image/jpeg");

            HttpResponse response = httpClient.execute(postRequest);

            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            String sResponse;
            StringBuilder s = new StringBuilder();
            while ((sResponse = reader.readLine()) != null) {
                s = s.append(sResponse);
            }
            //Log.i("log_tag", "Send response:\n" + s);
            photoUrl = s.substring(s.indexOf("http"), s.indexOf("\","));
            Log.i("log_tag", "photoUrl:\n" + photoUrl);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("log_tag", "Error in http connection " + e.toString());
        }
        return photoUrl;
    }

    //-----------------------------------------------------------------------------------------------
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            return false;
        } else
            return true;
    }

    //----------------------------------------------------------------------------------------------
    public static HttpResponse prepareResponse(String url) {
        HttpClient httpClient = prepareHttpClient();
        HttpGet get = new HttpGet(url);
        get.setHeader("Accept", "application/json");
        get.setHeader("Content-type", "application/json");

        get.setHeader("X-Parse-Application-Id", APPLICATION_ID);
        get.setHeader("X-Parse-REST-API-Key", REST_API_KEY);

        HttpResponse getResponse = null;
        try {
            getResponse = httpClient.execute(get);
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return getResponse;
    }

    public static HttpClient prepareHttpClient() {
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, TIMEOUT);
        HttpConnectionParams.setSoTimeout(httpParameters, TIMEOUT);
        return new DefaultHttpClient(httpParameters);
    }

    //----------------------------------------------------------------------------------------------
    public static void dataSynchronize(Context context) {//in SynchronizeService
        HttpResponse getResponse = prepareResponse(DEBTOR_CLASS_URL);
        if (getResponse != null) {
            HttpEntity responseEntity = getResponse.getEntity();
            BufferedHttpEntity httpEntity = null;
            BufferedReader reader = null;
            try {
                httpEntity = new BufferedHttpEntity(responseEntity);
                reader = new BufferedReader(new InputStreamReader(httpEntity.getContent(), "UTF-8"));
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            //Log.i("log_tag", "get responce:\n" + s);
            Gson gson = new Gson();
            SearchDebtorResponse debtorResponse = gson.fromJson(reader, SearchDebtorResponse.class);
            List<DebtorResult> debtorResults = debtorResponse.results;
            ArrayList<Debtor> debtors = DbCommonHelper.getDebtorsFromDB(context);//in service
            ArrayList<Debt> debts = DbCommonHelper.getDebtsFromDB(context);

            String unique = LoginActivity.getUserUniqueId();
            if (debtors.size() == 0 && debts.size() == 0) {
                for (DebtorResult result : debtorResults) {
                    if (result.getUserUniqueId().equals(unique)) {
                        Debtor debtor = new Debtor(result.getId(),
                                result.getName(),
                                result.getSecondName(),
                                result.getPhone(),
                                result.getEmail(),
                                result.getPhoto(),
                                result.getDebtorUniqueId(),
                                result.getUserUniqueId());

                        debtsDownloadSynchronize(result.getDebtorUniqueId(), context);

                        DbCommonHelper.insertDebtor(debtor, context);
                    }
                }
            } else {
                for (DebtorResult result : debtorResults) {
                    if (result.getUserUniqueId().equals(LoginActivity.getUserUniqueId())) {
                        boolean delete = true;
                        for (Debtor debtor : debtors) {
                            if (result.getDebtorUniqueId().equals(debtor.getDebtorUniqueId())) {
                                // TODO Compare
                                if (!debtor.equals(result)) {
                                    updateDeleteDebtor(debtor, context, OBJECT_UPDATE);
                                }
                                delete = false;
                                break;
                            }
                        }
                        if (delete) {
                            Debtor debtor = new Debtor(result.getId(),
                                    result.getName(),
                                    result.getSecondName(),
                                    result.getPhone(),
                                    result.getEmail(),
                                    result.getPhoto(),
                                    result.getDebtorUniqueId(),
                                    result.getUserUniqueId());
                            updateDeleteDebtor(debtor, context, OBJECT_DELETE);
                        }
                    }
                }
                for (Debtor debtor : debtors) {
                    boolean upload = true;
                    for (DebtorResult result : debtorResults) {
                        if (result.getUserUniqueId().equals(LoginActivity.getUserUniqueId())) {
                            if (result.getDebtorUniqueId().equals(debtor.getDebtorUniqueId())) {
                                upload = false;
                                break;
                            }
                        }
                    }
                    if (upload) {
                        postDebtorUpload(debtor, context);
                    }
                }
                dataAfterDebtorsSynchronize(context);
            }
        }
    }

    private static void debtsDownloadSynchronize(String debtorUniqueId, Context context) {
        HttpResponse getResponse = prepareResponse(DEBT_CLASS_URL);
        if (getResponse != null) {
            HttpEntity responseEntity = getResponse.getEntity();

            BufferedHttpEntity httpEntity = null;
            BufferedReader reader = null;
            try {
                httpEntity = new BufferedHttpEntity(responseEntity);
                reader = new BufferedReader(new InputStreamReader(httpEntity.getContent(), "UTF-8"));
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            Gson gson = new Gson();
            SearchDebtResponse debtResponse = gson.fromJson(reader, SearchDebtResponse.class);
            List<DebtResult> debtResults = debtResponse.results;

            for (DebtResult result : debtResults) {
                if (result.getDebtorUniqueId().equals(debtorUniqueId)) {
                    Debt debt = new Debt(result.getId(),
                            result.getDebtUniqueId(),
                            result.getDebtorUniqueId(),
                            result.getUserUniqueID(),
                            result.getDebtStart(),
                            result.getDebtEnd(),
                            result.getDebtTotal(),
                            result.getDebtPhotoPath(),
                            result.getDebtCount(),
                            result.getDebtName());
                    DbCommonHelper.insertDebt(debt, context);
                }
            }
        }
    }

    public static void dataAfterDebtorsSynchronize(Context context) {
        HttpResponse getResponse = prepareResponse(DEBTOR_CLASS_URL);
        if (getResponse != null) {
            HttpEntity responseEntity = getResponse.getEntity();

            BufferedHttpEntity httpEntity = null;
            BufferedReader reader = null;
            try {
                httpEntity = new BufferedHttpEntity(responseEntity);
                reader = new BufferedReader(new InputStreamReader(httpEntity.getContent(), "UTF-8"));
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            //Log.i("log_tag", "get responce:\n" + s);

            Gson gson = new Gson();
            SearchDebtorResponse debtorResponse = gson.fromJson(reader, SearchDebtorResponse.class);
            List<DebtorResult> debtorResults = debtorResponse.results;
            ArrayList<Debtor> debtors = DbCommonHelper.getDebtorsFromDB(context);

            for (DebtorResult result : debtorResults) {
                if (result.getUserUniqueId().equals(LoginActivity.getUserUniqueId())) {
                    for (Debtor debtor : debtors) {
                        if (result.getDebtorUniqueId().equals(debtor.getDebtorUniqueId())) {
                            debtsUpdateDeleteSynchronize(result.getDebtorUniqueId(), context);
                            break;
                        }
                    }
                }

            }
        }
    }

    private static void debtsUpdateDeleteSynchronize(String debtorUniqueId, Context context) {
        HttpResponse getResponse = prepareResponse(DEBT_CLASS_URL);
        if (getResponse != null) {
            HttpEntity responseEntity = getResponse.getEntity();

            BufferedHttpEntity httpEntity = null;
            BufferedReader reader = null;
            try {
                httpEntity = new BufferedHttpEntity(responseEntity);
                reader = new BufferedReader(new InputStreamReader(httpEntity.getContent(), "UTF-8"));
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }

            Gson gson = new Gson();
            SearchDebtResponse debtResponse = gson.fromJson(reader, SearchDebtResponse.class);
            List<DebtResult> debtResults = debtResponse.results;
            ArrayList<Debt> debts = DbCommonHelper.getDebtsFromDB(context);

            for (DebtResult result : debtResults) {
                if (result.getDebtorUniqueId().equals(debtorUniqueId)) {
                    boolean delete = true;
                    for (Debt debt : debts) {
                        if (result.getDebtUniqueId().equals(debt.getDebtUniqueId())) {
                            // TODO Compare
                            if (!debt.equals(result)) {
                                updateDeleteDebt(debt, context, OBJECT_UPDATE);
                            }
                            delete = false;
                            break;
                        }
                    }
                    if (delete) {
                        String url = DEBT_CLASS_URL + "/" + result.getDebtObjectId();
                        deleteObject(url, context);
                    }
                }
            }
            for (Debt debt : debts) {
                boolean upload = true;
                for (DebtResult result : debtResults) {
                    if (result.getUserUniqueID().equals(LoginActivity.getUserUniqueId())) {
                        if (result.getDebtorUniqueId().equals(debt.getDebtorUniqueId())) {
                            upload = false;
                            break;
                        }
                    }
                }
                if (upload) {
                    postDebtUpload(debt, context);
                }
            }
        }
    }
}
