package com.example.utils;

import android.database.Cursor;

import com.example.fragments.DebtListFragment;
import com.example.helpers.DbCommonHelper;
import com.example.objects.Debt;
import com.example.objects.Debtor;
import com.example.objects.User;

/**
 * Created by dev1 on 3/11/14.
 */
public class ObjectsUtils {
    public static Debt createDebtWithSetIdFromCursor(Cursor c){
        int idColIndex = c.getColumnIndex(DebtListFragment.FIRST_TABLE_ID);
        int debtUniqueId = c.getColumnIndex(DbCommonHelper.DEBT_UNIQUE_ID);
        int debtorUniqueId = c.getColumnIndex(DbCommonHelper.DEBTOR_UNIQUE_ID);
        int userUniqueId = c.getColumnIndex(DebtListFragment.FIRST_TABLE_USER_ID);
        int debtStart = c.getColumnIndex(DbCommonHelper.DEBT_START);
        int debtEnd = c.getColumnIndex(DbCommonHelper.DEBT_END);
        int debtTotal = c.getColumnIndex(DbCommonHelper.DEBT_TOTAL);
        int debtPhotoPath = c.getColumnIndex(DbCommonHelper.DEBT_PHOTO_PATH);
        int debtCount = c.getColumnIndex(DbCommonHelper.DEBT_COUNT);
        int debtName = c.getColumnIndex(DbCommonHelper.DEBT_NAME);
        Debt debt = new Debt(c.getLong(idColIndex),
                c.getString(debtUniqueId),
                c.getString(debtorUniqueId),
                c.getString(userUniqueId),
                c.getString(debtStart),
                c.getString(debtEnd),
                c.getFloat(debtTotal),
                c.getString(debtPhotoPath),
                c.getInt(debtCount),
                c.getString(debtName)
        );
        return debt;
    }
    public static Debt createDebtWithRealIdFromCursor(Cursor c){
        int idColIndex = c.getColumnIndex("_id");
        int debtUniqueId = c.getColumnIndex(DbCommonHelper.DEBT_UNIQUE_ID);
        int debtorUniqueId = c.getColumnIndex(DbCommonHelper.DEBTOR_UNIQUE_ID);
        int userUniqueId = c.getColumnIndex(DebtListFragment.FIRST_TABLE_USER_ID);
        int debtStart = c.getColumnIndex(DbCommonHelper.DEBT_START);
        int debtEnd = c.getColumnIndex(DbCommonHelper.DEBT_END);
        int debtTotal = c.getColumnIndex(DbCommonHelper.DEBT_TOTAL);
        int debtPhotoPath = c.getColumnIndex(DbCommonHelper.DEBT_PHOTO_PATH);
        int debtCount = c.getColumnIndex(DbCommonHelper.DEBT_COUNT);
        int debtName = c.getColumnIndex(DbCommonHelper.DEBT_NAME);
        Debt debt = new Debt(c.getLong(idColIndex),
                c.getString(debtUniqueId),
                c.getString(debtorUniqueId),
                c.getString(userUniqueId),
                c.getString(debtStart),
                c.getString(debtEnd),
                c.getFloat(debtTotal),
                c.getString(debtPhotoPath),
                c.getInt(debtCount),
                c.getString(debtName)
        );
        return debt;
    }
    public static Debtor createDebtorFromCursor(Cursor c){
        int debtorIdSecondTable = c.getColumnIndex("_id");
        int debtorName = c.getColumnIndex(DbCommonHelper.DEBTOR_NAME);
        int debtorSecondName = c.getColumnIndex(DbCommonHelper.DEBTOR_SECONDNAME);
        int debtorPhone = c.getColumnIndex(DbCommonHelper.DEBTOR_PHONE);
        int debtorEmail = c.getColumnIndex(DbCommonHelper.DEBTOR_EMAIL);
        int debtorPhoto = c.getColumnIndex(DbCommonHelper.DEBTOR_PHOTO);
        int debtorUniqueId = c.getColumnIndex(DbCommonHelper.DEBTOR_UNIQUE_ID);
        int userUniqueId = c.getColumnIndex(DbCommonHelper.USER_UNIQUE_ID);

        Debtor debtor = new Debtor(c.getLong(debtorIdSecondTable),
                c.getString(debtorName),
                c.getString(debtorSecondName),
                c.getString(debtorPhone),
                c.getString(debtorEmail),
                c.getString(debtorPhoto),
                c.getString(debtorUniqueId),
                c.getString(userUniqueId)
        );

        return debtor;
    }
    public static User createUserFromCursor(Cursor c){
        int idColIndex = c.getColumnIndex("_id");
        int loginIndex = c.getColumnIndex(DbCommonHelper.USER_LOGIN);
        int passwordIndex = c.getColumnIndex(DbCommonHelper.USER_PASSWORD);
        int uniqueIdIndex = c.getColumnIndex(DbCommonHelper.USER_UNIQUE_ID);
        User user = new User(c.getLong(idColIndex),
                c.getString(loginIndex),
                c.getString(passwordIndex),
                c.getString(uniqueIdIndex));

        return user;
    }
}
