package com.example.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.util.Log;

import com.example.objects.User;
import com.example.parsing.SearchUserResponse;
import com.example.parsing.UserResult;
import com.example.task16_02denis.app.MainActivity;
import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.entity.StringEntity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Created by dev1 on 3/4/14.
 */
public class LoginUtils {
    public static final int OBJECT_UPLOAD = 1;
    public static final int OBJECT_UPDATE = 2;
    public static final int OBJECT_DELETE = 3;

    private static final String USER_CLASS_NAME = "user";
    private static final String USER_CLASS_URL = "https://api.parse.com/1/classes/" + USER_CLASS_NAME;

    public static boolean postUserUpload(User user, Context context) {

        return changeUserObject(USER_CLASS_URL, user, context, OBJECT_UPLOAD);
    }
    private static boolean changeUserObject(String urlObjectItem, User user, Context context, int actionType) {
        HttpClient httpClient = SynchronizeUtils.prepareHttpClient();

        HttpEntityEnclosingRequestBase httpType = null;
        switch (actionType) {
            case OBJECT_UPLOAD:
                httpType = new HttpPost(urlObjectItem);
                break;
            case OBJECT_UPDATE:
                httpType = new HttpPut(urlObjectItem);
                break;
        }

        if (httpType != null) {
            httpType.setHeader("Accept", "application/json");
            httpType.setHeader("Content-type", "application/json");
            //
            httpType.setHeader("X-Parse-Application-Id", SynchronizeUtils.APPLICATION_ID);
            httpType.setHeader("X-Parse-REST-API-Key", SynchronizeUtils.REST_API_KEY);
            HttpResponse response;

            try {
                String json = new Gson().toJson(user);
                StringEntity se = new StringEntity(json);
                httpType.setEntity(se);

                response = httpClient.execute(httpType);
                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                String sResponse;
                StringBuilder s = new StringBuilder();
                while ((sResponse = reader.readLine()) != null) {
                    s = s.append(sResponse);
                }
                Log.i("log_tag", "Send response:\n" + s);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("log_tag", "Error in http connection " + e.toString());
            }
        }
        return false;
    }

    public static User findUserByLogin(User user, Context context) {
        HttpResponse getResponse = SynchronizeUtils.prepareResponse(USER_CLASS_URL);
        if (getResponse != null) {
            HttpEntity responseEntity = getResponse.getEntity();
            BufferedHttpEntity httpEntity = null;
            BufferedReader reader = null;
            try {
                httpEntity = new BufferedHttpEntity(responseEntity);
                reader = new BufferedReader(new InputStreamReader(httpEntity.getContent(), "UTF-8"));
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }

            Gson gson = new Gson();
            SearchUserResponse response = gson.fromJson(reader, SearchUserResponse.class);

            List<UserResult> results = response.results;
            for (UserResult result : results) {
                //Log.i("log_tag", "result.name=" + result.name);
                if (result.getLogin().equals(user.getLogin())) {
                    return new User(result.getLogin(),result.getPassword(),result.getUserUniqueId());
                }
            }
        }
        return null;
    }

    public static void setAutoOrientationEnabled(ContentResolver resolver, boolean enabled)
    {
        Settings.System.putInt(resolver, Settings.System.ACCELEROMETER_ROTATION, enabled ? 1 : 0);
    }
    public static void saveLoginPreferences(boolean state, Context context){
        SharedPreferences.Editor editor = context.getSharedPreferences(MainActivity.SYNCHRONIZING,Context.MODE_PRIVATE).edit();
        editor.putBoolean(MainActivity.SYNCHRONIZING, state);
        editor.commit();
    }
}
