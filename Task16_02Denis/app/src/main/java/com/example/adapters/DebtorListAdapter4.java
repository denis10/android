package com.example.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.CursorAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dialogs.PhotoDialogFragment;
import com.example.helpers.DbCommonHelper;
import com.example.task16_02denis.app.MainActivity;
import com.example.task16_02denis.app.R;
import com.example.utils.ImageUtils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by dev1 on 2/24/14.
 */
public class DebtorListAdapter4 extends CursorAdapter {
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private ImageLoader mImageLoader;
    private DisplayImageOptions mOptions;
    private ViewHolder holder;
    private FragmentManager fm;
    private Context mContext;


    public DebtorListAdapter4(Context context, Cursor c, boolean autoRequery, FragmentManager fm) {
        super(context, c, autoRequery);
        this.mContext=context;
        this.mImageLoader = ImageLoader.getInstance();
        this.fm = fm;
        this.mOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_stub)
                .showImageForEmptyUri(R.drawable.ic_empty)
                .showImageOnFail(R.drawable.ic_error)
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .displayer(new RoundedBitmapDisplayer(MainActivity.IMAGE_CURVATURE))
                .resetViewBeforeLoading(true)//first picture clean
                .build();

    }

    @Override
    public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
        if (getFilterQueryProvider() != null)
        {
            return getFilterQueryProvider().runQuery(constraint);
        }
        if (TextUtils.isEmpty(constraint)){
            return getCursor();
        }
        Cursor c = DbCommonHelper.searchDebtorByInputText(constraint.toString().trim(), mContext);
        changeCursor(c);
        return c;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.debtor_item, viewGroup, false);

        holder = new ViewHolder();
        holder.fullName = (TextView) view.findViewById(R.id.debtor_name_list_txt);

        holder.im1 = (ImageView) view.findViewById(R.id.debtor_photo_list_view);
        view.setTag(holder);
        return view;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        holder = (ViewHolder) view.getTag();
        holder.fullName.setText(cursor.getString(cursor.getColumnIndex(DbCommonHelper.DEBTOR_NAME)));
        holder.fullName.append(" ");
        holder.fullName.append(cursor.getString(cursor.getColumnIndex(DbCommonHelper.DEBTOR_SECONDNAME)));

        final String uri = cursor.getString(cursor.getColumnIndex(DbCommonHelper.DEBTOR_PHOTO));
        if (!TextUtils.isEmpty(uri)) {
            mImageLoader.displayImage(uri, holder.im1, mOptions, animateFirstListener);
            holder.im1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogFragment photoDialogFragment = PhotoDialogFragment.newInstance(uri);
                    FragmentTransaction ft = fm.beginTransaction();
                    photoDialogFragment.show(ft, "dlg1");
                }
            });
        } else {
            //holder.im1.setImageBitmap(null);//clear imageView !!!
            holder.im1.setImageBitmap(ImageUtils.roundCornerImage(BitmapFactory.decodeResource(context.getResources(), R.drawable.unknown_person), MainActivity.IMAGE_CURVATURE));
            holder.im1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }
    }

    private static class ViewHolder {
        public TextView fullName, email;
        private ImageView im1;
    }

    private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }

}
