package com.example.adapters;


import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


/**
 *
 * Adapter for creating menu of social networks for Share Button
 *
 * @author vineet.aggarwal@3pillarglobal.com
 *
 */

public class ShareButtonAdapter extends BaseAdapter {
    private final Context ctx;
    String[] data;
    int[] imagesdata;

    public ShareButtonAdapter(Context context, String[] objects, int[] images) {
        // Cache the LayoutInflate to avoid asking for a new one each time.
        ctx = context;
        data = objects;
        imagesdata = images;
    }

    /**
     * The number of items in the list is determined by the number of speeches
     * in our array.
     */
    @Override
    public int getCount() {
        return data.length;
    }

    /**
     * Since the data comes from an array, just returning the index is sufficent
     * to get at the data. If we were using a more complex data structure, we
     * would return whatever object represents one row in the list.
     */
    @Override
    public Object getItem(int position) {
        return position;
    }

    /**
     * Use the array index as a unique id.
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Make a view to hold each row.
     *
     * @see android.widget.ListAdapter#getView(int, android.view.View,
     *      android.view.ViewGroup)
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView text;

        if (convertView == null) {
            text = new TextView(ctx);
        } else {
            text = (TextView) convertView;
        }

        text.setText(data[position]);
        text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        final Drawable image;
        image = ctx.getResources().getDrawable(imagesdata[position]);
        image.setBounds(0, 0, 50, 50);

        text.setCompoundDrawables(image, null, null, null);
        text.setPadding(14, 7, 7, 7);
        text.setTextColor(Color.BLACK);
        text.setCompoundDrawablePadding(10);
        text.setGravity(Gravity.CENTER_VERTICAL);

        return text;
    }
}
