package com.example.interfaces;

/**
 * Created by dev1 on 3/7/14.
 */
public interface OnCapturePhotoListener {
    void capturePhoto();
}
