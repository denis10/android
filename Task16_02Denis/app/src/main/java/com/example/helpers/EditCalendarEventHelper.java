package com.example.helpers;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;

/**
 * Created by dev1 on 3/12/14.
 */
public class EditCalendarEventHelper {

    private static void editCalendarEvent(long calendarEventID, String uri, Context context) {

        Intent intent = new Intent(Intent.ACTION_EDIT);
        intent.setData(Uri.parse(uri + String.valueOf(calendarEventID)));
        //Android 2.1 and below.
        //intent.setData(Uri.parse("content://calendar/events/" + String.valueOf(calendarEventID)));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_NO_HISTORY
                | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        context.startActivity(intent);
    }

    public static void getNewEventId(String uri, Activity obj) {
        ContentResolver cr = obj.getContentResolver();
        Uri local_uri = Uri.parse(uri);
        Cursor cursor = cr.query(local_uri, new String[]{"MAX(_id) as max_id"}, null, null, "_id");
        if (cursor != null) {
            cursor.moveToFirst();
            long maxVal = cursor.getLong(cursor.getColumnIndex("max_id"));
            editCalendarEvent(maxVal, uri, obj);
            cursor.close();
        }
    }

    public void showCalendarPopup(Context context) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        // Android 2.2+
        i.setData(Uri.parse("content://com.android.calendar/time"));
        // Before Android 2.2+
        //i.setData(Uri.parse("content://calendar/time"));
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }

}

