package com.example.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.fragments.DebtListFragment;
import com.example.objects.Debt;
import com.example.objects.Debtor;
import com.example.objects.User;
import com.example.task16_02denis.app.LoginActivity;
import com.example.utils.ObjectsUtils;

import java.util.ArrayList;

/**
 * Created by dev1 on 2/4/14.
 */
public class DbCommonHelper extends SQLiteOpenHelper {
    public static final int DB_VERSION = 1;
    public static final String DB_NAME = "my16_01.db";
    public static final String DEBT_ID = "debt_id";//not for db
    public static final String DEBTOR_ID = "debtor_id";//not for db
    public static final String USER_ID = "user_id";//not for db

    public static final String TABLE_USERS = "users";
    public static final String USER_LOGIN = "user_login";
    public static final String USER_PASSWORD = "user_password";
    public static final String USER_UNIQUE_ID = "user_unique_id";

    private static final String CREATE_TABLE_USERS = "CREATE TABLE " + TABLE_USERS + " (_id integer primary key autoincrement, "
            + USER_LOGIN + " TEXT, "
            + USER_PASSWORD + " TEXT, "
            + USER_UNIQUE_ID + " TEXT"
            + ");";

    public static final String TABLE_DEBTORS = "debtors";
    public static final String DEBTOR_NAME = "debtor_name";
    public static final String DEBTOR_SECONDNAME = "debtor_secondname";
    public static final String DEBTOR_PHONE = "phone";
    public static final String DEBTOR_EMAIL = "email";
    public static final String DEBTOR_PHOTO = "photo";
    public static final String DEBTOR_UNIQUE_ID = "debtor_unique_id";
    private static final String CREATE_TABLE_DEBTORS = "CREATE TABLE " + TABLE_DEBTORS + " (_id integer primary key autoincrement, "
            + DEBTOR_NAME + " TEXT, "
            + DEBTOR_SECONDNAME + " TEXT, "
            + DEBTOR_PHONE + " TEXT, "
            + DEBTOR_EMAIL + " TEXT, "
            + DEBTOR_PHOTO + " TEXT, "
            + DEBTOR_UNIQUE_ID + " TEXT, "
            + USER_UNIQUE_ID + " TEXT, "
            + "FOREIGN KEY (" + USER_UNIQUE_ID + ") REFERENCES " + TABLE_USERS + " (" + USER_UNIQUE_ID + ") ON DELETE CASCADE"
            + ");";

    public static final String TABLE_DEBTS = "debts";
    public static final String DEBT_UNIQUE_ID = "debt_unique_id";
    public static final String DEBT_START = "debt_start";
    public static final String DEBT_END = "debt_end";
    public static final String DEBT_TOTAL = "debt_total";
    public static final String DEBT_PHOTO_PATH = "debt_photo_path";
    public static final String DEBT_COUNT = "debt_count";
    public static final String DEBT_NAME = "debt_name";

    private static final String CREATE_TABLE_DEBTS = "CREATE TABLE " + TABLE_DEBTS + " (_id integer primary key autoincrement, "
            + DEBT_UNIQUE_ID + " TEXT, "
            + DEBT_START + " DATE, "
            + DEBT_END + " DATE, "
            + DEBT_TOTAL + " REAL, "
            + DEBT_PHOTO_PATH + " TEXT, "
            + DEBT_COUNT + " INTEGER, "
            + DEBT_NAME + " TEXT, "
            + USER_UNIQUE_ID + " TEXT, "
            + DEBTOR_UNIQUE_ID + " TEXT, "
            + "FOREIGN KEY (" + DEBTOR_UNIQUE_ID + ") REFERENCES " + TABLE_DEBTORS + " (" + DEBTOR_UNIQUE_ID + ") ON DELETE CASCADE"
            + ");";

    public DbCommonHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public static ContentValues debtorToCv(Debtor d) {
        ContentValues cv = new ContentValues();
        cv.put(DbCommonHelper.DEBTOR_NAME, d.getName());
        cv.put(DbCommonHelper.DEBTOR_SECONDNAME, d.getSecondName());
        cv.put(DbCommonHelper.DEBTOR_PHONE, d.getPhone());
        cv.put(DbCommonHelper.DEBTOR_EMAIL, d.getEmail());
        cv.put(DbCommonHelper.DEBTOR_PHOTO, d.getPhoto());
        cv.put(DbCommonHelper.DEBTOR_UNIQUE_ID, d.getDebtorUniqueId());
        cv.put(DbCommonHelper.USER_UNIQUE_ID, d.getUserUniqueId());
        return cv;
    }

    public static ContentValues debtToCv(Debt d) {
        ContentValues cv = new ContentValues();
        cv.put(DbCommonHelper.DEBT_UNIQUE_ID, d.getDebtUniqueId());
        cv.put(DbCommonHelper.DEBTOR_UNIQUE_ID, d.getDebtorUniqueId());
        cv.put(DbCommonHelper.USER_UNIQUE_ID, d.getUserUniqueID());
        cv.put(DbCommonHelper.DEBT_START, d.getDebtStart());
        cv.put(DbCommonHelper.DEBT_END, d.getDebtEnd());
        cv.put(DbCommonHelper.DEBT_TOTAL, d.getDebtTotal());
        cv.put(DbCommonHelper.DEBT_PHOTO_PATH, d.getDebtPhotoPath());
        cv.put(DbCommonHelper.DEBT_COUNT, d.getDebtCount());
        cv.put(DbCommonHelper.DEBT_NAME, d.getDebtName());
        return cv;
    }

    public static ContentValues userToCv(User u) {
        ContentValues cv = new ContentValues();
        cv.put(DbCommonHelper.USER_LOGIN, u.getLogin());
        cv.put(DbCommonHelper.USER_PASSWORD, u.getPassword());
        cv.put(DbCommonHelper.USER_UNIQUE_ID, u.getUserUniqueId());
        return cv;
    }

    public static Cursor searchDebtorByInputText(String inputText, Context context) {
        DbCommonHelper mDBHelper = new DbCommonHelper(context);
        SQLiteDatabase db = mDBHelper.getReadableDatabase();

        String[] ss = inputText.split(" ");
        String query;
        Cursor mCursor = null;
        switch (ss.length) {
            case 1:
                query = "SELECT *" +
                        " FROM " + TABLE_DEBTORS +
                        " WHERE (" + DEBTOR_NAME + " LIKE ?" +
                        " OR " + DEBTOR_SECONDNAME + " LIKE ?)" +
                        " AND " + DbCommonHelper.USER_UNIQUE_ID +
                        "=?";
                mCursor = db.rawQuery(query, new String[]{"%" + inputText + "%",
                        "%" + inputText + "%",
                        LoginActivity.getUserUniqueId()});
                break;
            case 2:
                query = "SELECT *" +
                        " FROM " + TABLE_DEBTORS +
                        " WHERE (" + DEBTOR_NAME + " LIKE ?" +
                        " AND " + DEBTOR_SECONDNAME + " LIKE ?" +
                        " OR " + DEBTOR_NAME + " LIKE ?" +
                        " AND " + DEBTOR_SECONDNAME + " LIKE ?)" +
                        " AND " + DbCommonHelper.USER_UNIQUE_ID +
                        "=?";
                mCursor = db.rawQuery(query, new String[]{"%" + ss[0] + "%", "%" + ss[1] + "%",
                        "%" + ss[1] + "%", "%" + ss[0] + "%",
                        LoginActivity.getUserUniqueId()});
                break;
            default:
                break;
        }
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        if (db != null) {
            db.close();
        }
        return mCursor;
    }

    public static Cursor searchDebtByInputText(String inputText, Context context) {
        DbCommonHelper mDBHelper = new DbCommonHelper(context);
        SQLiteDatabase db = mDBHelper.getReadableDatabase();

        String[] ss = inputText.split(" ");
        String query;
        Cursor mCursor = null;
        switch (ss.length) {
            case 1:
                query = "SELECT T1._id as " + DebtListFragment.FIRST_TABLE_ID
                        + ", T1." + DbCommonHelper.DEBT_UNIQUE_ID
                        + ", T1." + DbCommonHelper.USER_UNIQUE_ID + " as " + DebtListFragment.FIRST_TABLE_USER_ID
                        + ", T1." + DbCommonHelper.DEBT_START + ", T1." + DbCommonHelper.DEBT_END
                        + ", T1." + DbCommonHelper.DEBT_TOTAL + ", T1." + DbCommonHelper.DEBT_PHOTO_PATH
                        + ", T1." + DbCommonHelper.DEBT_COUNT
                        + ", T1." + DbCommonHelper.DEBT_NAME
                        + ", T1." + DbCommonHelper.DEBTOR_UNIQUE_ID + " as " + DebtListFragment.FIRST_TABLE_UNIQUE_DEBTOR_ID
                        + ", T2.* FROM " + DbCommonHelper.TABLE_DEBTS + " T1 "
                        + " INNER JOIN " + DbCommonHelper.TABLE_DEBTORS + " T2 ON "
                        + "T1." + DbCommonHelper.DEBTOR_UNIQUE_ID + "=" + "T2." + DbCommonHelper.DEBTOR_UNIQUE_ID+
                        " WHERE (" + DEBT_NAME + " LIKE ?" +
                        " OR " + DEBT_COUNT + " LIKE ?" +
                        " OR " + DEBT_TOTAL + " LIKE ?)" +
                        " AND " + DebtListFragment.FIRST_TABLE_USER_ID +
                        "=?"
                ;
                mCursor = db.rawQuery(query, new String[]{"%" + inputText + "%",
                        "%" + inputText + "%", "%" + inputText + "%",
                        LoginActivity.getUserUniqueId()});
                break;
            case 2:
                query = "SELECT T1._id as " + DebtListFragment.FIRST_TABLE_ID
                        + ", T1." + DbCommonHelper.DEBT_UNIQUE_ID
                        + ", T1." + DbCommonHelper.USER_UNIQUE_ID + " as " + DebtListFragment.FIRST_TABLE_USER_ID
                        + ", T1." + DbCommonHelper.DEBT_START + ", T1." + DbCommonHelper.DEBT_END
                        + ", T1." + DbCommonHelper.DEBT_TOTAL + ", T1." + DbCommonHelper.DEBT_PHOTO_PATH
                        + ", T1." + DbCommonHelper.DEBT_COUNT
                        + ", T1." + DbCommonHelper.DEBT_NAME
                        + ", T1." + DbCommonHelper.DEBTOR_UNIQUE_ID + " as " + DebtListFragment.FIRST_TABLE_UNIQUE_DEBTOR_ID
                        + ", T2.* FROM " + DbCommonHelper.TABLE_DEBTS + " T1 "
                        + " INNER JOIN " + DbCommonHelper.TABLE_DEBTORS + " T2 ON "
                        + "T1." + DbCommonHelper.DEBTOR_UNIQUE_ID + "=" + "T2." + DbCommonHelper.DEBTOR_UNIQUE_ID+
                        " WHERE (" + DEBT_NAME + " LIKE ?" +
                        " AND " + DEBT_COUNT + " LIKE ?" +
                        " OR " + DEBT_NAME + " LIKE ?" +
                        " AND " + DEBT_COUNT + " LIKE ?" +
                        " OR " + DEBT_TOTAL + " LIKE ?)" +
                        " AND " + DbCommonHelper.USER_UNIQUE_ID +
                        "=?";
                mCursor = db.rawQuery(query, new String[]{"%" + ss[0] + "%", "%" + ss[1] + "%",
                        "%" + ss[1] + "%", "%" + ss[0] + "%", "%" + ss[0] + "%",
                        LoginActivity.getUserUniqueId()});
                break;
            default:
                break;
        }

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        if (db != null) {
            db.close();
        }
        return mCursor;
    }

    public static long insertDebtor(Debtor debtor, Context context) {
        DbCommonHelper mDBHelper = new DbCommonHelper(context);
        long id;
        synchronized (DbCommonHelper.class) {
            SQLiteDatabase db = mDBHelper.getWritableDatabase();
            id = db.insert(DbCommonHelper.TABLE_DEBTORS, null, DbCommonHelper.debtorToCv(debtor));
            db.close();
        }

        return id;
    }

    public static void updateDebtor(Debtor debtor, Context context) {
        DbCommonHelper mDBHelper = new DbCommonHelper(context);

        synchronized (DbCommonHelper.class) {
            SQLiteDatabase db = mDBHelper.getWritableDatabase();
            db.update(DbCommonHelper.TABLE_DEBTORS, DbCommonHelper.debtorToCv(debtor), DbCommonHelper.DEBTOR_UNIQUE_ID + "=?", new String[]{debtor.getDebtorUniqueId()});
            db.close();
        }
    }

    public static void deleteDebtor(String uniqueId, Context context) {
        DbCommonHelper mDBHelper = new DbCommonHelper(context);

        synchronized (DbCommonHelper.class) {
            SQLiteDatabase db = mDBHelper.getWritableDatabase();
            db.delete(DbCommonHelper.TABLE_DEBTORS, DbCommonHelper.DEBTOR_UNIQUE_ID + "=?", new String[]{uniqueId});
            db.close();
        }
    }

    public static long insertDebt(Debt debt, Context context) {
        DbCommonHelper mDBHelper = new DbCommonHelper(context);
        long id;
        synchronized (DbCommonHelper.class) {
            SQLiteDatabase db = mDBHelper.getWritableDatabase();
            id = db.insert(DbCommonHelper.TABLE_DEBTS, null, DbCommonHelper.debtToCv(debt));
            db.close();
        }
        return id;
    }

    public static void updateDebt(Debt debt, Context context) {
        DbCommonHelper mDBHelper = new DbCommonHelper(context);

        synchronized (DbCommonHelper.class) {
            SQLiteDatabase db = mDBHelper.getWritableDatabase();
            db.update(DbCommonHelper.TABLE_DEBTS, DbCommonHelper.debtToCv(debt), DbCommonHelper.DEBT_UNIQUE_ID + "=?", new String[]{debt.getDebtUniqueId()});
            db.close();
        }
    }

    public static void deleteDebt(String uniqueId, Context context) {
        DbCommonHelper mDBHelper = new DbCommonHelper(context);
        synchronized (DbCommonHelper.class) {
            SQLiteDatabase db = mDBHelper.getWritableDatabase();
            db.delete(DbCommonHelper.TABLE_DEBTS, DbCommonHelper.DEBT_UNIQUE_ID + "=?", new String[]{uniqueId});
            db.close();
        }
    }

    public static long insertUser(User user, Context context) {
        DbCommonHelper mDBHelper = new DbCommonHelper(context);
        long id;
        synchronized (DbCommonHelper.class) {
            SQLiteDatabase db = mDBHelper.getWritableDatabase();
            id = db.insert(DbCommonHelper.TABLE_USERS, null, DbCommonHelper.userToCv(user));
            db.close();
        }
        return id;
    }

    public static User getUserObject(String login, Context context) {
        User user = null;

        DbCommonHelper mDBHelper = new DbCommonHelper(context);

        synchronized (DbCommonHelper.class) {
            SQLiteDatabase db = mDBHelper.getReadableDatabase();
            String query = "SELECT * FROM " + DbCommonHelper.TABLE_USERS +
                    " WHERE " + DbCommonHelper.USER_LOGIN + "=?" +
                    ";";

            Cursor c = db.rawQuery(query, new String[]{login});
            if (c != null) {
                if (c.moveToFirst()) {
                    user = ObjectsUtils.createUserFromCursor(c);
                } else {
                    Log.d("", "0 rows");
                }
                c.close();
            }
            db.close();

        }
        return user;
    }

    public static ArrayList<User> getUsersFromDB(Context context) {
        ArrayList<User> dataUsers = new ArrayList<User>();

        DbCommonHelper mDBHelper = new DbCommonHelper(context);

        synchronized (DbCommonHelper.class) {
            SQLiteDatabase db = mDBHelper.getReadableDatabase();
            String query = "SELECT * FROM " + DbCommonHelper.TABLE_USERS
                    + ";";

            Cursor c = db.rawQuery(query, null);
            if (c != null) {
                if (c.moveToFirst()) {
                    int idColIndex = c.getColumnIndex("_id");
                    int loginIndex = c.getColumnIndex(USER_LOGIN);
                    int passwordIndex = c.getColumnIndex(USER_PASSWORD);
                    int uniqueIdIndex = c.getColumnIndex(USER_UNIQUE_ID);
                    do {
                        dataUsers.add(new User(c.getLong(idColIndex), c.getString(loginIndex), c.getString(passwordIndex), c.getString(uniqueIdIndex)));
                    } while (c.moveToNext());

                } else {
                    Log.d("log", "0 rows");
                }
                c.close();
            }

            db.close();
        }
        return dataUsers;
    }

    public static ArrayList<Debt> getDebtsFromDB(Context context) {
        ArrayList<Debt> dataDebts = new ArrayList<Debt>();

        DbCommonHelper mDBHelper = new DbCommonHelper(context);

        synchronized (DbCommonHelper.class) {
            SQLiteDatabase db = mDBHelper.getReadableDatabase();
            String query = "SELECT * FROM " + DbCommonHelper.TABLE_DEBTS
                    + " WHERE " + DbCommonHelper.USER_UNIQUE_ID + "=?"
                    + ";";

            Cursor c = db.rawQuery(query, new String[]{"" + LoginActivity.getUserUniqueId()});
            if (c != null) {
                if (c.moveToFirst()) {
                    int idColIndex = c.getColumnIndex("_id");
                    int debtUniqueId = c.getColumnIndex(DbCommonHelper.DEBT_UNIQUE_ID);
                    int debtorUniqueId = c.getColumnIndex(DbCommonHelper.DEBTOR_UNIQUE_ID);
                    int userUniqueId = c.getColumnIndex(DbCommonHelper.USER_UNIQUE_ID);
                    int debtStart = c.getColumnIndex(DbCommonHelper.DEBT_START);
                    int debtEnd = c.getColumnIndex(DbCommonHelper.DEBT_END);
                    int debtTotal = c.getColumnIndex(DbCommonHelper.DEBT_TOTAL);
                    int debtPhotoPath = c.getColumnIndex(DbCommonHelper.DEBT_PHOTO_PATH);
                    int debtCount = c.getColumnIndex(DbCommonHelper.DEBT_COUNT);
                    int debtName = c.getColumnIndex(DbCommonHelper.DEBT_NAME);
                    do {
                        dataDebts.add(new Debt(c.getLong(idColIndex), c.getString(debtUniqueId), c.getString(debtorUniqueId),
                                c.getString(userUniqueId), c.getString(debtStart), c.getString(debtEnd),
                                c.getFloat(debtTotal), c.getString(debtPhotoPath), c.getInt(debtCount), c.getString(debtName)));
                    } while (c.moveToNext());

                } else {
                    Log.d("log", "0 rows");
                }
                c.close();
            }

            db.close();
        }
        return dataDebts;
    }

    public static ArrayList<Debtor> getDebtorsFromDB(Context context) {
        ArrayList<Debtor> dataDebts = new ArrayList<Debtor>();

        DbCommonHelper mDBHelper = new DbCommonHelper(context);

        synchronized (DbCommonHelper.class) {
            SQLiteDatabase db = mDBHelper.getReadableDatabase();
            String query = "SELECT * FROM " + DbCommonHelper.TABLE_DEBTORS
                    + " WHERE " + DbCommonHelper.USER_UNIQUE_ID + "=?"
                    + ";";

            Cursor c = db.rawQuery(query, new String[]{"" + LoginActivity.getUserUniqueId()});
            if (c != null) {
                if (c.moveToFirst()) {
                    int debtorId = c.getColumnIndex("_id");
                    int debtorName = c.getColumnIndex(DbCommonHelper.DEBTOR_NAME);
                    int debtorSecondName = c.getColumnIndex(DbCommonHelper.DEBTOR_SECONDNAME);
                    int debtorPhone = c.getColumnIndex(DbCommonHelper.DEBTOR_PHONE);
                    int debtorEmail = c.getColumnIndex(DbCommonHelper.DEBTOR_EMAIL);
                    int debtorPhoto = c.getColumnIndex(DbCommonHelper.DEBTOR_PHOTO);
                    int debtorUniqueId = c.getColumnIndex(DbCommonHelper.DEBTOR_UNIQUE_ID);
                    int userUniqueId = c.getColumnIndex(DbCommonHelper.USER_UNIQUE_ID);
                    do {
                        dataDebts.add(new Debtor(c.getLong(debtorId), c.getString(debtorName), c.getString(debtorSecondName),
                                c.getString(debtorPhone), c.getString(debtorEmail), c.getString(debtorPhoto),
                                c.getString(debtorUniqueId), c.getString(userUniqueId)));
                    } while (c.moveToNext());

                } else {
                    Log.d("log", "0 rows");
                }
                c.close();
            }

            db.close();
        }
        return dataDebts;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_USERS);
        sqLiteDatabase.execSQL(CREATE_TABLE_DEBTORS);
        sqLiteDatabase.execSQL(CREATE_TABLE_DEBTS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

    }
}