package com.example.helpers;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import java.util.Calendar;

/**
 * Created by dev1 on 3/7/14.
 */
public class CalendarHelper {

    public CalendarHelper() {

    }

    public void addReminder(Activity activity, String title, String description, long startTime, long endTime) {
        // get calendar
        Calendar cal = Calendar.getInstance();
        Uri EVENTS_URI = Uri.parse(getCalendarUriBase(activity) + "events");
        ContentResolver cr = activity.getContentResolver();

        // event insert
        ContentValues values = new ContentValues();
        values.put("calendar_id", 1);
        values.put("title", title);
        values.put("allDay", 0);
        values.put("dtstart", startTime);
        values.put("dtend", endTime);
        values.put("description", description);
        values.put("visibility", 0);
        values.put("hasAlarm", 1);
        Uri event = cr.insert(EVENTS_URI, values);

        // reminder insert
        Uri REMINDERS_URI = Uri.parse(getCalendarUriBase(activity) + "reminders");
        values = new ContentValues();
        values.put("event_id", Long.parseLong(event.getLastPathSegment()));
        values.put("method", 1);
        values.put("minutes", 10);
        cr.insert(REMINDERS_URI, values);
    }

    private String getCalendarUriBase(Activity act) {

        String calendarUriBase = null;
        Uri calendars = Uri.parse("content://calendar/calendars");
        Cursor managedCursor = null;
        try {
            managedCursor = act.managedQuery(calendars, null, null, null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (managedCursor != null) {
            calendarUriBase = "content://calendar/";
            managedCursor.close();
        } else {
            calendars = Uri.parse("content://com.android.calendar/calendars");
            try {
                managedCursor = act.managedQuery(calendars, null, null, null, null);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (managedCursor != null) {
                calendarUriBase = "content://com.android.calendar/";
            }
        }
        return calendarUriBase;
    }
}
