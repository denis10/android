package com.example.parsing;

import com.example.objects.Debt;
import com.example.utils.SynchronizeUtils;
import com.google.gson.annotations.SerializedName;

/**
 * Created by dev1 on 3/3/14.
 */
public class DebtResult extends Debt{

    @SerializedName(SynchronizeUtils.DEBT_OBJECT_ID)
    private String debtObjectId;

    @SerializedName(SynchronizeUtils.URL)
    private String url;

    public String getDebtObjectId() {
        return debtObjectId;
    }

    public void setDebtObjectId(String debtObjectId) {
        this.debtObjectId = debtObjectId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
