package com.example.parsing;

import com.example.objects.Debtor;
import com.example.utils.SynchronizeUtils;
import com.google.gson.annotations.SerializedName;

/**
 * Created by dev1 on 3/3/14.
 */
public class DebtorResult extends Debtor{

    @SerializedName(SynchronizeUtils.DEBTOR_OBJECT_ID)
    public String debtorObjectId;

    @SerializedName(SynchronizeUtils.URL)
    public String url;
}
