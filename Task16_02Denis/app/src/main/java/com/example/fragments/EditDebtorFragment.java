package com.example.fragments;

import android.app.Activity;
import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.Photo;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.Contacts.Data;
import android.provider.ContactsContract.RawContacts;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dialogs.ChoosePhotoDialog;
import com.example.dialogs.SaveContactDialog;
import com.example.helpers.DbCommonHelper;
import com.example.interfaces.OnCapturePhotoListener;
import com.example.interfaces.OnSaveContactListener;
import com.example.interfaces.OnSdImageListener;
import com.example.objects.Debtor;
import com.example.services.ChangeObjectIntentService;
import com.example.task16_02denis.app.LoginActivity;
import com.example.task16_02denis.app.MainActivity;
import com.example.task16_02denis.app.R;
import com.example.utils.FileUtils;
import com.example.utils.ImageUtils;
import com.example.utils.SynchronizeUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by dev1 on 2/19/14.
 */
public class EditDebtorFragment extends Fragment implements OnSdImageListener, OnCapturePhotoListener, OnSaveContactListener {
    public static boolean dialog;
    private static final int REQUEST_TAKE_PHOTO = 3;
    private final int IMAGE_CODE = 2;
    private final int PICK_CONTACT_REQUEST = 1;
    private boolean mEdit;
    private Debtor mDebtor;
    private String mUri;
    private String mUriTemp;
    private TextView mDebtorName;
    private TextView mDebtorSecondName;
    private TextView mDebtorPhone;
    private TextView mDebtorEmail;
    private ImageView mImageView;
    private boolean mGetFromPhoneBook;
    private String mDebtorUniqueId;
    private Button mButton1;
    private Button mButton2;
    private ChoosePhotoDialog mChoosePhotoDialog;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.edit_debtor_fragment,
                container, false);
        ActionBar actionBar = ((ActionBarActivity) getActivity()).getSupportActionBar();

        mDebtorName = (TextView) view.findViewById(R.id.edt_debtor_name);
        mDebtorSecondName = (TextView) view.findViewById(R.id.edt_debtor_secondname);
        mDebtorPhone = (TextView) view.findViewById(R.id.edt_debtor_phone);
        mDebtorEmail = (TextView) view.findViewById(R.id.edt_debtor_email);
        mImageView = (ImageView) view.findViewById(R.id.edt_debtor_photo);
        mButton1 = (Button) view.findViewById(R.id.ok_btn);
        mButton2 = (Button) view.findViewById(R.id.get_debtor_btn);
        //
        mButton1.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mUri == null) {
                    mUri = "";//not null!!!
                }
                mDebtor = new Debtor(mDebtorName.getText().toString().trim(),
                        mDebtorSecondName.getText().toString().trim(),
                        mDebtorPhone.getText().toString().trim(),
                        mDebtorEmail.getText().toString().trim(),
                        mUri, "", LoginActivity.getUserUniqueId()
                );
                if (!checkEditText(mDebtor)) {

                } else {
                    if (mEdit) {
                        mDebtor.setDebtorUniqueId(mDebtorUniqueId);//important!
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                DbCommonHelper.updateDebtor(mDebtor, getActivity());
                            }
                        }).start();

                        if (SynchronizeUtils.isNetworkConnected(getActivity())) {
                            Intent intent = new Intent(getActivity(), ChangeObjectIntentService.class);
                            intent.putExtra(SynchronizeUtils.ACTION_TYPE, SynchronizeUtils.OBJECT_UPDATE);
                            intent.putExtra(SynchronizeUtils.OBJECT_TYPE, SynchronizeUtils.DEBTOR);
                            intent.putExtra(SynchronizeUtils.DEBTOR_OBJECT_ID, mDebtor);

                            getActivity().startService(intent);
                        } else {
                            Toast.makeText(getActivity(), R.string.not_synchronized, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        mDebtor.setDebtorUniqueId(UUID.randomUUID().toString());//important!
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                DbCommonHelper.insertDebtor(mDebtor, getActivity());
                            }
                        }).start();

                        if (SynchronizeUtils.isNetworkConnected(getActivity())) {
                            Intent intent = new Intent(getActivity(), ChangeObjectIntentService.class);
                            intent.putExtra(SynchronizeUtils.ACTION_TYPE, SynchronizeUtils.OBJECT_UPLOAD);
                            intent.putExtra(SynchronizeUtils.OBJECT_TYPE, SynchronizeUtils.DEBTOR);
                            intent.putExtra(SynchronizeUtils.DEBTOR_OBJECT_ID, mDebtor);
                            getActivity().startService(intent);
                        } else {
                            Toast.makeText(getActivity(), R.string.not_synchronized, Toast.LENGTH_SHORT).show();
                        }
                    }
                    //
                    if (!mGetFromPhoneBook) {
                        SaveContactDialog mSaveContactDialog = SaveContactDialog.newInstance();
                        mSaveContactDialog.mContactListener = EditDebtorFragment.this;
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        mSaveContactDialog.show(ft, "dlg12");
                    } else {
                        Intent intent = new Intent();
                        getActivity().setResult(Activity.RESULT_OK, intent);
                        getActivity().finish();
                    }

                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                            Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(mDebtorName.getWindowToken(), 0);
                }
            }
        });

        mButton2.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickContact();
            }
        });
        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog = true;
                mChoosePhotoDialog = ChoosePhotoDialog.newInstance();
                mChoosePhotoDialog.mSdListener = EditDebtorFragment.this;
                mChoosePhotoDialog.mCaptureListener = EditDebtorFragment.this;
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                mChoosePhotoDialog.show(ft, "dlg6");
            }
        });
        //
        if (savedInstanceState != null) {
            mUri = savedInstanceState.getString("mUri");
            mGetFromPhoneBook = savedInstanceState.getBoolean("mGetFromPhoneBook");
            setDebtorImageView();
            mDebtorUniqueId = savedInstanceState.getString(DbCommonHelper.DEBTOR_UNIQUE_ID);
        }


        Bundle args = getArguments();
        mEdit = args.getBoolean(MainActivity.EDIT);

        if (mEdit) {
            actionBar.setTitle(R.string.edit_debtor);
            mDebtor = (Debtor) args.getSerializable(DbCommonHelper.DEBTOR_UNIQUE_ID);
            mDebtorName.setText(mDebtor.getName());
            mDebtorSecondName.setText(mDebtor.getSecondName());
            mDebtorPhone.setText(mDebtor.getPhone());
            mDebtorEmail.setText(mDebtor.getEmail());
            mUri = mDebtor.getPhoto();
            mDebtorUniqueId = mDebtor.getDebtorUniqueId();
            setDebtorImageView();
        }else {
            actionBar.setTitle(R.string.add_debtor);
        }

        if (dialog) {
            mChoosePhotoDialog = ChoosePhotoDialog.newInstance();
            mChoosePhotoDialog.mSdListener = EditDebtorFragment.this;
            mChoosePhotoDialog.mCaptureListener = EditDebtorFragment.this;
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            mChoosePhotoDialog.show(ft, "dlg6");
        }
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mChoosePhotoDialog!=null){
            mChoosePhotoDialog.dismiss();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("mUri", mUri);
        outState.putBoolean("mGetFromPhoneBook", mGetFromPhoneBook);
        outState.putString(DbCommonHelper.DEBTOR_UNIQUE_ID, mDebtorUniqueId);
    }

    private boolean checkEditText(Debtor debtor) {
        if (TextUtils.isEmpty(debtor.getName())) {
            Toast.makeText(getActivity(), R.string.put_correct_name, Toast.LENGTH_LONG).show();
            return false;
        }
        if (TextUtils.isEmpty(debtor.getSecondName())) {
            Toast.makeText(getActivity(), R.string.put_correct_secondname, Toast.LENGTH_LONG).show();
            return false;
        }
        if (!TextUtils.isEmpty(debtor.getPhone()) && !PhoneNumberUtils.isGlobalPhoneNumber(debtor.getPhone())) {
            Toast.makeText(getActivity(), R.string.put_correct_phonenumber, Toast.LENGTH_LONG).show();
            return false;
        }
        if (!TextUtils.isEmpty(debtor.getEmail()) && !android.util.Patterns.EMAIL_ADDRESS.matcher(debtor.getEmail()).matches()) {
            Toast.makeText(getActivity(), R.string.put_correct_email, Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    public void showSdImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, IMAGE_CODE);
    }

    public void capturePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = FileUtils.createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (photoFile != null) {
                mUri = Uri.fromFile(photoFile).toString();
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(intent, REQUEST_TAKE_PHOTO);
            }
        } else {
            Toast.makeText(getActivity(), R.string.no_app, Toast.LENGTH_SHORT).show();
        }
    }

    private void pickContact() {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT_REQUEST);
        mGetFromPhoneBook = true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // If the request went well (OK) and the request was PICK_CONTACT_REQUEST
        if (resultCode == Activity.RESULT_OK /*&& data != null*/) {
            switch (requestCode) {
                case REQUEST_TAKE_PHOTO:
                        setDebtorImageView();
                    break;
                case IMAGE_CODE:
                    mUri = data.getData().toString();
                        setDebtorImageView();
                    break;
                case PICK_CONTACT_REQUEST:
                    Uri contactData = data.getData();
                    String cNumber = "";
                    String name = "";
                    String secondName = "";
                    String email = "";
                    String id = "";
                    String uri = "";

                    if (contactData != null) {
                        Cursor c = getActivity().getContentResolver().query(contactData, null, null, null, null);
                        if (c != null) {
                            if (c.moveToFirst()) {
                                id = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                                String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                                if (hasPhone != null) {
                                    if (hasPhone.equalsIgnoreCase("1")) {
                                        Uri phoneUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
                                        if (phoneUri != null) {
                                            Cursor phoneCursor = getActivity().getContentResolver().query(
                                                    phoneUri, null,
                                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id,
                                                    null, null);
                                            if (phoneCursor != null) {
                                                if (phoneCursor.moveToFirst()) {
                                                    cNumber = phoneCursor.getString(phoneCursor.getColumnIndex("data1"));
                                                }
                                                phoneCursor.close();
                                            }
                                        }
                                    }
                                }
                                Uri emailUri = ContactsContract.CommonDataKinds.Email.CONTENT_URI;
                                if (emailUri != null) {
                                    Cursor emailCursor = getActivity().getContentResolver().query(
                                            emailUri, null,
                                            ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + id,
                                            null, null);
                                    if (emailCursor != null) {
                                        if (emailCursor.moveToFirst()) {
                                            email = emailCursor.getString(emailCursor.getColumnIndex("data1"));
                                        }
                                        emailCursor.close();
                                    }
                                }
                                Uri dataUri = ContactsContract.Data.CONTENT_URI;
                                if (dataUri != null) {
                                    String[] projection = new String[]
                                            {ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME, ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME};
                                    String where = ContactsContract.Data.CONTACT_ID + " = ?AND " + ContactsContract.CommonDataKinds.StructuredName.MIMETYPE + "= ?";
                                    String[] whereParameters = new String[]
                                            {id, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE};
                                    Cursor dataCursor =
                                            getActivity().getContentResolver().query(dataUri, projection, where,
                                                    whereParameters, null);
                                    if (dataCursor.moveToFirst()) {
                                        name = dataCursor.getString(dataCursor.getColumnIndex(StructuredName.GIVEN_NAME));
                                        secondName = dataCursor.getString(dataCursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME));
                                    }
                                }
                            }
                            c.close();
                        }
                    }

                    int res = 0;
                    try {
                        res = Integer.parseInt(id);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                        Log.e("", "not parse id");
                    }

                    if (FileUtils.getPhotoUriFromId(res, getActivity()) != null) {
                        uri = FileUtils.getPhotoUriFromId(res,getActivity()).toString();
                        mUri = uri;
                        if (mUri != null) {
                            setDebtorImageView();
                        }
                    }

                    mDebtorName.setText(name);
                    mDebtorSecondName.setText(secondName);
                    mDebtorPhone.setText(cNumber);
                    mDebtorEmail.setText(email);

                    break;

                default:
                    break;
            }
        }
    }

    private void setDebtorImageView() {
        if (!TextUtils.isEmpty(mUri)) {
            Bitmap mBitmap = ImageUtils.getBitmap(getActivity(), mUri);
            if (mBitmap!=null) {
                mImageView.setImageBitmap(mBitmap);
            }else{
                mUri="";
                mImageView.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.unknown_person));
            }
        } else {
            mImageView.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.unknown_person));
        }
    }

    public void addContact() {
        ArrayList<ContentProviderOperation> ops =
                new ArrayList<ContentProviderOperation>();

        int rawContactID = ops.size();

        ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                .withValue(RawContacts.ACCOUNT_TYPE, null)
                .withValue(RawContacts.ACCOUNT_NAME, null)
                .build());

        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                .withValue(Data.MIMETYPE, StructuredName.CONTENT_ITEM_TYPE)
                .withValue(StructuredName.FAMILY_NAME, mDebtorSecondName.getText().toString().trim())
                .withValue(StructuredName.GIVEN_NAME, mDebtorName.getText().toString().trim())
                .build());


        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                .withValue(ContactsContract.Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE)
                .withValue(Phone.NUMBER, mDebtorPhone.getText().toString().trim())
                .withValue(Phone.TYPE, Phone.TYPE_MOBILE)
                .build());

        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                .withValue(ContactsContract.Data.MIMETYPE, Email.CONTENT_ITEM_TYPE)
                .withValue(Email.DATA, mDebtorEmail.getText().toString().trim())
                .withValue(Email.TYPE, Email.TYPE_WORK)
                .build());
        //
        int height = 48;
        int width = 48;
        Bitmap bitmap = null;
        if (!TextUtils.isEmpty(mUri)) {
            if (mUri.contains("content://com.android.contacts/contacts/")) {
                try {
                    InputStream input = getActivity().getContentResolver().openInputStream(Uri.parse(mUri));
                    bitmap = BitmapFactory.decodeStream(input);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else {
                bitmap = ImageUtils.getImage(FileUtils.getRealPathFromURI(getActivity(), Uri.parse(mUri)), height, width);
            }
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if (bitmap != null) {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);
        }

        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(Data.RAW_CONTACT_ID, 0)
                .withValue(Data.MIMETYPE, Photo.CONTENT_ITEM_TYPE)
                .withValue(Photo.PHOTO, baos.toByteArray()).build());

        try {
            baos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //
        try {
            // Executing all the insert operations as a single database transaction
            getActivity().getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
            Toast.makeText(getActivity(), "Contact is successfully added", Toast.LENGTH_SHORT).show();
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }
    }
}
