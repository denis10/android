package com.example.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dialogs.ChoosePhotoDialog;
import com.example.dialogs.SetReminderDialog;
import com.example.helpers.DbCommonHelper;
import com.example.interfaces.OnCapturePhotoListener;
import com.example.interfaces.OnSdImageListener;
import com.example.objects.Debt;
import com.example.objects.Debtor;
import com.example.objects.Reminder;
import com.example.services.ChangeObjectIntentService;
import com.example.task16_02denis.app.LoginActivity;
import com.example.task16_02denis.app.MainActivity;
import com.example.task16_02denis.app.R;
import com.example.task16_02denis.app.ReturnActivity;
import com.example.utils.FileUtils;
import com.example.utils.ImageUtils;
import com.example.utils.SynchronizeUtils;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

/**
 * Created by dev1 on 2/25/14.
 */

public class EditDebtFragment extends Fragment implements OnSdImageListener, OnCapturePhotoListener {
    public static final int REQUEST_RETURN_ACTIVITY = 2;
    private static final int REQUEST_TAKE_PHOTO = 1;
    private static final int IMAGE_CODE = 3;
    public static boolean dialog;
    private static TextView mStartDebt;
    private static TextView mFinishDebt;
    private static boolean mStartDate;
    private final String BUTTON2 = "button2";
    private String mUri;
    private String mDebtUri;
    private String mDebtUriTemp;
    private boolean mEdit;
    private Debtor mDebtor;
    private TextView mDebtorName;
    private TextView mDebtorPhone;
    private TextView mDebtorEmail;
    private TextView mTotal;
    private TextView mCount;
    private TextView mDebtName;
    private ImageView mDebtPhoto;
    private String mDebtorUniqueId;
    private String mDebtUniqueId;
    private ArrayAdapter<CharSequence> mAdapter;
    private Spinner mSpinner;
    private float mFloatTotal = 0;
    private int mIntCount = 0;
    private long mLongFinishDebt = 0;
    private Button mButton1;
    private Button mButton2;
    // private ImageView mDebtImageView;
    private ImageView mDebtorImageView;
    private View mUpView;
    private View mBottomView;
    private boolean mButton2Pressed;
    private Debt mDebt;
    private ChoosePhotoDialog mChoosePhotoDialog;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.edit_debt_fragment,
                container, false);
        ActionBar actionBar = ((ActionBarActivity) getActivity()).getSupportActionBar();

        mDebtorName = (TextView) view.findViewById(R.id.edt_debtor_name);
        mDebtorPhone = (TextView) view.findViewById(R.id.edt_debtor_phone);
        mDebtorEmail = (TextView) view.findViewById(R.id.edt_debtor_email);
        mStartDebt = (TextView) view.findViewById(R.id.edt_debt_start);
        mFinishDebt = (TextView) view.findViewById(R.id.edt_debt_finish);
        mTotal = (TextView) view.findViewById(R.id.edt_debt_total);
        mCount = (TextView) view.findViewById(R.id.edt_debt_count);
        mDebtName = (TextView) view.findViewById(R.id.edt_debt_name);
        mDebtPhoto = (ImageView) view.findViewById(R.id.edt_debt_photo);

        mButton1 = (Button) view.findViewById(R.id.ok_btn);
        mButton2 = (Button) view.findViewById(R.id.set_debtor_btn);
        mSpinner = (Spinner) view.findViewById(R.id.type_spinner);
        mAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.type_array, R.layout.spinner_item);
        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(mAdapter);
        mDebtorImageView = (ImageView) view.findViewById(R.id.edt_debtor_photo);

        mUpView = view.findViewById(R.id.up_debt_layout);
        mBottomView = view.findViewById(R.id.bottom_debt_layout);
        //

        mButton2.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ReturnActivity.class);
                startActivityForResult(intent, REQUEST_RETURN_ACTIVITY);
                mButton2Pressed = true;
            }
        });

        mStartDebt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mStartDate = true;
                showDatePickerDialog();
            }
        });

        mFinishDebt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mStartDate = false;
                showDatePickerDialog();
            }
        });

        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                switch (position) {
                    case 0://price
                        mTotal.setVisibility(View.VISIBLE);
                        mCount.setVisibility(View.GONE);
                        mCount.setText("");
                        mDebtName.setVisibility(View.GONE);
                        mDebtName.setText("");
                        mDebtPhoto.setVisibility(View.GONE);
                        mDebtPhoto.setOnClickListener(null);
                        break;
                    case 1://object
                        mTotal.setVisibility(View.GONE);
                        mTotal.setText("");
                        mCount.setVisibility(View.VISIBLE);
                        mDebtName.setVisibility(View.VISIBLE);
                        mDebtPhoto.setVisibility(View.VISIBLE);
                        mDebtPhoto.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog = true;
                                mChoosePhotoDialog = ChoosePhotoDialog.newInstance();
                                mChoosePhotoDialog.mSdListener = EditDebtFragment.this;
                                mChoosePhotoDialog.mCaptureListener = EditDebtFragment.this;
                                FragmentTransaction ft = getFragmentManager().beginTransaction();
                                mChoosePhotoDialog.show(ft, "dlg7");
                            }
                        });
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mButton1.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!checkEditText()) {

                } else {
                    if (mDebtUri == null) {
                        mDebtUri = "";//not null!!!
                    }
                    mDebt = new Debt("", mDebtorUniqueId, LoginActivity.getUserUniqueId(), mStartDebt.getText().toString(), mFinishDebt.getText().toString().trim(),
                            mFloatTotal, mDebtUri, mIntCount, mDebtName.getText().toString());
                    if (mEdit) {
                        mDebt.setDebtUniqueId(mDebtUniqueId);//important!
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                DbCommonHelper.updateDebt(mDebt, getActivity());
                            }
                        }).start();

                        if (SynchronizeUtils.isNetworkConnected(getActivity())) {
                            Intent intent = new Intent(getActivity(), ChangeObjectIntentService.class);
                            intent.putExtra(SynchronizeUtils.ACTION_TYPE, SynchronizeUtils.OBJECT_UPDATE);
                            intent.putExtra(SynchronizeUtils.OBJECT_TYPE, SynchronizeUtils.DEBT);
                            intent.putExtra(SynchronizeUtils.DEBT_OBJECT_ID, mDebt);

                            getActivity().startService(intent);
                        } else {
                            Toast.makeText(getActivity(), R.string.not_synchronized, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        mDebt.setDebtUniqueId(UUID.randomUUID().toString());//important!
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                DbCommonHelper.insertDebt(mDebt, getActivity());
                            }
                        }).start();

                        if (SynchronizeUtils.isNetworkConnected(getActivity())) {
                            Intent intent = new Intent(getActivity(), ChangeObjectIntentService.class);
                            intent.putExtra(SynchronizeUtils.ACTION_TYPE, SynchronizeUtils.OBJECT_UPLOAD);
                            intent.putExtra(SynchronizeUtils.OBJECT_TYPE, SynchronizeUtils.DEBT);
                            intent.putExtra(SynchronizeUtils.DEBT_OBJECT_ID, mDebt);

                            getActivity().startService(intent);
                        } else {
                            Toast.makeText(getActivity(), R.string.not_synchronized, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Reminder reminder = new Reminder(getResources().getString(R.string.debtor) + " " + mDebtorName.getText().toString(),
                            getActivity().getResources().getString(R.string.debt),
                            mLongFinishDebt + 10 * 60 * 60 * 1000,//10 hours am
                            mLongFinishDebt + 10 * 60 * 60 * 1000 + 10 * 60 * 1000,//and 10 minutes
                            getActivity()
                    );
                    SetReminderDialog setReminderDialog = SetReminderDialog.newInstance(reminder);
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    setReminderDialog.show(ft, "dlg8");

                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                            Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(mDebtorName.getWindowToken(), 0);
                }
            }
        });
        //
        Bundle args = getArguments();
        mEdit = args.getBoolean(MainActivity.EDIT);
        if (mEdit) {
            actionBar.setTitle(R.string.edit_debt);
            mDebt = (Debt) args.getSerializable(DbCommonHelper.DEBT_UNIQUE_ID);
            mStartDebt.setText(mDebt.getDebtStart());
            mFinishDebt.setText(mDebt.getDebtEnd());
            /*if (Math.abs(mDebt.getDebtTotal()) < 1e-6) {//float
                mTotal.setText("");
                mSpinner.setSelection(1);*/
            if (mDebt.getDebtTotal() == 0) {//float
                mTotal.setText("");
                mSpinner.setSelection(1);
            } else {
                mTotal.setText("" + mDebt.getDebtTotal());
            }
            mDebtUri = mDebt.getDebtPhotoPath();
            if (mDebt.getDebtCount() == 0) {
                mCount.setText("");
                mDebtName.setText("");
            } else {
                mCount.setText("" + mDebt.getDebtCount());
                mDebtName.setText("" + mDebt.getDebtName());
            }
            mDebtUniqueId = mDebt.getDebtUniqueId();

            mDebtor = (Debtor) args.getSerializable(DbCommonHelper.DEBTOR_UNIQUE_ID);
            mDebtorName.setText(mDebtor.getName());
            mDebtorName.append(" ");
            mDebtorName.append(mDebtor.getSecondName());
            mDebtorPhone.setText(mDebtor.getPhone());
            mDebtorEmail.setText(mDebtor.getEmail());
            mUri = mDebtor.getPhoto();
            mDebtorUniqueId = mDebtor.getDebtorUniqueId();

            mUpView.setVisibility(View.VISIBLE);
            mBottomView.setVisibility(View.VISIBLE);

            setDebtorImageView();
            setDebtImageView();
        }else {
            actionBar.setTitle(R.string.add_debt);
        }
        if (savedInstanceState != null) {
            mUri = savedInstanceState.getString(DbCommonHelper.DEBTOR_PHOTO);
            setDebtorImageView();
            mDebtUri = savedInstanceState.getString(DbCommonHelper.DEBT_PHOTO_PATH);
            setDebtImageView();
            mStartDebt.setText(savedInstanceState.getString(DbCommonHelper.DEBT_START));
            mFinishDebt.setText(savedInstanceState.getString(DbCommonHelper.DEBT_END));

            mButton2Pressed = savedInstanceState.getBoolean(BUTTON2);
            mDebtorUniqueId = savedInstanceState.getString(DbCommonHelper.DEBTOR_UNIQUE_ID);

            mDebtorName.setText(savedInstanceState.getString(DbCommonHelper.DEBTOR_NAME));
            mDebtorEmail.setText(savedInstanceState.getString(DbCommonHelper.DEBTOR_EMAIL));
            mDebtorPhone.setText(savedInstanceState.getString(DbCommonHelper.DEBTOR_PHONE));
            mFloatTotal = savedInstanceState.getFloat(DbCommonHelper.DEBT_TOTAL);
            mIntCount = savedInstanceState.getInt(DbCommonHelper.DEBT_COUNT);
        }

        if (mButton2Pressed) {
            //mButton2.setVisibility(View.GONE);
            mUpView.setVisibility(View.VISIBLE);
            mBottomView.setVisibility(View.VISIBLE);
        }

        if (dialog) {
            mChoosePhotoDialog = ChoosePhotoDialog.newInstance();
            mChoosePhotoDialog.mSdListener = EditDebtFragment.this;
            mChoosePhotoDialog.mCaptureListener = EditDebtFragment.this;
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            mChoosePhotoDialog.show(ft, "dlg7");
        }
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mChoosePhotoDialog != null) {
            mChoosePhotoDialog.dismiss();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(DbCommonHelper.DEBTOR_NAME, mDebtorName.getText().toString().trim());
        outState.putString(DbCommonHelper.DEBTOR_PHONE, mDebtorPhone.getText().toString().trim());
        outState.putString(DbCommonHelper.DEBTOR_EMAIL, mDebtorEmail.getText().toString().trim());
        outState.putFloat(DbCommonHelper.DEBT_TOTAL, mFloatTotal);
        outState.putInt(DbCommonHelper.DEBT_COUNT, mIntCount);

        outState.putString(DbCommonHelper.DEBTOR_PHOTO, mUri);
        outState.putString(DbCommonHelper.DEBT_PHOTO_PATH, mDebtUri);
        outState.putString(DbCommonHelper.DEBT_START, mStartDebt.getText().toString().trim());
        outState.putString(DbCommonHelper.DEBT_END, mFinishDebt.getText().toString().trim());
        outState.putBoolean(BUTTON2, mButton2Pressed);

        outState.putString(DbCommonHelper.DEBTOR_UNIQUE_ID, mDebtorUniqueId);
    }

    private boolean checkEditText() {
        if (mDebtorName.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), R.string.put_correct_debtor, Toast.LENGTH_SHORT).show();
            return false;
        }
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd");
        try {
            Date start = formatter.parse(mStartDebt.getText().toString().trim());
            Date end = formatter.parse(mFinishDebt.getText().toString().trim());
            mLongFinishDebt = end.getTime();
            if (end.before(start)) {
                Toast.makeText(getActivity(), R.string.set_correct_date, Toast.LENGTH_SHORT).show();
                return false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), R.string.set_correct_date, Toast.LENGTH_SHORT).show();
            return false;
        }
        //check for type
        if (TextUtils.isEmpty(mTotal.getText().toString()) && TextUtils.isEmpty(mCount.getText().toString())) {
            Toast.makeText(getActivity(), R.string.set_money_or_object, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!TextUtils.isEmpty(mTotal.getText().toString())) {
            try {
                mFloatTotal = Float.parseFloat(mTotal.getText().toString().trim());
            } catch (NumberFormatException e) {
                Toast.makeText(getActivity(), R.string.set_correct_price, Toast.LENGTH_SHORT).show();
                return false;
            }
            mIntCount = 0;
            mDebtUri = "";
        } else {
            if (TextUtils.isEmpty(mDebtName.getText().toString())) {
                Toast.makeText(getActivity(), R.string.set_correct_name, Toast.LENGTH_SHORT).show();
                return false;
            }
            try {
                mIntCount = Integer.parseInt(mCount.getText().toString().trim());
                if (mIntCount==0){
                    Toast.makeText(getActivity(), R.string.set_correct_count, Toast.LENGTH_SHORT).show();
                    return false;
                }
            } catch (NumberFormatException e) {
                Toast.makeText(getActivity(), R.string.set_correct_count, Toast.LENGTH_SHORT).show();
                return false;
            }
            mFloatTotal = 0;
        }
        return true;
    }

    public void showSdImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, IMAGE_CODE);
    }

    public void capturePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = FileUtils.createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (photoFile != null) {
                mDebtUri = Uri.fromFile(photoFile).toString();
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(intent, REQUEST_TAKE_PHOTO);
            }
        } else {
            Toast.makeText(getActivity(), R.string.no_app, Toast.LENGTH_SHORT).show();
        }
    }

    private void setDebtorImageView() {
        if (!TextUtils.isEmpty(mUri)) {
            mDebtorImageView.setImageBitmap(ImageUtils.getBitmap(getActivity(), mUri));
        } else {
            mDebtorImageView.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.unknown_person));
        }
    }

    private void setDebtImageView() {
        if (!TextUtils.isEmpty(mDebtUri)) {
            mDebtPhoto.setImageBitmap(ImageUtils.getBitmap(getActivity(), mDebtUri));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case IMAGE_CODE:
                    mDebtUri = data.getData().toString();
                    setDebtImageView();
                    break;
                case REQUEST_TAKE_PHOTO:
                    setDebtImageView();
                    break;
                case REQUEST_RETURN_ACTIVITY:
                    mDebtor = (Debtor) data.getExtras().getSerializable(DbCommonHelper.DEBTOR_UNIQUE_ID);
                    mDebtorName.setText(mDebtor.getName());
                    mDebtorName.append(" ");
                    mDebtorName.append(mDebtor.getSecondName());
                    mDebtorPhone.setText(mDebtor.getPhone());
                    mDebtorEmail.setText(mDebtor.getEmail());
                    mUri = mDebtor.getPhoto();
                    mDebtorUniqueId = mDebtor.getDebtorUniqueId();

                    //mButton2.setVisibility(View.GONE);
                    mUpView.setVisibility(View.VISIBLE);
                    mBottomView.setVisibility(View.VISIBLE);

                    setDebtorImageView();

                    break;
                default:
                    Toast.makeText(getActivity(), R.string.unable_action, Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }

    public void showDatePickerDialog() {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            if (mStartDate) {
                mStartDebt.setText(year + "." + (month + 1) + "." + day);
            } else {
                mFinishDebt.setText(year + "." + (month + 1) + "." + day);
            }
        }
    }
}
