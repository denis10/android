package com.example.fragments;

import android.app.Activity;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.example.adapters.DebtListAdapter;
import com.example.dialogs.DebtItemDialogFragment;
import com.example.helpers.DbCommonHelper;
import com.example.objects.Debt;
import com.example.objects.Debtor;
import com.example.objects.FullDebt;
import com.example.services.ChangeObjectIntentService;
import com.example.task16_02denis.app.EditActivity;
import com.example.task16_02denis.app.LoginActivity;
import com.example.task16_02denis.app.MainActivity;
import com.example.task16_02denis.app.R;
import com.example.utils.LoginUtils;
import com.example.utils.ObjectsUtils;
import com.example.utils.SynchronizeUtils;

/**
 * Created by dev1 on 2/19/14.
 */
public class DebtListFragment extends AbsListViewBaseFragment implements LoaderManager.LoaderCallbacks<Cursor> {
    public static final String NOTIFICATION = "com.example.task16_02denis.app.fragments.DebtListFragment";
    public final static String FIRST_TABLE_ID = "id1";
    public final static String FIRST_TABLE_USER_ID = "userUniqueId";
    public static final int UNIQUE_FRAGMENT_GROUP_ID = 0;
    public final static String FIRST_TABLE_UNIQUE_DEBTOR_ID = "debtorUniqueId";
    private static final int CM_DELETE_ID = 1;
    private static final int CM_EDIT_ID = 2;
    private DbCommonHelper mDBHelper;
    private DebtListAdapter scAdapter;
    private Cursor mCursor;
    public static SearchView mSearchView;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                int resultCode = bundle.getInt(LoginActivity.RESULT);
                if (resultCode == LoginActivity.RESULT_OK) {
                    updateTable();
                    MainActivity.setRefreshActionButtonState(false);
                    //LoginUtils.saveLoginPreferences(false, getActivity());
                    Toast.makeText(getActivity(),R.string.synchronization_finished,Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(receiver, new IntentFilter(NOTIFICATION));
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(receiver);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.debt_list_fragment,
                container, false);
        ActionBar actionBar = ((ActionBarActivity) getActivity()).getSupportActionBar();
        actionBar.setTitle(R.string.debts_list);

        if (!SynchronizeUtils.isNetworkConnected(getActivity())) {//internet fault during sync
            MainActivity.setRefreshActionButtonState(false);
        }

        mDBHelper = new DbCommonHelper(getActivity());
        if (scAdapter == null) {
            FragmentManager fm = getActivity().getSupportFragmentManager();
            scAdapter = new DebtListAdapter(getActivity(), mCursor, true, fm);
        }
        setListAdapter(scAdapter);
        getActivity().getSupportLoaderManager().initLoader(UNIQUE_FRAGMENT_GROUP_ID, null, this).forceLoad();

        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        /*imageLoader.clearMemoryCache();
        imageLoader.stop();*/
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        registerForContextMenu(getListView());
        updateTable();
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Cursor c = scAdapter.getCursor();
                if (c != null) {
                    c.moveToPosition(i);
                    Debtor debtor = ObjectsUtils.createDebtorFromCursor(c);
                    Debt debt = ObjectsUtils.createDebtWithSetIdFromCursor(c);
                    FullDebt fullDebt = new FullDebt(debtor, debt);
                    DialogFragment debtItemDialogFragment = DebtItemDialogFragment.newInstance(
                            fullDebt);
                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                    debtItemDialogFragment.show(ft, "dlg5");
                }
            }
        });
    }

    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(UNIQUE_FRAGMENT_GROUP_ID, CM_DELETE_ID, 0, R.string.delete_record);
        menu.add(UNIQUE_FRAGMENT_GROUP_ID, CM_EDIT_ID, 0, R.string.edit_record);
    }

    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId() == CM_DELETE_ID) {
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item
                    .getMenuInfo();

            final Cursor c = scAdapter.getCursor();
            if (c != null) {
                c.moveToPosition(acmi.position);

                final Debt debt = ObjectsUtils.createDebtWithSetIdFromCursor(c);
                if (SynchronizeUtils.isNetworkConnected(getActivity())) {
                    Intent intent = new Intent(getActivity(), ChangeObjectIntentService.class);
                    intent.putExtra(SynchronizeUtils.ACTION_TYPE, SynchronizeUtils.OBJECT_DELETE);
                    intent.putExtra(SynchronizeUtils.OBJECT_TYPE, SynchronizeUtils.DEBT);
                    intent.putExtra(SynchronizeUtils.DEBT_OBJECT_ID, debt);
                    getActivity().startService(intent);
                } else {
                    Toast.makeText(getActivity(), R.string.not_synchronized, Toast.LENGTH_SHORT).show();
                }
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        DbCommonHelper.deleteDebt(debt.getDebtUniqueId(), getActivity());
                    }
                }).start();

                updateTable();
            }
            return true;
        } else if (item.getItemId() == CM_EDIT_ID) {
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item
                    .getMenuInfo();
            final Bundle bundle = new Bundle();
            Cursor c = scAdapter.getCursor();
            if (c != null) {
                c.moveToPosition(acmi.position);
                final Debt debt = ObjectsUtils.createDebtWithSetIdFromCursor(c);
                final Debtor debtor = ObjectsUtils.createDebtorFromCursor(c);
                Intent intent = new Intent(getActivity(), EditActivity.class);
                bundle.putInt(MainActivity.EDIT_TYPE, MainActivity.REQUEST_EDIT_DEBT);
                bundle.putBoolean(MainActivity.EDIT, true);
                bundle.putSerializable(DbCommonHelper.DEBT_UNIQUE_ID, debt);
                bundle.putSerializable(DbCommonHelper.DEBTOR_UNIQUE_ID, debtor);
                intent.putExtra(MainActivity.SEND_BUNDLE, bundle);
                startActivityForResult(intent, MainActivity.REQUEST_EDIT_DEBT);
                updateTable();
            }
            MainActivity.setRefreshActionButtonState(false);
            return true;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        SearchManager SManager =  (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchMenuItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) MenuItemCompat.getActionView(searchMenuItem);
        mSearchView.setSearchableInfo(SManager.getSearchableInfo(getActivity().getComponentName()));
        // mSearchView.setIconifiedByDefault(false);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            public boolean onQueryTextChange(String newText) {
                if (scAdapter != null)
                    scAdapter.runQueryOnBackgroundThread(newText);
                return true;
            }

            public boolean onQueryTextSubmit(String query) {
                scAdapter.runQueryOnBackgroundThread(query);
                return false;
            }
        });
        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                updateTable();
                return false;
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home)
        {
            mSearchView.onActionViewCollapsed();
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        synchronized (DbCommonHelper.class) {
            return new MyCursorLoader(getActivity(), mDBHelper);
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        mCursor = cursor;
        scAdapter.swapCursor(cursor);
        scAdapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        scAdapter.changeCursor(null);
    }

    public void updateTable() {
        getActivity().getSupportLoaderManager().getLoader(UNIQUE_FRAGMENT_GROUP_ID).forceLoad();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case MainActivity.REQUEST_EDIT_DEBT:
                    DebtListFragment currentFragment = (DebtListFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.lin_second);
                    currentFragment.updateTable();
            }
        }
    }

    private static class MyCursorLoader extends CursorLoader {

        DbCommonHelper mDBHelper;

        public MyCursorLoader(Context context, DbCommonHelper mDBHelper) {
            super(context);
            this.mDBHelper = mDBHelper;
        }

        @Override
        public Cursor loadInBackground() {

            String query = "SELECT T1._id as " + FIRST_TABLE_ID
                    + ", T1." + DbCommonHelper.DEBT_UNIQUE_ID
                    + ", T1." + DbCommonHelper.USER_UNIQUE_ID + " as " + FIRST_TABLE_USER_ID
                    + ", T1." + DbCommonHelper.DEBT_START + ", T1." + DbCommonHelper.DEBT_END
                    + ", T1." + DbCommonHelper.DEBT_TOTAL + ", T1." + DbCommonHelper.DEBT_PHOTO_PATH
                    + ", T1." + DbCommonHelper.DEBT_COUNT
                    + ", T1." + DbCommonHelper.DEBT_NAME
                    + ", T1." + DbCommonHelper.DEBTOR_UNIQUE_ID + " as " + FIRST_TABLE_UNIQUE_DEBTOR_ID
                    + ", T2.* FROM " + DbCommonHelper.TABLE_DEBTS + " T1 "
                    + " INNER JOIN " + DbCommonHelper.TABLE_DEBTORS + " T2 ON "
                    + "T1." + DbCommonHelper.DEBTOR_UNIQUE_ID + "=" + "T2." + DbCommonHelper.DEBTOR_UNIQUE_ID
                    + " WHERE " + FIRST_TABLE_USER_ID + "=?"
                    + ";";

            SQLiteDatabase db = mDBHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery(query, new String[]{"" + LoginActivity.getUserUniqueId()});

            return cursor;
        }
    }
}
