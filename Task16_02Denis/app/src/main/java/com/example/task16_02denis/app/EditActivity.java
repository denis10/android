package com.example.task16_02denis.app;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;

import com.example.fragments.EditDebtFragment;
import com.example.fragments.EditDebtorFragment;

public class EditActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null) {
            switch (getIntent().getExtras().getBundle(MainActivity.SEND_BUNDLE).getInt(MainActivity.EDIT_TYPE)){
                case MainActivity.REQUEST_EDIT_DEBTOR:
                    EditDebtorFragment editDebtorFragment = new EditDebtorFragment();
                    editDebtorFragment.setArguments(getIntent().getExtras().getBundle(MainActivity.SEND_BUNDLE));
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.add_activity_container, editDebtorFragment)
                                    //.addToBackStack(null)
                            .commit();
                    break;
                case MainActivity.REQUEST_EDIT_DEBT:
                    EditDebtFragment fragment = new EditDebtFragment();
                    fragment.setArguments(getIntent().getExtras().getBundle(MainActivity.SEND_BUNDLE));
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.add_activity_container, fragment)
                                    //.addToBackStack(null)
                            .commit();
                    break;
            }
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_return_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
