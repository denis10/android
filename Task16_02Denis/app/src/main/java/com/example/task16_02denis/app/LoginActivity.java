package com.example.task16_02denis.app;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.adapters.ShareButtonAdapter;
import com.example.dialogs.LinkedinDialog;
import com.example.services.LoginService;
import com.example.services.SynchronizeService;
import com.example.utils.SocialUtils;
import com.example.utils.SynchronizeUtils;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.code.linkedinapi.client.LinkedInApiClient;
import com.google.code.linkedinapi.client.LinkedInApiClientFactory;
import com.google.code.linkedinapi.client.enumeration.ProfileField;
import com.google.code.linkedinapi.client.oauth.LinkedInAccessToken;
import com.google.code.linkedinapi.client.oauth.LinkedInOAuthService;
import com.google.code.linkedinapi.client.oauth.LinkedInOAuthServiceFactory;
import com.google.code.linkedinapi.client.oauth.LinkedInRequestToken;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKSdk;
import com.vk.sdk.VKSdkListener;
import com.vk.sdk.VKUIHelper;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import java.util.EnumSet;
import java.util.Map;

public class LoginActivity extends Activity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public static final String EXTRA_EMAIL = "com.example.android.authenticatordemo.extra.EMAIL";
    public static final String NOTIFICATION = "com.example.task16_02denis.app";
    public static final String RESULT = "result";
    public static final String UNIQUE_USER_ID = "userUniqueId";
    public static final String FIND_USER = "findUser";
    public static final String EMAIL_USER = "emailUser";
    public static final String PASSWORD_USER = "passwordUser";
    public static final String CONNECTION_ERROR = "mConnectionError";
    public static final String SOCIAL_LOGIN = "socialLogin";
    private static final String FACEBOOK_PASSWORD = "facebookPassword";
    private static final String LINKEDIN_PASSWORD = "linkedinPassword";
    private static final String CONTACT_PASSWORD = "contactPassword";
    private static final String GOOGLE_PLUS_PASSWORD = "googlePlusPassword";
    private static final int FACEBOOK = 0;
    private static final int CONTACT = 1;
    private static final int LINKEDIN = 2;
    private static final int GOOGLE_PLUS = 3;

    private static final int RC_SIGN_IN = 49404;
    public static boolean socialLogin;
    private static String mUserUniqueId;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                mUserUniqueId = bundle.getString(UNIQUE_USER_ID);
                boolean mFind = bundle.getBoolean(FIND_USER);
                boolean mConnectionError = bundle.getBoolean(CONNECTION_ERROR);
                int resultCode = bundle.getInt(RESULT);
                if (resultCode == RESULT_OK) {
                    showProgress(false);
                    if (mFind) {
                        if (SynchronizeUtils.isNetworkConnected(LoginActivity.this)) {
                            Intent intent3 = new Intent(LoginActivity.this, SynchronizeService.class);
                            startService(intent3);
                            MainActivity.sSynchronizing = true;
                            //LoginUtils.saveLoginPreferences(true, LoginActivity.this);
                        }
                        Intent intent2 = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent2);
                        finish();
                    } else {
                        if (mConnectionError) {
                            mEmailView.setError(getString(R.string.connection_error));
                        } else {
                            if (socialLogin) {
                                Toast.makeText(LoginActivity.this, R.string.have_account, Toast.LENGTH_SHORT).show();
                            } else {
                                mPasswordView.setError(getString(R.string.error_incorrect_password));
                                mPasswordView.requestFocus();
                            }
                        }
                        //socialLogin=false;
                    }
                }
            }
        }
    };
    //private static String[] sMyScope = new String[]{VKScope.OFFLINE, VKScope.FRIENDS, VKScope.WALL, VKScope.PHOTOS, VKScope.NOHTTPS};
    private static String[] sMyScope = new String[]{};
    private static String mEmail;
    private static String mPassword;
    final LinkedInOAuthService oAuthService = LinkedInOAuthServiceFactory
            .getInstance().createLinkedInOAuthService(SocialUtils.ConfigLinkedin.LINKEDIN_CONSUMER_KEY, SocialUtils.ConfigLinkedin.LINKEDIN_CONSUMER_SECRET);
    final LinkedInApiClientFactory factory = LinkedInApiClientFactory
            .newInstance(SocialUtils.ConfigLinkedin.LINKEDIN_CONSUMER_KEY,
                    SocialUtils.ConfigLinkedin.LINKEDIN_CONSUMER_SECRET);
    private LinkedInRequestToken liToken;
    private LinkedInApiClient client;
    private LinkedInAccessToken accessToken = null;
    private String appIdVk = "4257326";
    private String mTokenKey = "VK_SDK_ACCESS_TOKEN_PLEASE_DONT_TOUCH";//must be the same in library//or no need
    private VKSdkListener sdkListener = new VKSdkListener() {
        @Override
        public void onCaptchaError(VKError captchaError) {
            /*Log.i("log_tag", "onCaptchaError");
            new VKCaptchaDialog(captchaError).show();*/
        }

        @Override
        public void onTokenExpired(VKAccessToken expiredToken) {
            /*Log.i("log_tag", "onTokenExpired");
            VKSdk.authorize(sMyScope);*/
        }

        @Override
        public void onAccessDenied(VKError authorizationError) {
            Log.i("log_tag", "onAccessDenied");
            new AlertDialog.Builder(LoginActivity.this)
                    .setMessage(authorizationError.errorMessage)
                    .show();
        }

        @Override
        public void onReceiveNewToken(VKAccessToken newToken) {
            /*Log.i("log_tag", "onReceiveNewToken");
            newToken.saveTokenToSharedPreferences(LoginActivity.this, mTokenKey);*/
        }

        @Override
        public void onAcceptUserToken(VKAccessToken token) {
            /*if (socialLogin) {
                Log.i("log_tag", "onAcceptUserToken");
                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(i);
            }*/
        }
    };
    private Button mSignIn;
    private Button mSignInVia;
    private EditText mEmailView;
    //private LoginButton mLoginButton;
    private EditText mPasswordView;
    private View mLoginFormView;
    private View mLoginStatusView;
    private TextView mLoginStatusMessageView;
    private SharedPreferences mPreference;
    private Editor mEditor;
    private ProgressDialog mProgressDialog;
    private LinkedinDialog linkedinDialog;
    private GoogleApiClient mGoogleApiClient;
    //
    private boolean mIntentInProgress;
    private boolean mSignInClicked;
    private ConnectionResult mConnectionResult;

    public static String getUserUniqueId() {
        return mUserUniqueId;
    }

    public static void setUserUniqueId(String id) {
        mUserUniqueId = id;
    }

    public static void myLogout() {
        try {
            if (VKSdk.isLoggedIn()) {
                VKSdk.logout();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(NOTIFICATION));

        if (mPreference.contains(UNIQUE_USER_ID)) {
            mUserUniqueId = mPreference.getString(UNIQUE_USER_ID, "");
        }
        if (mPreference.contains(SOCIAL_LOGIN)) {
            socialLogin = mPreference.getBoolean(SOCIAL_LOGIN, true);
        }
        if (mPreference.contains(EMAIL_USER) && mPreference.contains(PASSWORD_USER)) {
            mEmail = mPreference.getString(EMAIL_USER, "");
            mPassword = mPreference.getString(PASSWORD_USER, "");
            if (!TextUtils.isEmpty(mEmail) && !TextUtils.isEmpty(mPassword)) {
                attemptLogin();
            }
        }
        VKUIHelper.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
        savePreference();
        if (mProgressDialog != null) {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        }
        if (linkedinDialog != null) {
            if (linkedinDialog.isShowing()) {
                linkedinDialog.dismiss();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //VKUIHelper.onDestroy(this);//do not use!!!
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mEmailView = (EditText) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);
        mLoginFormView = findViewById(R.id.login_form);
        mLoginStatusView = findViewById(R.id.login_status);
        mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);
        mSignIn = (Button) findViewById(R.id.sign_in_button);
        mSignInVia = (Button) findViewById(R.id.sign_in_via_social_button);
        mPreference = getSharedPreferences(UNIQUE_USER_ID, Context.MODE_PRIVATE);
        //mLoginButton = (LoginButton) findViewById(R.id.authButton);

        mEmail = getIntent().getStringExtra(EXTRA_EMAIL);

        mGoogleApiClient = buildGoogleApiClient();

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        mSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Store values at the time of the login attempt.
                socialLogin = false;
                mEmail = mEmailView.getText().toString().trim();
                mPassword = mPasswordView.getText().toString().trim();
                attemptLogin();
            }
        });

        mSignInVia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SynchronizeUtils.isNetworkConnected(LoginActivity.this)) {
                    SocialUtils.init();

                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                    builder.setTitle(R.string.action_sign_in_via_social);
                    builder.setCancelable(true);
                    builder.setIcon(android.R.drawable.ic_menu_more);

                    builder.setSingleChoiceItems(new ShareButtonAdapter(LoginActivity.this,
                                    SocialUtils.getProviderNames(),
                                    SocialUtils.getProviderLogos()),
                            0,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int item) {
                                    switch (item) {
                                        case FACEBOOK:
                                            facebookLogin();
                                            break;
                                        case CONTACT:
                                            contactLogin();
                                            break;
                                        case LINKEDIN:
                                            linkedInLogin();
                                            break;
                                        case GOOGLE_PLUS:
                                            signInWithGplus();
                                            break;
                                    }
                                    dialog.dismiss();
                                }
                            }
                    );
                    final AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    Toast.makeText(LoginActivity.this, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void attemptLogin() {
        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password.
        if (TextUtils.isEmpty(mPassword)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        } else if (mPassword.length() < 4) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }
        // Check for a valid email address.
        if (!socialLogin) {
            if (TextUtils.isEmpty(mEmail)) {
                mEmailView.setError(getString(R.string.error_field_required));
                focusView = mEmailView;
                cancel = true;
            } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(mEmail).matches()) {
                mEmailView.setError(getString(R.string.error_invalid_email));
                focusView = mEmailView;
                cancel = true;
            }
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
            showProgress(true);

            Intent intent = new Intent(LoginActivity.this, LoginService.class);
            intent.putExtra(EMAIL_USER, mEmail);
            intent.putExtra(PASSWORD_USER, mPassword);
            startService(intent);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginStatusView.setVisibility(View.VISIBLE);
            mLoginStatusView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 1 : 0)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
                        }
                    });

            mLoginFormView.setVisibility(View.VISIBLE);
            mLoginFormView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 0 : 1)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                        }
                    });
        } else {
            mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        ///Log.i("log_tag", "requestCode=" + requestCode);
        switch (requestCode) {
            case 61992:
                VKUIHelper.onActivityResult(requestCode, resultCode, data);
                VKRequest request = VKApi.users().get();
                request.executeWithListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(VKResponse response) {
                        String s = response.json.toString();
                        Log.i("log_tag", "s=" + s);
                        mEmail = s.substring(s.indexOf("id") + 4, s.indexOf("first_name") - 2);
                        //Log.i("log_tag", "mEmail="+mEmail);
                        if (mEmail != null) {
                            successSocialLogin(mEmail, CONTACT_PASSWORD);
                        } else {
                            Toast.makeText(LoginActivity.this, R.string.no_email, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(VKError error) {
                        Log.i("log_tag", "Провес. Сообщаем пользователю об error.");
                        Toast.makeText(LoginActivity.this, R.string.login_error, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                        Log.i("log_tag", "Неудачная испытание. В аргументах имеется номер попытки и точка соприкосновени€ их количество.");
                    }
                });
                savePreference();
                break;
            case 64206:
                super.onActivityResult(requestCode, resultCode, data);
                Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
            case RC_SIGN_IN:
                if (resultCode != RESULT_OK) {
                    mSignInClicked = false;
                }
                mIntentInProgress = false;
                if (!mGoogleApiClient.isConnecting()) {
                    mGoogleApiClient.connect();
                }
                break;
        }
    }

    private void savePreference() {
        mEditor = mPreference.edit();
        mEditor.putString(UNIQUE_USER_ID, mUserUniqueId);
        mEditor.putBoolean(SOCIAL_LOGIN, socialLogin);
        mEditor.putString(EMAIL_USER, mEmail);
        mEditor.putString(PASSWORD_USER, mPassword);
        mEditor.commit();
    }

    private void removeData() {
        mEditor = mPreference.edit();
        mEditor.remove(LoginActivity.UNIQUE_USER_ID);
        mEditor.remove(LoginActivity.SOCIAL_LOGIN);
        mEditor.remove(LoginActivity.EMAIL_USER);
        mEditor.remove(LoginActivity.PASSWORD_USER);
        mEditor.commit();
        LoginActivity.setUserUniqueId("");
        mEmail = "";
        mPassword = "";
    }

    private void successSocialLogin(String email, String password) {
        socialLogin = true;
        mEmail = email;
        mPassword = password;
        showProgress(true);
        Intent intent = new Intent(LoginActivity.this, LoginService.class);
        intent.putExtra(EMAIL_USER, email);
        intent.putExtra(PASSWORD_USER, password);
        startService(intent);
    }

    private void contactLogin() {
        VKSdk.initialize(sdkListener, appIdVk, VKAccessToken.tokenFromSharedPreferences(LoginActivity.this, mTokenKey));
        VKSdk.authorize(sMyScope, true, true);
    }

    private void facebookLogin() {
        // start Facebook Login
        Session.openActiveSession(LoginActivity.this, true, new Session.StatusCallback() {
            // callback when session changes state
            @Override
            public void call(Session session, SessionState state, Exception exception) {
                if (session.isOpened()) {
                    // make request to the /me API
                    Request.newMeRequest(session, new Request.GraphUserCallback() {
                        // callback after Graph API response with user object
                        @Override
                        public void onCompleted(GraphUser user, Response response) {
                            if (user != null) {
                                Map map = user.asMap();
                                String some = map.get("id").toString();
                                successSocialLogin(some, FACEBOOK_PASSWORD);
                                //successSocialLogin(user.asMap().get("email").toString(), FACEBOOK_PASSWORD);
                                savePreference();
                            }
                        }
                    }).executeAsync();
                }
            }
        });
    }

    private void linkedInLogin() {
        mProgressDialog = new ProgressDialog(
                LoginActivity.this);
        mProgressDialog.setCanceledOnTouchOutside(false);

        linkedinDialog = new LinkedinDialog(LoginActivity.this,
                mProgressDialog);

        linkedinDialog.show();
        // set call back listener to get oauth_verifier value
        linkedinDialog.setVerifierListener(new LinkedinDialog.OnVerifyListener() {
            @Override
            public void onVerify(final String verifier) {
                Log.i("log_tag", "verifier: " + verifier);
                if (verifier != null) {
                    new AsyncTask<Void, Void, String>() {
                        @Override
                        protected String doInBackground(Void... voids) {
                            accessToken = LinkedinDialog.oAuthService
                                    .getOAuthAccessToken(LinkedinDialog.liToken,
                                            verifier);
                            LinkedinDialog.factory.createLinkedInApiClient(accessToken);
                            client = factory.createLinkedInApiClient(accessToken);
                            com.google.code.linkedinapi.schema.Person p = client.getProfileForCurrentUser(EnumSet.of(
                                    ProfileField.ID, ProfileField.FIRST_NAME,
                                    ProfileField.LAST_NAME, ProfileField.HEADLINE,
                                    ProfileField.INDUSTRY, ProfileField.PICTURE_URL,
                                    ProfileField.DATE_OF_BIRTH, ProfileField.LOCATION_NAME,
                                    ProfileField.MAIN_ADDRESS, ProfileField.LOCATION_COUNTRY));
                            Log.i("log_tag", "id=" + p.getId());
                            return p.getId();
                        }

                        @Override
                        protected void onPostExecute(String str) {
                            super.onPostExecute(str);
                            //Log.i("log_tag", "str=" + str);
                            successSocialLogin(str, LINKEDIN_PASSWORD);
                            savePreference();
                        }
                    }.execute();
                }
            }
        });

        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCancelable(true);
        mProgressDialog.show();
    }

    private GoogleApiClient buildGoogleApiClient() {
        return new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API, null)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .build();
    }

    private void signInWithGplus() {
        if (!mGoogleApiClient.isConnecting()) {
            mSignInClicked = true;
            resolveSignInError();
        }
    }

    private void resolveSignInError() {
        if (mConnectionResult == null) {
            if (mGoogleApiClient.isConnected()) {
                mGoogleApiClient.disconnect();
            }
            mGoogleApiClient.connect();
        } else {
            if (mConnectionResult.hasResolution()) {
                try {
                    mIntentInProgress = true;
                    mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
                   /* if (mSignInClicked) {
                        getProfileInformation();
                    }*/
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                    mIntentInProgress = false;
                    mGoogleApiClient.connect();
                }
            }
        }
    }

    private void getProfileInformation() {//onConnected
        try {
            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                Person currentPerson = Plus.PeopleApi
                        .getCurrentPerson(mGoogleApiClient);
                String personName = currentPerson.getDisplayName();
                String personPhotoUrl = currentPerson.getImage().getUrl();
                String personGooglePlusProfile = currentPerson.getUrl();
                String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
                successSocialLogin(email, GOOGLE_PLUS_PASSWORD);
                savePreference();
                Log.i("log_tag", "email: " + email);
            } else {
                Toast.makeText(LoginActivity.this,
                        R.string.no_email, Toast.LENGTH_LONG).show();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    private void signOutFromGplus() {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
            //mGoogleApiClient.connect();
        }
    }

    private void revokeGplusAccess() {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient)
                    .setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status arg0) {
                            mGoogleApiClient.connect();
                        }

                    });
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (mSignInClicked) {
            getProfileInformation();
        }
        if (!socialLogin) {
            signOutFromGplus();
        }
        mSignInClicked = false;
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (!result.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this,
                    0).show();
            return;
        }
        if (!mIntentInProgress) {
            mConnectionResult = result;
            if (mSignInClicked) {
                resolveSignInError();
            }
        }
    }
}
