package com.example.task16_02denis.app;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;

import com.example.fragments.DebtListFragment;
import com.example.fragments.DebtorListFragment;
import com.facebook.Session;

import java.util.ArrayList;

public class MainActivity extends ActionBarActivity implements
        ActionBar.OnNavigationListener {
    public static final int REQUEST_EDIT_DEBTOR = 1;
    public static final int REQUEST_EDIT_DEBT = 0;
    public static int previousSpinnerState = REQUEST_EDIT_DEBT;
    public static final String EDIT_TYPE = "editType";
    public static final String SEND_BUNDLE = "sendBundle";
    public static final String PICK = "pick";
    public static final String EDIT = "edit";
    public static final int IMAGE_CURVATURE = 20;
    public static boolean sSynchronizing;
    public static final  String SYNCHRONIZING= "sSynchronizing";
    private static String sSpinnerState = "spinnerState";
    private static Menu sOptionsMenu;
    private static MenuItem sRefreshItem;
    private ActionBar actionBar;
    private ArrayList<String> mItemList;
    private SharedPreferences mPreference;

    public static void setRefreshActionButtonState(final boolean refreshing) {
        if (sOptionsMenu != null) {

            if (sRefreshItem != null) {
                if (refreshing) {
                    sRefreshItem.setVisible(true);
                    MenuItemCompat.setActionView(sRefreshItem, R.layout.actionbar_indeterminate_progress);
                } else {
                    sSynchronizing = false;
                    sRefreshItem.setVisible(false);
                    //sRefreshItem.setActionView(null);
                }
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        mItemList = new ArrayList<String>();
        mItemList.add(getResources().getString(R.string.view_debt_list));
        mItemList.add(getResources().getString(R.string.view_debtor_list));
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, mItemList);
        actionBar.setListNavigationCallbacks(spinnerAdapter, this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mPreference = getSharedPreferences(sSpinnerState, Context.MODE_PRIVATE);
        if (mPreference.contains(sSpinnerState)) {
            previousSpinnerState = mPreference.getInt(sSpinnerState, 0);
            actionBar.setSelectedNavigationItem(previousSpinnerState);
        }
  /*      if (mPreference.contains(SYNCHRONIZING)) {
            sSynchronizing = mPreference.getBoolean(SYNCHRONIZING, false);
        }*/
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences.Editor editor = mPreference.edit();
        editor.putInt(sSpinnerState, previousSpinnerState);
        //editor.putBoolean(SYNCHRONIZING, sSynchronizing);
        editor.commit();
        MainActivity.setRefreshActionButtonState(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        sOptionsMenu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_actions, menu);
        sRefreshItem = sOptionsMenu.findItem(R.id.menu_refresh);
        if (sSynchronizing) {
            setRefreshActionButtonState(true);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            /*case android.R.id.home:
                //close keyboard
                TextView txt = (TextView) findViewById(R.id.main_txt);
                InputMethodManager imm = (InputMethodManager) this.getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(txt.getWindowToken(), 0);
                actionBar.setSelectedNavigationItem(REQUEST_EDIT_DEBT);
                break;*/
            case R.id.menu_refresh:
                break;
            case R.id.action_add:
                setRefreshActionButtonState(false);
                switch (previousSpinnerState) {
                    case REQUEST_EDIT_DEBT:
                        Intent intent2 = new Intent(this, EditActivity.class);
                        Bundle bundle2 = new Bundle();
                        bundle2.putInt(EDIT_TYPE, REQUEST_EDIT_DEBT);
                        bundle2.putBoolean(EDIT, false);
                        intent2.putExtra(SEND_BUNDLE, bundle2);
                        startActivityForResult(intent2, REQUEST_EDIT_DEBT);
                        break;
                    case REQUEST_EDIT_DEBTOR:
                        Intent intent1 = new Intent(this, EditActivity.class);
                        Bundle bundle1 = new Bundle();
                        bundle1.putInt(EDIT_TYPE, REQUEST_EDIT_DEBTOR);
                        bundle1.putBoolean(EDIT, false);
                        intent1.putExtra(SEND_BUNDLE, bundle1);
                        startActivityForResult(intent1, REQUEST_EDIT_DEBTOR);
                        break;
                }
                break;
            case R.id.action_logout:
                SharedPreferences mPreference = getSharedPreferences(LoginActivity.UNIQUE_USER_ID, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = mPreference.edit();
                editor.remove(LoginActivity.UNIQUE_USER_ID);
                editor.remove(LoginActivity.SOCIAL_LOGIN);
                editor.remove(LoginActivity.EMAIL_USER);
                editor.remove(LoginActivity.PASSWORD_USER);
                editor.commit();
                LoginActivity.setUserUniqueId("");

                if (LoginActivity.socialLogin) {
                    LoginActivity.socialLogin = false;
                    callFacebookLogout(MainActivity.this);
                    LoginActivity.myLogout();
                }

                Intent intent3 = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent3);
                finish();
                break;
            case R.id.action_exit:
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(int position, long id) {
        Bundle bundle;
        switch (position) {
            case REQUEST_EDIT_DEBT:
                Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.lin_second);
                if (!(currentFragment instanceof DebtListFragment)) {
                    previousSpinnerState = REQUEST_EDIT_DEBT;
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.lin_second, new DebtListFragment())
                                    //.addToBackStack(null)
                            .commit();
                }
                break;
            case REQUEST_EDIT_DEBTOR:
                Fragment currentFragment2 = getSupportFragmentManager().findFragmentById(R.id.lin_second);
                if (!(currentFragment2 instanceof DebtorListFragment)) {
                    previousSpinnerState = REQUEST_EDIT_DEBTOR;
                    bundle = new Bundle();
                    bundle.putBoolean(PICK, false);
                    DebtorListFragment debtorListFragment = new DebtorListFragment();
                    debtorListFragment.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.lin_second, debtorListFragment)
                                    //.addToBackStack(null)
                            .commit();
                }
                break;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_EDIT_DEBT:
                    previousSpinnerState = REQUEST_EDIT_DEBT;
                    DebtListFragment currentFragment = (DebtListFragment) getSupportFragmentManager().findFragmentById(R.id.lin_second);
                    currentFragment.updateTable();
                    break;
                case REQUEST_EDIT_DEBTOR:
                    previousSpinnerState = REQUEST_EDIT_DEBTOR;
                    DebtorListFragment currentFragment2 = (DebtorListFragment) getSupportFragmentManager().findFragmentById(R.id.lin_second);
                    currentFragment2.updateTable();
                    break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (DebtListFragment.mSearchView != null && !DebtListFragment.mSearchView.isIconified()) {
            DebtListFragment.mSearchView.setIconified(true);
        } else if (DebtorListFragment.mSearchView != null && !DebtorListFragment.mSearchView.isIconified()) {
            DebtorListFragment.mSearchView.setIconified(true);
        } else {
            super.onBackPressed();
        }
    }

    private void callFacebookLogout(Context context) {
        Session session = Session.getActiveSession();
        if (session != null) {
            if (!session.isClosed()) {
                session.closeAndClearTokenInformation();
                //clear your preferences if saved
            }
        } else {
            session = new Session(context);
            Session.setActiveSession(session);
            session.closeAndClearTokenInformation();
            //clear your preferences if saved
        }
    }
}