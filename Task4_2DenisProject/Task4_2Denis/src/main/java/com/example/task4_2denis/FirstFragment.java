package com.example.task4_2denis;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by dev1 on 1/31/14.
 */
public class FirstFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.first_fragment,
                container, false);

        Button b1=(Button)view.findViewById(R.id.btn1);
        b1.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction()
                        /*.remove(FirstFragment.this)
                        .add(R.id.container, new SecondFragment())*/
                        .replace(R.id.container, new SecondFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });

        return view;
    }
}