package com.example.task4_2denis;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by dev1 on 1/31/14.
 */
public class FourthFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fourth_fragment,
                container, false);
        Button b4=(Button)view.findViewById(R.id.btn4);
        b4.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View view) {
                getFragmentManager()
                        .popBackStack(0, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        });

        return view;
    }
}