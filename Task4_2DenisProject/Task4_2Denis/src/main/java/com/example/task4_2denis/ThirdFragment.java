package com.example.task4_2denis;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by dev1 on 1/31/14.
 */
public class ThirdFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.third_fragment,
                container, false);
        Button b3=(Button)view.findViewById(R.id.btn3);
        b3.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction()
                        /*.remove(ThirdFragment.this)
                        .add(R.id.container, new FourthFragment())*/
                        .replace(R.id.container, new FourthFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });


        return view;
    }
}