package com.example.task4_1denis;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by dev1 on 1/31/14.
 */
public class FirstFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.first_fragment_layout,
                container, false);

        final  LinearLayout mLinearLayoutSecond = (LinearLayout) inflater.inflate(R.layout.second_fragment_layout,
                container, false);

        Button b1=(Button)view.findViewById(R.id.btn1);
        b1.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View view) {
//???
                EditText edt=(EditText)getActivity().getFragmentManager().findFragmentById(R.id.first).getView().findViewById(R.id.edt1);
                TextView txt=(TextView)getActivity().getFragmentManager().findFragmentById(R.id.second).getView().findViewById(R.id.txt2);
                txt.setText(edt.getText());
            }
        });
        return view;
    }
}