package com.example.task14_02third.app;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ThirdActivity extends BaseActivity {
    private int mNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
        //doBindService();

        txt1 = (TextView) findViewById(R.id.txt3);

        Button b3 = (Button) findViewById(R.id.btn3);
        b3.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText edt3 = (EditText) findViewById(R.id.edt3);
                if (checkInput(edt3.getText().toString())) {
                    sendMessage();

                }
            }
        });
    }

    private void sendMessage() {
        if (!mIsBound) return;

        // Give it some value as an example.
        Message msg = Message.obtain(null,
                RemoteService.MSG_SET_VALUE, mNumber, 0);

        try {
            mService.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private boolean checkInput(String edit) {
        //boolean res=false;
        try {
            mNumber = Integer.parseInt(edit);
            return true;
        } catch (NumberFormatException e) {
            Toast.makeText(getApplicationContext(), "Please put correct data", Toast.LENGTH_SHORT).show();
        }
        return false;
    }
}
