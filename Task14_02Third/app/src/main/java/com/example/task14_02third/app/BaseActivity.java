package com.example.task14_02third.app;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.TextView;
import android.widget.Toast;

public class BaseActivity extends ActionBarActivity {
    protected TextView txt1;
    //private boolean mFirstCall = true;
    protected long mResultValue;
    /**
     * Messenger for communicating with service.
     */
    protected Messenger mService = null;
    /**
     * Flag indicating whether we have called bind on the service.
     */
    protected boolean mIsBound;

    /**
     * Handler of incoming messages from service.
     */
    protected class IncomingHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case RemoteService.MSG_SET_VALUE:
                    Toast.makeText(BaseActivity.this, "Received from service: " + msg.arg1,
                            Toast.LENGTH_SHORT).show();
                    txt1.setText("factorial: " + msg.arg1);
                    mResultValue = msg.arg1;
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    protected final Messenger mMessenger = new Messenger(new IncomingHandler());

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putLong("value", mResultValue);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        mResultValue = savedInstanceState.getLong("value");
        txt1.setText("factorial=" + mResultValue);
        super.onRestoreInstanceState(savedInstanceState);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        doBindService();

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        doUnbindService();
    }

    protected ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the service object we can use to
            // interact with the service.  We are communicating with our
            // service through an IDL interface, so get a client-side
            // representation of that from the raw service object.
            mService = new Messenger(service);
            //txt2.setText("Attached.");

            // We want to monitor the service for as long as we are
            // connected to it.
            try {
                Message msg = Message.obtain(null,
                        RemoteService.MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mService.send(msg);

            } catch (RemoteException e) {
                // In this case the service has crashed before we could even
                // do anything with it; we can count on soon being
                // disconnected (and then reconnected if it can be restarted)
                // so there is no need to do anything here.
                Log.e("log_tag", "RemoteException");
            }

            // As part of the sample, tell the user what happened.
            Toast.makeText(BaseActivity.this, "remote_service_connected",
                    Toast.LENGTH_SHORT).show();
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            mService = null;
            // txt2.setText("Disconnected.");

            // As part of the sample, tell the user what happened.
            Toast.makeText(BaseActivity.this, "remote_service_disconnected",
                    Toast.LENGTH_SHORT).show();
        }
    };

    protected void doBindService() {
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
        bindService(new Intent(BaseActivity.this,
                RemoteService.class), mConnection, Context.BIND_AUTO_CREATE);
        mIsBound = true;
//        txt3.setText("Binding.");
    }

    protected void doUnbindService() {
        if (mIsBound) {
            // If we have received the service, and hence registered with
            // it, then now is the time to unregister.
            if (mService != null) {
                try {
                    Message msg = Message.obtain(null,
                            RemoteService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {
                    // There is nothing special we need to do if the service
                    // has crashed.
                }
            }
            // Detach our existing connection.
            unbindService(mConnection);
            mIsBound = false;
            // txt2.setText("Unbinding.");
        }
    }
}
