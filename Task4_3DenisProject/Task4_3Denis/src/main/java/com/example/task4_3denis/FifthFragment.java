package com.example.task4_3denis;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by dev1 on 1/31/14.
 */
public class FifthFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fifth_fragment,
                container, false);
        return view;
    }
    static FifthFragment newInstance() {
        return new FifthFragment();
    }
}