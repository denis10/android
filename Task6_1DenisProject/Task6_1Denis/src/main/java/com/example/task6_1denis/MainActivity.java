package com.example.task6_1denis;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {
    private SQLiteDatabase db;
    private ContentValues cv;
    private DbCommonHelper dbCommonHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn1=(Button)findViewById(R.id.btn1);
        btn1.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                dbCommonHelper = new DbCommonHelper(MainActivity.this);
                try {
                    db = dbCommonHelper.getWritableDatabase();
                } catch (SQLiteException ex) {
                    db = dbCommonHelper.getReadableDatabase();
                }
                cv = new ContentValues();
                cv.put(DbCommonHelper.OFFICER_NAME, "Max");
                db.insert(DbCommonHelper.TABLE_OFFICERS, null, cv);
                cv = new ContentValues();
                cv.put(DbCommonHelper.OFFICER_NAME, "Denis");
                db.insert(DbCommonHelper.TABLE_OFFICERS, null, cv);

                cv = new ContentValues();
                cv.put(DbCommonHelper.SUSPECT_NAME, "Bill");
                db.insert(DbCommonHelper.TABLE_SUSPECTS, null, cv);
                cv = new ContentValues();
                cv.put(DbCommonHelper.SUSPECT_NAME, "Carl");
                db.insert(DbCommonHelper.TABLE_SUSPECTS, null, cv);

                cv = new ContentValues();
                cv.put(DbCommonHelper.DISTRICT_NAME, "DownTown");
                db.insert(DbCommonHelper.TABLE_DISTRICTS, null, cv);
                cv = new ContentValues();
                cv.put(DbCommonHelper.DISTRICT_NAME, "village");
                db.insert(DbCommonHelper.TABLE_DISTRICTS, null, cv);

                cv = new ContentValues();
                cv.put(DbCommonHelper.OFFICER_ID, 1);
                cv.put(DbCommonHelper.SUSPECT_ID, 1);
                cv.put(DbCommonHelper.DISTRICT_ID, 1);
                db.insert(DbCommonHelper.TABLE_DETENTIONS, null, cv);
                cv.put(DbCommonHelper.OFFICER_ID, 2);
                cv.put(DbCommonHelper.SUSPECT_ID, 2);
                cv.put(DbCommonHelper.DISTRICT_ID, 2);
                db.insert(DbCommonHelper.TABLE_DETENTIONS, null, cv);
                db.close();
                Toast.makeText(getApplicationContext(), "created", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
