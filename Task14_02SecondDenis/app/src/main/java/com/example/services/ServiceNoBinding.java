package com.example.services;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.widget.Toast;

import com.example.app.MainActivity;
import com.example.app.R;

//import android.os.HandlerThread;

/**
 * Created by Denis on 15.02.14.
 */
public class ServiceNoBinding extends Service {
    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;
    //private NotificationManager mNotificationManager;
    private long mCount;
    private LooperThread mLooperThread;
    private boolean mStopCount;

    public static final String NOTIFICATION = "com.example.app";
    private int result = Activity.RESULT_CANCELED;
    public static final String RESULT = "result";
    public static final String VALUE = "value";


    private class LooperThread extends Thread {
        private Message msg;

        public LooperThread(Message msg) {
            this.msg = msg;
        }

        public void run() {
            Looper.prepare();
            while (!mStopCount) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                mServiceHandler.handleMessage(msg);
            }

            Looper.loop();
        }
    }

    // Handler that receives messages from the thread
    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            mCount++;
            if (!mStopCount) {
                makeNotification();
                result = Activity.RESULT_OK;
                publishResults(mCount, result);
            }
            stopSelf(msg.arg1);
        }
    }

    @Override
    public void onCreate() {
        /*mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);*/

        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);

        thread.start();

        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Message msg = mServiceHandler.obtainMessage();
        mLooperThread = new LooperThread(msg);
        mLooperThread.start();

        // If we get killed, after returning from here, restart
        return START_NOT_STICKY;
    }


    @Override
    public IBinder onBind(Intent intent) {
        // We don't provide binding, so return null
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show();
        mStopCount = true;
        //mNotificationManager.cancelAll();
        stopForeground(true);

    }

    private void makeNotification() {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getApplicationContext())
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle("count")
                        .setContentText("=" + mCount);

        Intent resultIntent = new Intent(getApplicationContext(), ServiceNoBinding.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT

                );
        mBuilder.setContentIntent(resultPendingIntent);
        //mNotificationManager.notify(1, mBuilder.build());
        startForeground(1337, mBuilder.build());
    }
    private void publishResults(long fac, int result) {
        //Log.i("log_tag","in publishResults");
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(VALUE, fac);
        intent.putExtra(RESULT, result);
        sendBroadcast(intent);
    }
}