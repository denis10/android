package com.example.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.services.ServiceNoBinding;

public class MainActivity extends ActionBarActivity {
    private Intent intent;
    private TextView txt1;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                long value = bundle.getLong(ServiceNoBinding.VALUE);
                int resultCode = bundle.getInt(ServiceNoBinding.RESULT);
                if (resultCode == RESULT_OK) {
                   txt1.setText("Count="+value);
                }
            }
        }
    };

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("current",txt1.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        txt1.setText(savedInstanceState.getString("current"));
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(ServiceNoBinding.NOTIFICATION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txt1=(TextView)findViewById(R.id.txt1);
        intent = new Intent(MainActivity.this, ServiceNoBinding.class);
        Button  b1=(Button)findViewById(R.id.btn1);
        b1.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {

                        startService(intent);
            }
        });

        Button  b2=(Button)findViewById(R.id.btn2);
        b2.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getApplicationContext(), "Stop service", Toast.LENGTH_SHORT).show();
                //stopService(new Intent(MainActivity.this,ServiceNoBinding.class));
                stopService(intent);
            }
        });

    }
}
